package com.alg.shopping.tools;

import android.content.Context;
import android.text.TextUtils;

import com.alg.shopping.R;
import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Lenovo on 2017/12/19.
 */

public class DateUtils {

    /**
     * 获取两个日期之间的间隔天数
     * @return
     */
    public static int getGapCount(Date startDate, Date endDate) {
        if(startDate != null && endDate != null){
            Calendar fromCalendar = Calendar.getInstance();
            fromCalendar.setTime(startDate);
            fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
            fromCalendar.set(Calendar.MINUTE, 0);
            fromCalendar.set(Calendar.SECOND, 0);
            fromCalendar.set(Calendar.MILLISECOND, 0);
            Calendar toCalendar = Calendar.getInstance();
            toCalendar.setTime(endDate);
            toCalendar.set(Calendar.HOUR_OF_DAY, 0);
            toCalendar.set(Calendar.MINUTE, 0);
            toCalendar.set(Calendar.SECOND, 0);
            toCalendar.set(Calendar.MILLISECOND, 0);
            int num = (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
            if(num < 1){
                num = 1;
            }
            return num;
        }else{
            return 1;
        }
    }


    static TimePickerView pvCustomTime;
    public static TimePickerView initDatePicker(Context context, Calendar startCaledar, Calendar currCaledar, Calendar endCaledar, TimePickerView.OnTimeSelectListener listener) {
        //时间选择器 ，自定义布局
        pvCustomTime = new TimePickerView.Builder(context, listener)
                .setDate(currCaledar)
                .setRangDate(startCaledar, endCaledar)
                .setContentSize(18)//滚轮文字大小
                .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(false)//是否循环滚动
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("年", "月", "日", "", "", "")//默认设置为年月日时分秒
                .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setDividerColor(context.getResources().getColor(R.color.line_color))
                .build();
        return pvCustomTime;
    }
    static OptionsPickerView pvCustomOptions;
    /**
     * @description 注意事项：
     * 自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针。
     * 具体可参考demo 里面的两个自定义layout布局。
     */
    public static OptionsPickerView initOptionPicker(Context context, String[] data, int currSelect, OptionsPickerView.OnOptionsSelectListener onOptionsSelectListener) {//条件选择器初始化，自定义布局
        NormalUtils.hintKbTwo(context);
        pvCustomOptions = new OptionsPickerView.Builder(context, onOptionsSelectListener)
                .setContentTextSize(18)//滚轮文字大小
                .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                .setSelectOptions(currSelect)
                .setDividerColor(context.getResources().getColor(R.color.line_color))
                .build();
        if(data != null && data.length > 0){
            List<String> datas = Arrays.asList(data);
            pvCustomOptions.setPicker(datas);
            return pvCustomOptions;
        }
        return null;
    }
    /**
     * 判断相差多少天(效率比较高)
     * @param day 传入的 时间  "2016-06-28 10:10:30" "2016-06-28" 都可以
     * @return 多少天前
     * @throws ParseException
     */
    public static int IsToday(String day) {
        int diffDay = 0;
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        Date date = getDate(day,"yyyy-MM-dd");
        if(date != null){
            cal.setTime(date);
            if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
                diffDay = cal.get(Calendar.DAY_OF_YEAR) - pre.get(Calendar.DAY_OF_YEAR);
            }
        }
        return diffDay;
    }
    /**
     * 判断多少分钟前(效率比较高)
     * @param day 传入的 时间  "2016-06-28 10:10:30"
     * @return 返回相差多少分钟
     * @throws ParseException
     */
    public static int IsMinute(String day) {
        int diffDay = 0;
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        Date date = getDate(day,"yyyy-MM-dd HH:mm:ss");
        if(date != null){
            cal.setTime(date);
            if (cal.get(Calendar.MINUTE) == (pre.get(Calendar.MINUTE))) {
                diffDay = cal.get(Calendar.MINUTE) - pre.get(Calendar.MINUTE);
            }
        }
        return diffDay;
    }
    /**
     * 判断多少分钟前(效率比较高)
     * @param day 传入的 时间  "2016-06-28 10:10:30"
     * @return 返回相差多少小时
     */
    public static int IsHour(String day) {
        int diffDay = 0;
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        Date date = getDate(day,"yyyy-MM-dd HH:mm:ss");
        if(date != null){
            cal.setTime(date);
            if (cal.get(Calendar.HOUR_OF_DAY) == (pre.get(Calendar.HOUR_OF_DAY))) {
                diffDay = cal.get(Calendar.HOUR_OF_DAY) - pre.get(Calendar.HOUR_OF_DAY);
            }
        }
        return diffDay;
    }
    public static String decoverDateToStr(String day){
        String dayDesc = day;
        try {
            int day_num = IsToday(day);
            if(day_num == 0){
                int hour_num = IsHour(day);
                if(hour_num == 0){
                    dayDesc = IsMinute(day)+"分钟前";
                }else{
                    dayDesc = hour_num+"小时前";
                }
            }else if(day_num == 1){
                dayDesc = "昨天";
            }else{
                dayDesc = dateCoverStr(getDate(day),"yyyy.MM.dd");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dayDesc;
    }
    public static Date getDate(String time){
        Date mDate = null;
        if(!TextUtils.isEmpty(time)){
            try {
                mDate = new Date();
                mDate.setTime(Long.parseLong(time));
            }catch (Exception e){}
        }
        return mDate;
    }
    public static String dateCoverStr(Date mDate,String dateStyle) {
        String mDateStr = "";
        if (mDate != null) {
            SimpleDateFormat sf = new SimpleDateFormat(dateStyle);
            mDateStr = sf.format(mDate);
        }
        return mDateStr;
    }
    public static Date getDate(String dateStr,String currDateStr){
        SimpleDateFormat formatter = new SimpleDateFormat(currDateStr);
        try {
            return formatter.parse(dateStr);
        }catch (Exception e){

        }
        return null;
    }
    public static String dateStyleCover(String dateStr,String currDateStr,String coverDateStr){
        Date mDate = getDate(dateStr,currDateStr);
        return dateCoverStr(mDate,coverDateStr);
    }

    /*
     * 毫秒转化天时分秒
     */
    public static String formatTime(Long ms) {
        Integer ss = 1000;
        Integer mi = ss * 60;
        Integer hh = mi * 60;
        Integer dd = hh * 24;

        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = (ms - day * dd - hour * hh) / mi;
        Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
//        Long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

        StringBuffer sb = new StringBuffer();
        if(day > 0) {
            sb.append(day+"天");
        }
        if(hour > 0) {
            sb.append(hour+"小时");
        }
        if(minute > 0) {
            sb.append(minute+"分");
        }
        if(second > 0) {
            sb.append(second+"秒");
        }
//        if(milliSecond > 0) {
//            sb.append(milliSecond+"毫秒");
//        }
        return sb.toString();
    }
}
