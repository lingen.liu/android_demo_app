package com.alg.shopping.wxapi;
import android.content.Intent;
import android.text.TextUtils;

import com.alg.shopping.bean.EventBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.pub.PayResultActivity;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;


public class WXPayEntryActivity extends BaseActivity implements IWXAPIEventHandler {
    private IWXAPI api;
	@Override
	protected void reciverMesssage(EventBean obj) {
		if(TextUtils.equals(obj.getFlag(), ActionFlag.PAYSUCCESS)){
			finish();
		}
	}

	@Override
	protected void setLayouts() {
	}

	@Override
	protected void initViews() {
		api = WXAPIFactory.createWXAPI(this, Constants.WX_APP_ID);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}
	@Override
	public void onResp(BaseResp resp) {
		Intent intent = new Intent(WXPayEntryActivity.this, PayResultActivity.class);
		intent.putExtra("moneyStr", CacheUtils.getInstance().getShared("moneyStr"));
		if (resp.errCode == 0) {
			intent.putExtra("type",1);
		}else{
			intent.putExtra("type",0);
		}
		jumpPage(intent);
		finish();
	}
}