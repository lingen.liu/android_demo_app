package com.alg.shopping.contants;

/**
 * Created by Lenovo on 2017/12/13.
 */

public class ActionFlag {
    public final static String LOGINLOUT = "LOGINLOUT";//退出登录
    public final static String LOGINLSUCCESS = "LOGINLSUCCESS";//登录成功
    public final static String CLOSEALL = "CLOSEALL";//崩溃关闭所有页面
    public final static String BACKHOME = "BACKHOME";//返回首页通知
    public final static String CARTCHANGE = "CARTCHANGE";//购物车发生变化通知
    public final static String ADDRESSCHANGE = "ADDRESSCHANGE";//收货地址变动通知
    public final static String ADDBANKSUCCESS = "ADDBANKSUCCESS";//添加银行卡成功
    public final static String PAYSUCCESS = "PAYSUCCESS";//支付成功通知

    public final static String ORDERCHANGE = "ORDERCHANGE";//订单状态改变监听

}
