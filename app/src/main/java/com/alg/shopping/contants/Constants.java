package com.alg.shopping.contants;

import android.os.Environment;

import org.xutils.x;

import java.io.File;

/**
 * Created by Lenovo on 2017/12/13.
 */

public class Constants {
    public static String SQL_NAME = "alg.db";//数据库名
    public static int SQL_VERSION = 1001;//数据库版本号
    public static String DB_DATA = x.app().getCacheDir() + File.separator + x.app().getPackageName() + File.separator;
    public static String DB_DATA_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + x.app().getPackageName() + File.separator;

    public static final String IMAGE_CACHE_DIR = DB_DATA_PATH + "/image_cache";//Glide缓存路劲

    public final static int REQUEST_CODE_SCAN = 1001;//注册二维码扫描

    public static final String SHAREDPRENCEDATA ="SHAREDPRENCEDATA";//Sharedprence存储key

    public static int PAGENUM = 20;//每页数据条数
    public static long CODELONG = 60 * 1000;//获取验证码时间
    public static long countDownInterval = 1 * 1000;//验证码时间间隔

    public static final String WX_APP_ID = "wx993fd94545d00bc1";//微信支付key

    //sql数据存储key
    public static final String USERINFOKEY ="USERINFOKEY";//用户信息KEY
    public static final String HOMEDATAKEY ="HOMEDATAKEY";//首页数据KEY
    public static final String ACCOUNTDATAKEY ="ACCOUNTDATAKEY";//账户信息KEY


    public static final String[] articlesArray = {"","公告","活动","促销","新闻"};//资讯类型
    public static final String[] orderType = {"SUBMITTED","PAID","SHIPPING","COMPLETED"};//订单状态
}
