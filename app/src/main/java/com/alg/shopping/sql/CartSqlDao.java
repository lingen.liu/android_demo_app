package com.alg.shopping.sql;

import android.text.TextUtils;

import com.alg.shopping.contants.Constants;

import org.xutils.DbManager;
import org.xutils.common.util.KeyValue;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.ex.DbException;
import org.xutils.x;

import java.io.File;
import java.util.List;

/**
 * 数据库操作
 *
 * @author 邱强 创建于 2016/3/29 10:35
 */
public class CartSqlDao {
    private static CartSqlDao intance = null;
    private static DbManager mDbManager;

    public static CartSqlDao getInance() {
        if (intance == null) {
            intance = new CartSqlDao();
        }
        if (mDbManager == null) {
            mDbManager = x.getDb(getDaoConfig());
        }
        return intance;
    }

    private static DbManager.DaoConfig daoConfig;

    private static DbManager.DaoConfig getDaoConfig() {
        File file = new File(Constants.DB_DATA);
        if (!file.exists()) {
            file.mkdirs();
        }
        if (daoConfig == null) {
            daoConfig = new DbManager.DaoConfig()
                    .setDbName(Constants.SQL_NAME)
                    .setDbVersion(Constants.SQL_VERSION)
                    .setDbOpenListener(new DbManager.DbOpenListener() {
                        @Override
                        public void onDbOpened(DbManager db) {
                            // 开启WAL, 对写入加速提升巨大
                            db.getDatabase().enableWriteAheadLogging();
                        }
                    })
                    .setDbUpgradeListener(new DbManager.DbUpgradeListener() {
                        @Override
                        public void onUpgrade(DbManager db, int oldVersion, int newVersion) {
                            // TODO: ...
                            // db.dropTable(...);
                        }
                    });
        }
        if (file.exists()) {
            daoConfig.setDbDir(file);
        }
        return daoConfig;
    }
    /**
     * 查询全部数据
     *
     * @return
     */
    public List<CartDataTable> query() {
        try {
            if (mDbManager == null) {
                mDbManager = x.getDb(getDaoConfig());
            }
            List<CartDataTable> persons = mDbManager.findAll(CartDataTable.class);
            return persons;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 查询数据
     *
     * @param keyStr 方法名
     * @return
     */
    public CartDataTable query(String keyStr) {
        try {
            if (mDbManager == null) {
                mDbManager = x.getDb(getDaoConfig());
            }
            CartDataTable persons = mDbManager.selector(CartDataTable.class).where("goodId", "=", keyStr).findFirst();
            return persons;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void clearAll(){
        try {
            if (mDbManager == null) {
                mDbManager = x.getDb(getDaoConfig());
            }
            mDbManager.delete(CartDataTable.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }
    public void insert(String goodId, String valueStr,int num) {
        CartDataTable mCartDataTable = query(goodId);
        if (mCartDataTable == null) {
            try {
                CartDataTable person = new CartDataTable();
                person.setGoodId(goodId);
                person.setNum(num);
                person.setValueStr(valueStr);
                if (mDbManager == null) {
                    mDbManager = x.getDb(getDaoConfig());
                }
                mDbManager.save(person);
            } catch (DbException e) {
                e.printStackTrace();
            }
        } else {
            update(goodId, valueStr,num);
        }
    }
    public void delete(String goodId){
        WhereBuilder wb = WhereBuilder.b("goodId", "=", goodId);
        try {
            if (mDbManager == null) {
                mDbManager = x.getDb(getDaoConfig());
            }
            mDbManager.delete(CartDataTable.class,wb);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }
    public void update(String keyStr, String valueStr,int num) {
        try {
            if (mDbManager == null) {
                mDbManager = x.getDb(getDaoConfig());
            }
            WhereBuilder wb = WhereBuilder.b("goodId", "=", keyStr);
            KeyValue kv = new KeyValue("valueStr", valueStr);
            if(num != -1){
                KeyValue kvNum = new KeyValue("num", num);
                mDbManager.update(CartDataTable.class, wb, kv,kvNum);
            }else{
                mDbManager.update(CartDataTable.class, wb, kv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
