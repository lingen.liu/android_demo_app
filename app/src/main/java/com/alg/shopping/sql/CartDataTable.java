package com.alg.shopping.sql;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

import java.io.Serializable;

@Table(name = "CartDataTable")
public class CartDataTable implements Serializable{
    @Column(name = "id", isId = true, autoGen = true)
    private int id;
    @Column(name = "goodId")
    private String goodId;
    @Column(name = "valueStr")
    private String valueStr;
    @Column(name = "num")
    private int num = 0;
    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
