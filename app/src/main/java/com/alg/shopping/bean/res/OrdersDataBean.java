package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;

import java.util.List;

public class OrdersDataBean extends BasePageResBean{
    public List<OrderRowsBean> rows;
}
