package com.alg.shopping.bean.res;


public class ArticleDescDataBean{
    public String id;

    /**
     * 标题
     */
    public String title;

    //二级标题
    public String subTitle;

    //推荐到首页
    public boolean recommend;

    //文章类型
    public String type;

    //文章封面,之前有解释
    public PhotoBean postCover;

    //标签，逗号分隔，没什么用
    public String tags;

    /**
     * 正文内容  html代码。
     */
    public String details;

}
