package com.alg.shopping.bean.req;

import com.alg.shopping.bean.base.BaseReqBean;

/**
 * Created by XB on 2018/3/15.
 */

public class EditAddressReqBean extends BaseReqBean {
    public String id = "";
    public String receiver = "";
    public String mobile = "";
    public String province = "";
    public String city = "";
    public String borough = "";
    public String address = "";
    public String is_default = "";
}
