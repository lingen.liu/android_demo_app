package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BaseResBean;

import java.util.ArrayList;

/**
 * Created by XB on 2018/3/15.
 */

public class AddressResBean extends BaseResBean {
    public ArrayList<AddressDataBean> result;
}
