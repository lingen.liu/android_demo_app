package com.alg.shopping.bean.req;

import com.alg.shopping.bean.base.BaseReqBean;

public class ForgetPwdReqBean extends BaseReqBean{
    public String password = "";//密码
    public String repassword = "";//重复密码
    public String smsCode = "";//验证码，
    public String tel = "";//电话号码
}
