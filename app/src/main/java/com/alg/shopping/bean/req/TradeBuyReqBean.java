package com.alg.shopping.bean.req;

import com.alg.shopping.bean.base.BaseReqBean;

public class TradeBuyReqBean extends BaseReqBean{
    public String singlePrice = "";
    public String count = "";
}
