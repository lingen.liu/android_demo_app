package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BaseResBean;

import java.util.List;

public class ShippingsResBean extends BaseResBean{
    public List<ShippingsDataBean> result;
}
