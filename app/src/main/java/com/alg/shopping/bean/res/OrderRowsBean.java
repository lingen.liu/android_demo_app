package com.alg.shopping.bean.res;

import java.io.Serializable;
import java.util.List;

public class OrderRowsBean implements Serializable{
    public String id = "";
    public String status = "";
    public String status_text = "";
    public String order_number = "";
    public String order_status = "";
    public String occurred_on = "";
    public String product_price = "";
    public String total_price = "";
    public String shipping_fee = "";
    public String guest_message = "";
    public String paymentTypeText = "";
    public String paymentType = "";
    public String bi_count = "";
    public String need_money = "";
    public String jifen_count = "";
    public String order_status_text = "";
    public AddressDataBean receive_info;
    public ShippingsBean shipping;
    public List<OrdersItemsBean> order_items;
    public List<OrdersItemsBean> items;
}
