package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BaseResBean;

public class VersionResBean extends BaseResBean{
    public String version = "";
    public String changelog = "";
    public String versionShort = "";
    public String build = "";
    public String install_url = "";
}
