package com.alg.shopping.bean.res;

import java.util.List;

public class HomeDataBean {
    public List<HomekjsBean> kjs;
    public List<HomekjsBean> jifens;
    public List<HomekjsBean> bis;
    public List<BannersBean> banners;
}
