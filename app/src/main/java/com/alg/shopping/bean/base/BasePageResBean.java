package com.alg.shopping.bean.base;

/**
 * Created by Administrator on 2018/1/9.
 */

public class BasePageResBean {
    String page = "";
    String pageSize = "";
    String pages = "";
    String total = "";

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
