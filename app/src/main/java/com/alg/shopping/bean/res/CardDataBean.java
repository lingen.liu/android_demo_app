package com.alg.shopping.bean.res;

import java.io.Serializable;

public class CardDataBean implements Serializable {
    public String id = "";
    public String realName = "";
    public String cardNumber = "";
    public String kh = "";
    public String ok = "";
    public String createDate = "";
    public PersionBean person;
}
