package com.alg.shopping.bean.res;


public class ConcDataBean{
    public String id = "";
    //数量
    public String count = "";
    //成交价
    public String price = "";
    //挂单时间
    public String createDate = "";
    //成交时间
    public String occDate = "";
    //交易方向BUY（买入） SELL（卖出）
    public String type = "";
    //买家ID
    public String buyerId = "";
    //卖家ID
    public String sellerId = "";

}
