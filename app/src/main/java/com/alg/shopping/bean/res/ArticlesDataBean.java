package com.alg.shopping.bean.res;


public class ArticlesDataBean{
    public String id;

    public String title;
    public String url;
    //子标题
    public String subTitle;

    //缩略图地址
    public String thumb;

    //原图或者原视频地址
    public String path;

    public String mediaType;

    //内容描述：单独用于文章
    public String description;

    //更新日期
    public String createDate;
}
