package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;
import com.alg.shopping.bean.base.BaseResBean;

import java.util.List;

public class ArticlesRowsBean extends BasePageResBean{
    public List<ArticlesDataBean> rows;
}
