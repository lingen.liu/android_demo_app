package com.alg.shopping.bean.base;

import java.io.Serializable;

public class BaseErrorBean implements Serializable {
    public String field = "";
    public String message = "";
}
