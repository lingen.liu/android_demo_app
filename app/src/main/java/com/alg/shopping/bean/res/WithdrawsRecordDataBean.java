package com.alg.shopping.bean.res;


public class WithdrawsRecordDataBean{
    public String id;

    //申请类型 1:商家申请 0：平台自动
    public int drawType;

    //申请金额
    public String requestPrice;

    //结算状态，REQUESTED（已申请），PAID（已结算）
    public String status;

    //打款方式 ，直接输出即可，没有枚举
    public String payType;

    //到账金额
    public String sendPrice;

    //打款时间
    public String sendTime;

    //申请时间
    public String createDate;

    //打款备注
    public String shopRemark;

    //银行卡信息,这里是null
//    public BankCardDto cardDto;

}
