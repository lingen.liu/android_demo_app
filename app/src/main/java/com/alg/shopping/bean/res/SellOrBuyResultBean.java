package com.alg.shopping.bean.res;


import java.io.Serializable;

public class SellOrBuyResultBean implements Serializable{
    //下单的ID
    public String id;
    public String count; //兑换数量
    public String type = "";//兑换类型 JP（竞拍）DH(兑换)
    public String create_user_id = "";

    public String total_amount = "";
    public String create_date = "";
    public String occ_date = "";

    //兑换行情价
    public String price ;
    public String create_user_name = "";
    public String limit_time = "";
    public String status = "";//PENDING //状态  PENDING(等待成交)  PENDED(已成交) CANCEL(已取消)

    public String user_id = "";
    public String user_name = "";
    public String tq_number = "";//权益编号
    public String td_number = "";////土地编号
    public String deal_amount = "";

}
