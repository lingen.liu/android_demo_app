package com.alg.shopping.bean.res;

import java.io.Serializable;

/**
 * Created by XB on 2018/3/15.
 */

public class AddressDataBean implements Serializable {
    public String id = "";
    public String receiver ="";
    public String mobile = "";
    public String province = "";
    public String city = "";
    public String borough = "";
    public String province_text = "";
    public String city_text = "";
    public String borough_text = "";
    public String address = "";
    public String json = "";
    public String is_default = "";
}
