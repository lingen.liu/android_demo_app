package com.alg.shopping.bean.res;

import java.util.List;

public class GoodDetailDataBean {
    public String id = "";
    public String productNumber = "";
    public String name = "";
    public String recommand = "";
    public String cate = "";
    public String originalPrice = "";
    public String unit = "";
    public String offSelves = "";
    public Object specs;
    public Object prices = "";
    public Object options = "";
    public int inventoryCount = 0;
    public String details = "";
    public PhotoBean photo;
    public List<PhotoBean> photos;
    public String type = "";
    public String sub_title = "";
    public String is_new = "";
    public String allow_stockout = "";
    public String jifen_price = "";
    public String bi_price = "";
    public String freight_price = "0";
}
