package com.alg.shopping.bean.res;


import java.util.List;

public class ExchangesDataBean{
    public List<ExchangesBean> buyOrders;
    public List<ExchangesBean> sellOrders;
}
