package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;

import java.util.List;

public class PromosDataBean extends BasePageResBean{
    public List<PromosRowsBean> rows;
}
