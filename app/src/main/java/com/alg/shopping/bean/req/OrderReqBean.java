package com.alg.shopping.bean.req;

import java.util.List;

public class OrderReqBean {
    public String guestMessage = "";
    public String receiverId = "";
    public String shippingId = "";
    public List<OrderItemBean> items;

}
