package com.alg.shopping.bean.res;

public class BannerBean {
    public String id = "";
    public String title = "";
    public String url = "";
    public String createDate = "";
    public PhotoBean photo;
}
