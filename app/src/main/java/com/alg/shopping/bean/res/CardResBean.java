package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BaseResBean;

import java.io.Serializable;

public class CardResBean extends BaseResBean implements Serializable{
    public CardDataBean result;
}
