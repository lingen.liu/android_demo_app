package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;


public class GainRecordRowsBean extends BasePageResBean{
    //领取人
    public String personId;

    public String tel;

    //领取时间，FORMAT yyyy-MM-dd
    public String createDate;

    //领取数量
    public int quantity;

    //余额奖励
    public String balance;

    //用户是否已经实名
    public boolean realAuth;

    //是什么类型 0:RMB 1：积分
    public int type;

}
