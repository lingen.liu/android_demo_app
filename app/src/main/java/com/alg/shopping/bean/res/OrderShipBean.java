package com.alg.shopping.bean.res;

import java.io.Serializable;

public class OrderShipBean implements Serializable{
    public String shipping_fee = "";
    public String shipping_num = "";
    public String shipping_name = "";
}
