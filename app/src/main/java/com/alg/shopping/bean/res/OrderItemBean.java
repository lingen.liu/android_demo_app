package com.alg.shopping.bean.res;

import java.io.Serializable;

public class OrderItemBean implements Serializable{
    public String subtotal = "";
    public String product_id = "";
    public String product_name = "";
    public String product_status = "";
    public String product_unit = "";
    public String unit_price = "";
    public String product_type = "";
    public String product_jifen_price = "";
    public String product_bi_price = "";
    public PhotoBean productPhoto;

}
