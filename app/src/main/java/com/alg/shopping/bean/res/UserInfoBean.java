package com.alg.shopping.bean.res;

public class UserInfoBean {
    public String id = "";
    public String name = "";
    public String tel = "";
    public String role = "";
    public String token = "";
    public String user_type = "";
    public String recommand_code = "";
}
