package com.alg.shopping.bean.base;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2018/1/9.
 */

public class BaseResBean implements Serializable {
    public String msg = "";
    public int status = 0;
    public String url = "";
    public String errorMsg = "";
    public String errorCode = "";
    public ArrayList<BaseErrorBean> fieldErrors;
}
