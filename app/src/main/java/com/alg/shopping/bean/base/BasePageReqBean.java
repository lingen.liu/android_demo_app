package com.alg.shopping.bean.base;


import com.alg.shopping.contants.Constants;

/**
 * Created by Administrator on 2018/1/9.
 */

public class BasePageReqBean extends BaseReqBean{
    public int skip = 1;
    public int rows = Constants.PAGENUM;
}
