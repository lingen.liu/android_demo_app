package com.alg.shopping.bean.res;

import java.io.Serializable;

public class PhotoBean implements Serializable{
    public String id = "";
    public String name = "";
    public String path = "";
    public String thumb = "";
    public String description = "";
    public String sort_order = "";
}
