package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;


public class SubscribeRecordRowsBean extends BasePageResBean{
    //下单的ID
    public String id = "";

    public String count; //兑换数量
    public String type = "";//兑换类型 JP（竞拍）DH(兑换)
    //兑换行情价
    public String price ;

    public String phone = "";
    //兑换总价
    public String total_amount;
    //状态  PENDING(等待成交)  PENDED(已成交) CANCEL(已取消)
    public String status;
    public String user_id = "";
    public String user_name = "";
    public String tq_number = "";//权益编号
    public String td_number = "";////土地编号
    public String deal_amount = "";
    //完成时间
    public String occ_date;

    public String create_date;
}
