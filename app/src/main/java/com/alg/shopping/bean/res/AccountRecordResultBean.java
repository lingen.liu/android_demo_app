package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;

import java.util.List;

public class AccountRecordResultBean extends BasePageResBean{
    public List<AccountRecordRowsBean> rows;

}
