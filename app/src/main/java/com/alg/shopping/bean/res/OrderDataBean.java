package com.alg.shopping.bean.res;

import java.io.Serializable;
import java.util.List;

public class OrderDataBean implements Serializable{
    public String id = "";
    public String paymentType = "";
    public String paymentTypeText = "";
    public String order_number = "";
    public String order_status = "";
    public String order_status_text = "";
    public String create_date = "";
    public String occurred_on = "";
    public String total_price = "";
    public String guest_message = "";
//    public Object order_phrases;
    public String need_money = "";
    public String bi_count = "";
    public String jifen_count = "";
    public List<OrderItemBean> order_items;
    public AddressDataBean receive_info;
    public OrderShipBean shipping;
}
