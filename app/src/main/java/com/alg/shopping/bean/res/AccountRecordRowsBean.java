package com.alg.shopping.bean.res;

import com.alg.shopping.bean.base.BasePageResBean;


public class AccountRecordRowsBean extends BasePageResBean{
    //领取人ID
    public String receiptor_id;

    public String receiptor_tel;

    //自己领取或者推荐领取
    public String rec_type;

    //0 :余额，1积分
    public int type = 0;

    public String show_id;

    public String who_tel;

    //数量
    public String quantity;

    public String create_date;

}
