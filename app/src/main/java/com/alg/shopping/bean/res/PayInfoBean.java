package com.alg.shopping.bean.res;


public class PayInfoBean {
    public String appId = "";
    public String partnerId = "";
    public String prepayId = "";
    public String nonce = "";
    public String timestamp = "";
    public String signature = "";
    public String pack = "";
}
