package com.alg.shopping.bean.res;

import java.util.List;

public class PromosRowsBean {
    public String personId = "";
    public String tel = "";
    public String createDate = "";
    public String quantity = "";
    public String balance = "";
    public String realAuth = "";
    public String type = "";
    public List<PromosRowsBean> subItems;

}
