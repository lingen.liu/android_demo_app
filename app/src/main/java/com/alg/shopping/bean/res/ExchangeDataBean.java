package com.alg.shopping.bean.res;


public class ExchangeDataBean{
    //买入手续费
    public String buySxf;
    //卖出手续费
    public String sellSxf;
    //行情价
    public String price;
    //我的积分余额
    public String jifenCount;
    //农合链余额
    public String nhjCount;
    //限卖比例
    public String sellPercent;

}
