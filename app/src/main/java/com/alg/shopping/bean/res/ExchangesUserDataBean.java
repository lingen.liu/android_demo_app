package com.alg.shopping.bean.res;


public class ExchangesUserDataBean{
    //挂单ID
    public String id = "";
    //用户
    public String guestId = "";
    //预计成交数，即：挂单数
    public String count = "";
    //实际成交数
    public String realCount = "";
    //状态，挂单中，已成交，已撤销
    public String status = "";
    //成交价
    public String price = "";
    //挂单时间
    public String createDate = "";
    //类型 BUY（买） SELL（卖）
    public String type = "";

    public String limit_time = "";//到期时间
}
