package com.alg.shopping.net;

import android.content.Context;
import android.text.TextUtils;

import com.alg.shopping.bean.base.BasePageReqBean;
import com.alg.shopping.bean.base.BaseReqBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.req.AcountReqBean;
import com.alg.shopping.bean.req.AddBankCardResBean;
import com.alg.shopping.bean.req.ArticlesReqBean;
import com.alg.shopping.bean.req.CancleTradeReqBean;
import com.alg.shopping.bean.req.EditAddressReqBean;
import com.alg.shopping.bean.req.ForgetPwdReqBean;
import com.alg.shopping.bean.req.OrderItemBean;
import com.alg.shopping.bean.req.OrderReqBean;
import com.alg.shopping.bean.req.QueryCodeReqBean;
import com.alg.shopping.bean.req.RegistReqBean;
import com.alg.shopping.bean.req.SgAlgReqBean;
import com.alg.shopping.bean.req.TradeBuyReqBean;
import com.alg.shopping.bean.req.UserInfoReqBean;
import com.alg.shopping.bean.req.WithDrawApplyReqBean;
import com.alg.shopping.bean.res.AccountRecordResBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.AddressCurrResBean;
import com.alg.shopping.bean.res.AddressResBean;
import com.alg.shopping.bean.res.ArticleDescResBean;
import com.alg.shopping.bean.res.ArticlesResBean;
import com.alg.shopping.bean.res.BalanceResBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.bean.res.CityResBean;
import com.alg.shopping.bean.res.ConcResBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.bean.res.ExchangesResBean;
import com.alg.shopping.bean.res.ExchangesUserResBean;
import com.alg.shopping.bean.res.GainRecordResBean;
import com.alg.shopping.bean.res.GoodDetailResBean;
import com.alg.shopping.bean.res.HomeResBean;
import com.alg.shopping.bean.res.OrderDetailResBean;
import com.alg.shopping.bean.res.OrderResBean;
import com.alg.shopping.bean.res.OrdersResBean;
import com.alg.shopping.bean.res.PayResBean;
import com.alg.shopping.bean.res.PromoResBean;
import com.alg.shopping.bean.res.PromosResBean;
import com.alg.shopping.bean.res.SellOrBuyResBean;
import com.alg.shopping.bean.res.SgAlgResBean;
import com.alg.shopping.bean.res.SgResBean;
import com.alg.shopping.bean.res.ShippingsResBean;
import com.alg.shopping.bean.res.SubscribeRecordResBean;
import com.alg.shopping.bean.res.UserInfoResBean;
import com.alg.shopping.bean.res.VersionResBean;
import com.alg.shopping.bean.res.WithdrawsRecordResBean;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;

import java.util.List;

/**
 * Created by Lenovo on 2017/12/13.
 */
public class NetParamets {
    /**
     * 登录
     *
     * @param context
     * @param username
     * @param password
     * @param callback
     */
    public static void login(Context context, String username, String password, NetCallBack<UserInfoResBean> callback) {
        UserInfoReqBean mUserInfoReqBean = new UserInfoReqBean();
        mUserInfoReqBean.username = username;
        mUserInfoReqBean.password = password;
        new NetUtils().postHttp(context, NetConstants.MSGQuery, callback, mUserInfoReqBean, UserInfoResBean.class, Constants.USERINFOKEY);
    }

    /**
     * 注册
     *
     * @param context
     * @param nick_name
     * @param tel
     * @param promoter_code
     * @param sms_code
     * @param password
     * @param repassword
     * @param callback
     */
    public static void regist(Context context, String nick_name, String tel, String promoter_code, String sms_code, String password, String repassword, NetCallBack<BaseResBean> callback) {
        RegistReqBean mRegistReqBean = new RegistReqBean();
        mRegistReqBean.nick_name = nick_name;
        mRegistReqBean.tel = tel;
        mRegistReqBean.promoter_code = promoter_code;
        mRegistReqBean.sms_code = sms_code;
        mRegistReqBean.password = password;
        mRegistReqBean.repassword = repassword;
        new NetUtils().postHttp(context, NetConstants.regiest, callback, mRegistReqBean, BaseResBean.class, Constants.USERINFOKEY);
    }

    /**
     * 获取短信验证码
     *
     * @param context
     * @param phone
     * @param callback
     */
    public static void queryCode(Context context, String phone, NetCallBack<BaseResBean> callback) {
        QueryCodeReqBean mQueryCodeReqBean = new QueryCodeReqBean();
        mQueryCodeReqBean.tel = phone;
        new NetUtils().postHttp(context, NetConstants.regiest_code, callback, mQueryCodeReqBean, BaseResBean.class, "");
    }
    /**
     * 首页数据
     *
     * @param context
     * @param callback
     */
    public static void products(Context context, NetCallBack<HomeResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.products, callback, null, HomeResBean.class, Constants.HOMEDATAKEY);
    }

    /**
     * 获取个人信息和账户信息
     *
     * @param context
     * @param callback
     */
    public static void account(Context context, NetCallBack<AccountResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.account, callback, null, AccountResBean.class, Constants.ACCOUNTDATAKEY);
    }

    /**
     * 查询当前用户绑定的银行卡
     *
     * @param context
     * @param callback
     */
    public static void card(Context context, NetCallBack<CardResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.card, callback, null, CardResBean.class, "");
    }

    /**
     * 添加或修改一张银行卡
     *
     * @param context
     * @param id
     * @param realName
     * @param cardNumber
     * @param kh
     * @param callback
     */
    public static void cardAdd(Context context, String id, String realName, String cardNumber, String kh, NetCallBack<BaseResBean> callback) {
        AddBankCardResBean mAddBankCardResBean = new AddBankCardResBean();
        mAddBankCardResBean.id = id;
        mAddBankCardResBean.realName = realName;
        mAddBankCardResBean.cardNumber = cardNumber;
        mAddBankCardResBean.kh = kh;
        new NetUtils().postHttp(context, NetConstants.card, callback, mAddBankCardResBean, BaseResBean.class, "");
    }

    /**
     * 获取我的推荐
     *
     * @param context
     * @param callback
     */
    public static void promo(Context context, NetCallBack<PromoResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.promo, callback, null, PromoResBean.class, "");
    }

    /**
     * 查询用户推荐关系
     *
     * @param context
     * @param callback
     */
    public static void promos(Context context, int page, NetCallBack<PromosResBean> callback) {
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.promos, callback, mBasePageReqBean, PromosResBean.class, "");
    }

    /**
     * 获取商品详情
     *
     * @param context
     * @param id
     * @param callback
     */
    public static void details(Context context, String id, NetCallBack<GoodDetailResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.getDetails(id), callback, null, GoodDetailResBean.class, "");
    }

    /**
     * 下单
     *
     * @param context
     * @param callback
     */
    public static void order(Context context, String guestMessage, String receiverId, String shippingId, List<OrderItemBean> items, NetCallBack<OrderResBean> callback) {
        OrderReqBean mOrderReqBean = new OrderReqBean();
        mOrderReqBean.guestMessage = guestMessage;
        mOrderReqBean.receiverId = receiverId;
        mOrderReqBean.shippingId = shippingId;
        mOrderReqBean.items = items;
        new NetUtils().postHttp(context, NetConstants.order, callback, mOrderReqBean, OrderResBean.class, "");
    }
    public static void cancleOrder(Context context, String order_id, NetCallBack<BaseResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.order_cancle(order_id), callback, null, BaseResBean.class, "");
    }
    /**
     * 查询我的订单列表
     *
     * @param context
     * @param callback
     */
    public static void orders(Context context, int page, String status, NetCallBack<OrdersResBean> callback) {
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.getOrders(status), callback, mBasePageReqBean, OrdersResBean.class, "");
    }

    /**
     * 查询当前用户的收货地址
     *
     * @param context
     * @param callback
     */
    public static void currAddress(Context context, NetCallBack<AddressCurrResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.receive, callback, null, AddressCurrResBean.class, "");
    }

    /**
     * 查询收货地址列表
     *
     * @param context
     * @param callback
     */
    public static void queryAddress(Context context, NetCallBack<AddressResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.receives, callback, new BaseReqBean(), AddressResBean.class, "");
    }

    /**
     * 编辑或新增收货地址
     */
    public static void editAddress(Context context, String id, String receiver, String mobile, String addr, String isDefault, String province, String city, String borough, NetCallBack<AddressCurrResBean> callback) {
        EditAddressReqBean mEditAddressReqBean = new EditAddressReqBean();
        mEditAddressReqBean.id = id;
        mEditAddressReqBean.receiver = receiver;
        mEditAddressReqBean.mobile = mobile;
        mEditAddressReqBean.province = province;
        mEditAddressReqBean.city = city;
        mEditAddressReqBean.borough = borough;
        mEditAddressReqBean.address = addr;
        mEditAddressReqBean.is_default = isDefault;
        new NetUtils().postHttp(context, NetConstants.receive, callback, mEditAddressReqBean, AddressCurrResBean.class, "");
    }

    /**
     * 查询省市区
     *
     * @param context
     * @param province
     * @param city
     * @param callback
     */
    public static void queryAddress(Context context, String province, String city, NetCallBack<CityResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.getCitys(province, city), callback, null, CityResBean.class, "");
    }

    /**
     * 查询配送方式
     *
     * @param context
     * @param callback
     */
    public static void shippings(Context context, NetCallBack<ShippingsResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.shippings, callback, null, ShippingsResBean.class, "");
    }

    /**
     * 根据订单，获取支付方式
     *
     * @param context
     * @param order_number
     * @param paymentType
     * @param callback
     */
    public static void pay(Context context, String order_number, String paymentType, NetCallBack<PayResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.pay(order_number, paymentType), callback, null, PayResBean.class, "");
    }
    /**
     * 查询银行开户行
     *
     * @param context
     * @param callback
     */
    public static void bankkh(Context context, NetCallBack<BaseResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.bank, callback, null, BaseResBean.class, "");
    }

    /**
     * 查询交易中心配置数据
     *
     * @param context
     * @param callback
     */
    public static void trade_config(Context context, NetCallBack<ExchangeResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.trade_config, callback, null, ExchangeResBean.class, "");
    }
    /**
     * 查询挂的买单和卖单
     *
     * @param context
     * @param callback
     */
    public static void trade_exchanges(Context context, NetCallBack<ExchangesResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.trade_exchanges, callback, null, ExchangesResBean.class, "");
    }

    /**
     * 查询我的委托
     * @param context
     * @param page
     * @param callback
     */
    public static void user_exchange(Context context, int page,NetCallBack<ExchangesUserResBean> callback) {
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.user_exchange, callback, mBasePageReqBean, ExchangesUserResBean.class, "");
    }
    /**
     * 我的成交记录
     *
     * @param context
     * @param callback
     */
    public static void conc_record(Context context, int page,NetCallBack<ConcResBean> callback) {
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.conc_record, callback, mBasePageReqBean, ConcResBean.class, "");
    }
    /**
     * 撤销挂单
     *
     * @param context
     * @param callback
     */
    public static void UndoTrade(Context context, String tradeId,NetCallBack<BaseResBean> callback) {
        CancleTradeReqBean mCancleTradeReqBean = new CancleTradeReqBean();
        mCancleTradeReqBean.id = tradeId;
        new NetUtils().postHttp(context, NetConstants.trade_cancel, callback, mCancleTradeReqBean, BaseResBean.class, "");
    }

    /**
     * 挂买单
     * status
     * 502 挂单量超过限制
     503 不允许买入
     504 挂单数不符合要求
     505 挂单价有误，必须是当前行情价
     506 交易已经关闭，不在开盘时间内。
     507 交易积分不足。系统描述返回的是2008
     * @param context
     * @param singlePrice 挂单价
     * @param count 挂单数
     * @param callback
     */
    public static void trade_buy(Context context, String singlePrice,String count,NetCallBack<BaseResBean> callback) {
        TradeBuyReqBean mTradeBuyReqBean = new TradeBuyReqBean();
        mTradeBuyReqBean.singlePrice = singlePrice;
        mTradeBuyReqBean.count = count;
        new NetUtils().postHttp(context, NetConstants.trade_buy, callback, mTradeBuyReqBean, BaseResBean.class, "");
    }
    /**
     * 挂卖单
     * status
     * 502 挂单量超过限制
         503 不允许买入
         504 挂单数不符合要求
         505 挂单价有误，必须是当前行情价
         506 交易已经关闭，不在开盘时间内。
         507 交易积分不足。系统描述返回的是2008
     * @param context
     * @param singlePrice 挂单价
     * @param count 挂单数
     * @param callback
     */
    public static void trade_sell(Context context, String singlePrice,String count,NetCallBack<BaseResBean> callback) {
        TradeBuyReqBean mTradeBuyReqBean = new TradeBuyReqBean();
        mTradeBuyReqBean.singlePrice = singlePrice;
        mTradeBuyReqBean.count = count;
        new NetUtils().postHttp(context, NetConstants.trade_sell, callback, mTradeBuyReqBean, BaseResBean.class, "");
    }
    /**
     * 查询当前可提现余额
     *
     * @param context
     * @param callback
     */
    public static void balance(Context context, NetCallBack<BalanceResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.balance, callback, null, BalanceResBean.class, "");
    }
    /**
     * 查询提现详情
     * @param context
     * @param id
     * @param callback
     */
    public static void balance_details(Context context, String id,NetCallBack<BaseResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.balance_details(id), callback, null, BaseResBean.class, "");
    }
    /**
     * 获取提现记录
     * @param context
     * @param page
     * @param callback
     */
    public static void withdraws_record(Context context, int page,NetCallBack<WithdrawsRecordResBean> callback) {
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.withdraws_record, callback, mBasePageReqBean, WithdrawsRecordResBean.class, "");
    }

    /**
     *  申请提现
     * @param context
     * @param price  100的整数倍，必须判断，单笔最高5万
     * @param callback
     */
    public static void withdraw_apply(Context context, int price,NetCallBack<BaseResBean> callback) {
        WithDrawApplyReqBean mWithDrawApplyReqBean = new WithDrawApplyReqBean();
        mWithDrawApplyReqBean.price = price;
        new NetUtils().postHttp(context, NetConstants.withdraw_apply, callback, mWithDrawApplyReqBean, BaseResBean.class, "");
    }
    /**
     * 订单详情接口
     * @param context
     * @param order_id
     * @param callback
     */
    public static void order_details(Context context, String order_id,NetCallBack<OrderDetailResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.order_details(order_id), callback, null, OrderDetailResBean.class, "");
    }

    /**
     * 咨询列表接口
     * @param context
     * @param page
     * @param type
     * @param callback
     */
    public static void articles(Context context, int page,String type,NetCallBack<ArticlesResBean> callback) {
        ArticlesReqBean mArticlesReqBean = new ArticlesReqBean();
        mArticlesReqBean.skip = page * Constants.PAGENUM;
        mArticlesReqBean.key = type;
        new NetUtils().postHttp(context, NetConstants.articles, callback, mArticlesReqBean, ArticlesResBean.class, "");
    }
    /**
     * 咨询详情
     * @param context
     * @param article_id
     * @param callback
     */
    public static void article_desc(Context context, String article_id,NetCallBack<ArticleDescResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.article_desc+article_id, callback, null, ArticleDescResBean.class, "");
    }

    /**忘记密码
     *
     * @param context
     * @param password
     * @param repassword
     * @param smsCode
     * @param tel
     * @param callback
     */
    public static void forget_password(Context context, String password, String repassword, String smsCode, String tel  ,NetCallBack<BaseResBean> callback) {
        ForgetPwdReqBean mForgetPwdReqBean = new ForgetPwdReqBean();
        mForgetPwdReqBean.password = password;
        mForgetPwdReqBean.repassword = repassword;
        mForgetPwdReqBean.smsCode = smsCode;
        mForgetPwdReqBean.tel= tel;
        new NetUtils().postHttp(context, NetConstants.forget_password, callback, mForgetPwdReqBean, BaseResBean.class, "");
    }
    /**
     * 忘记密码获取短信验证码
     *
     * @param context
     * @param phone
     * @param callback
     */
    public static void forget_password_code(Context context, String phone, NetCallBack<BaseResBean> callback) {
        QueryCodeReqBean mQueryCodeReqBean = new QueryCodeReqBean();
        mQueryCodeReqBean.tel = phone;
        new NetUtils().postHttp(context, NetConstants.forget_password_code, callback, mQueryCodeReqBean, BaseResBean.class, "");
    }

    /**
     * 我的收益明细
     * @param context
     * @param page
     * @param callback
     */

    public static void recordRewards(Context context, int page,NetCallBack<GainRecordResBean> callback){
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.recordRewards, callback, mBasePageReqBean, GainRecordResBean.class, "");
    }
    /**
     * 我的账户明细
     * @param context
     * @param page
     * @param callback
     */

    public static void accountRecord(Context context, int page,String type,NetCallBack<AccountRecordResBean> callback){
        AcountReqBean mAcountReqBean = new AcountReqBean();
        mAcountReqBean.skip = page * Constants.PAGENUM;
        String url = NetConstants.account_records;
        if(!TextUtils.isEmpty(type)){
            url = url+"?type="+type;
        }
        new NetUtils().postHttp(context, url, callback, mAcountReqBean, AccountRecordResBean.class, "");
    }
    public static void deliveredOrder(Context context, String order_id, NetCallBack<BaseResBean> callback) {
        new NetUtils().getHttp(context, NetConstants.order_delivered(order_id), callback, null, BaseResBean.class, "");
    }

    public static void versionUpdate(Context context,String id,String api_token,NetCallBack<VersionResBean> callback){
        new NetUtils().getHttp(context, "http://api.fir.im/apps/latest/"+id+"?api_token="+api_token, callback, null, VersionResBean.class, "");
    }
    public static void shenggoualg(Context context, String count,NetCallBack<SgResBean> callback){
        SgAlgReqBean mSgAlgReqBean = new SgAlgReqBean();
        mSgAlgReqBean.count = count;
        new NetUtils().postHttp(context, NetConstants.shenggoualg, callback, mSgAlgReqBean, SgResBean.class, "");
    }
    /**
     * 挂卖单
     * status
     * 502 挂单量超过限制
     503 不允许买入
     504 挂单数不符合要求
     505 挂单价有误，必须是当前行情价
     506 交易已经关闭，不在开盘时间内。
     507 交易积分不足。系统描述返回的是2008
     * @param context
     * @param singlePrice 挂单价
     * @param count 挂单数
     * @param callback
     */
    public static void sell(Context context, String singlePrice,String count,NetCallBack<SellOrBuyResBean> callback) {
        TradeBuyReqBean mTradeBuyReqBean = new TradeBuyReqBean();
        mTradeBuyReqBean.singlePrice = singlePrice;
        mTradeBuyReqBean.count = count;
        new NetUtils().postHttp(context, NetConstants.sell, callback, mTradeBuyReqBean, SellOrBuyResBean.class, "");
    }
    public static void buy(Context context, String singlePrice,String count,NetCallBack<SellOrBuyResBean> callback) {
        TradeBuyReqBean mTradeBuyReqBean = new TradeBuyReqBean();
        mTradeBuyReqBean.singlePrice = singlePrice;
        mTradeBuyReqBean.count = count;
        new NetUtils().postHttp(context, NetConstants.buy, callback, mTradeBuyReqBean, SellOrBuyResBean.class, "");
    }
    public static void subscribe_records(Context context, int type,int page,NetCallBack<SubscribeRecordResBean> callback){
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.subscribe_records(type), callback, mBasePageReqBean, SubscribeRecordResBean.class, "");
    }
    public static void equity_records(Context context, int type,int page,NetCallBack<SubscribeRecordResBean> callback){
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.equity_records(type), callback, mBasePageReqBean, SubscribeRecordResBean.class, "");
    }
    public static void equity_center(Context context,int page,NetCallBack<SubscribeRecordResBean> callback){
        BasePageReqBean mBasePageReqBean = new BasePageReqBean();
        mBasePageReqBean.skip = page * Constants.PAGENUM;
        new NetUtils().postHttp(context, NetConstants.equity_center, callback, mBasePageReqBean, SubscribeRecordResBean.class, "");
    }
    public static void equity_apply(Context context,NetCallBack<SgAlgResBean> callback){
        new NetUtils().postHttp(context,NetConstants.equity_apply,callback,null,SgAlgResBean.class,"");
    }
    public static void details_order(Context context,String id,NetCallBack<SellOrBuyResBean> callback){
        new NetUtils().getHttp(context,NetConstants.details_order(id),callback,null,SellOrBuyResBean.class,"");
    }
    public static void exchange_details(Context context,String id,NetCallBack<SellOrBuyResBean> callback){
        new NetUtils().getHttp(context,NetConstants.exchange_details(id),callback,null,SellOrBuyResBean.class,"");
    }
    public static void exchange(Context context,String id,NetCallBack<SgResBean> callback){
        new NetUtils().postHttp(context,NetConstants.exchange(id),callback,null,SgResBean.class,"");
    }
    public static void details_equity(Context context,String id,NetCallBack<SellOrBuyResBean> callback){
        new NetUtils().getHttp(context,NetConstants.details_equity(id),callback,null,SellOrBuyResBean.class,"");
    }

}
