package com.alg.shopping.net;

import android.text.TextUtils;

/**
 * Created by Lenovo on 2017/12/13.
 */

public class NetConstants {
//    public static String baseUrl = "http://test.lsswxx.com/";
    public static String baseUrl = "http://www.ainonggul.com/";
    public static String products = baseUrl+"api/index/products";//获取首页数据
    public static String MSGQuery = baseUrl+"api/login";//登录接口
    public static String regiest = baseUrl+"api/regist";
    public static String regiest_code = baseUrl+"api/sms";
    public static String receives = baseUrl+"api/receives";//查询收货地址列表
    public static String receive = baseUrl+"api/receive";//保存收货地址、查询当前收货地址
    public static String account = baseUrl+"api/user/account";//获取个人信息和账户信息
    public static String card = baseUrl + "api/user/card";//查询当前用户绑定的银行卡、添加银行卡
    public static String promo = baseUrl + "api/user/promo";//获取我的推荐
    public static String promos = baseUrl + "api/user/promos";//查询用户推荐关系
    public static String getDetails(String id) {//获取商品详情
        return baseUrl + "api/product/"+id+"/details";
    }
    public static String order = baseUrl + "api/order";//下订单
    public static String getOrders(String status) {//查询我的订单列表
        return baseUrl + "api/orders?status="+status;
    }
    public static String getCitys(String province,String city) {//查询省市区
        String citys = baseUrl + "api/citys";
        if(!TextUtils.isEmpty(province)){
            citys = citys+"?province="+province;
        }else if(!TextUtils.isEmpty(city)){
            citys = citys+"?city="+city;
        }
        return citys;
    }
    public static String shippings = baseUrl+"api/shippings";//查询配送方式
    public static String pay(String order_number,String paymentType) {//根据订单，获取支付方式
        return baseUrl + "api/order/"+order_number+"/request/pay?pt="+paymentType;
    }
    public static String bank = baseUrl+"api/shippings";//查询银行开户行
    public static String trade_config = baseUrl+"api/exchange";//获取交易中心的配置数据
    public static String trade_exchanges = baseUrl+"api/exchanges";//查询挂的买单和卖单
    public static String user_exchange = baseUrl+"api/user/exchanges";//查询我的委托
    public static String conc_record = baseUrl+"api/user/exchange/success";//我的成交记录
    public static String trade_cancel = baseUrl+"api/user/exchange/order/cancel";//撤销挂单


    public static String trade_buy = baseUrl+"api/user/exchange/order/buy";//挂买单
    public static String trade_sell = baseUrl+"api/user/exchange/order/sell";//挂卖单
    public static String balance = baseUrl+"api/withdraw/balance";//查询当前可提现余额
    public static String balance_details(String id){//查询提现详情
         return baseUrl+"api/withdraw/"+id+"/details";
    }
    public static String withdraws_record = baseUrl+"api/withdraws";//获取提现记录
    public static String withdraw_apply = baseUrl+"api/withdraw";//申请提现
    public static String order_details(String id){//订单详情
        return baseUrl+"api/order/"+id+"/details";
    }
    public static String order_cancle(String id){//订单取消
        return baseUrl+"api/order/"+id+"/cancel";
    }
    public static String articles = baseUrl+"api/articles";//咨询列表接口

    public static String article_desc = baseUrl+"api/article/";//咨询详情
    public static String forget_password = baseUrl+"api/forget/password";//忘记密码
    public static String forget_password_code = baseUrl+"api/forget/sms";//忘记密码获取验证码
    public static String recordRewards = baseUrl+"api/my/recordRewards";//我的收益明细
    public static String account_records = baseUrl+"api/my/account/records";//我的账户明细
    public static String order_delivered(String id){//签收
        return baseUrl+"api/order/"+id+"/delivered";
    }

    public static String protocol = "api/protocol";//用户服务协议

    public static String shenggoualg = baseUrl+"api/buy/request";//申购农合链下单
    public static String sell = baseUrl+"api/user/exchange/order/sell/qr";//点对点下单-卖
    public static String buy = baseUrl+"api/user/exchange/order/buy/qr";//点对点下单-买
    public static String equity_center = baseUrl+"api/sell/center/orders";//权益中心列表

    public static String equity_apply = baseUrl+"api/sell/request";//土地权益申请

    public static String subscribe_records(int type){//我的申购记录
        if(type == 0){
            return baseUrl+"api/buy/orders?t=PENDING";
        }else{
            return baseUrl+"api/buy/orders?t=PENDED";
        }
    }
    public static String equity_records(int type){//我的土地权益记录
        if(type == 0){
            return baseUrl+"api/sell/orders?t=PENDED";
        }else{
            return baseUrl+"api/sell/orders?t=COMPLETED";
        }
    }

    public static String details_equity(String id){//权益详情
        return baseUrl+"api/sell/order/"+id+"/details";
    }
    public static String details_order(String id){//查询申购详情
        return baseUrl+"api/buy/order/"+id+"/details";
    }
    public static String exchange_details(String id){//根据二维码中的ID查询详情
        return baseUrl+"api/exchange/order/"+id;
    }
    public static String exchange(String id){//换入换出
        return baseUrl+"api/exchange/order/"+id+"/deal";
    }
}