package com.alg.shopping.net;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.sql.CacheSqlDao;
import com.alg.shopping.tools.NetStateUtil;
import com.alg.shopping.tools.NormalUtils;
import com.google.gson.Gson;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * Created by Lenovo on 2017/12/13.
 */
public class NetUtils {
    /**
     * 网络请求Post
     *
     * @param context     上下文
     * @param url         请求地址
     * @param callback    回调
     * @param obj         上传参数
     * @param tclass      接收类型
     * @param cacheKey    是否缓存
     * @param <T>
     */
    public <T> void postHttp(final Context context, final String url, final NetCallBack<T> callback, Object obj, final Class<T> tclass, final String cacheKey) {
        final RequestParams mRequestParams = new RequestParams(url);
        mRequestParams.setConnectTimeout(1 * 1000 * 15);
        mRequestParams.setReadTimeout(1 * 1000 * 15);
        mRequestParams.addHeader("Content-Type", "application/json");
        mRequestParams.addHeader("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.4; SAMSUNG-SM-N900A Build/tt) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36");
        String token = NormalUtils.getLoginUserToken();
        if(!TextUtils.isEmpty(token)){
            mRequestParams.addHeader("token", token);
        }
        if (obj != null) {
            String json = new Gson().toJson(obj);
            NormalUtils.LogI(token+"——请求：key-value请求接口:" + url +"?"+ json);
            mRequestParams.setBodyContent(json);
        }else{
            NormalUtils.LogI(token+"——请求：key-value请求接口:"+token + url);
        }
        boolean isNetConnected = NetStateUtil.isNetConnetced();
        if(!isNetConnected && callback != null){
            callback.backError("网络连接失败", null);
            return;
        }
        x.http().post(mRequestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                successParse(context,url,mRequestParams,result,cacheKey,tclass,callback);
            }
            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                String exStr = "";
                if(ex != null){
                    exStr = ex.toString();
                    if(exStr.contains("errorCode: ")){
                        exStr = "服务器错误";
                    }
                }
                failParse(context,url,mRequestParams,exStr,cacheKey,tclass,callback);
            }
            @Override
            public void onCancelled(CancelledException cex) {
            }
            @Override
            public void onFinished() {
            }
        });
    }
    /**
     * 网络请求Post
     *
     * @param context     上下文
     * @param url         请求地址
     * @param callback    回调
     * @param obj         上传参数
     * @param tclass      接收类型
     * @param cacheKey    是否缓存
     * @param <T>
     */
    public <T> void getHttp(final Context context, final String url, final NetCallBack<T> callback, Object obj, final Class<T> tclass, final String cacheKey) {
        final RequestParams mRequestParams = new RequestParams(url);
        mRequestParams.setConnectTimeout(1 * 1000 * 10);
        mRequestParams.setReadTimeout(1 * 1000 * 10);
        mRequestParams.setMaxRetryCount(1);
        mRequestParams.addHeader("Content-Type", "application/json");
        mRequestParams.addHeader("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.4; SAMSUNG-SM-N900A Build/tt) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36");
        String token = NormalUtils.getLoginUserToken();
        if(!TextUtils.isEmpty(token)){
            mRequestParams.addHeader("token", token);
        }
        if(obj != null){
            String json = new Gson().toJson(obj);
            NormalUtils.LogI(token+"——请求：key-value请求接口:" + url +"?"+ json);
            mRequestParams.setBodyContent(json);
        }else{
            NormalUtils.LogI(token+"——请求：key-value请求接口:" + url);
        }
        boolean isNetConnected = NetStateUtil.isNetConnetced();
        if(!isNetConnected && callback != null){
            callback.backError("网络连接失败", null);
            return;
        }
        x.http().get(mRequestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                successParse(context,url,mRequestParams,result,cacheKey,tclass,callback);
            }
            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                String exStr = "";
                if(ex != null){
                    exStr = ex.toString();
                    if(exStr.contains("errorCode: ")){
                        exStr = "服务器连接失败";
                    }
                }
                failParse(context,url,mRequestParams,exStr,cacheKey,tclass,callback);
            }
            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    private void resultLog(String url, RequestParams mRequestParams, String result){
        NormalUtils.LogI("数据返回结果:" + result);
    }
    private <T>void failParse(Context context, String url, RequestParams mRequestParams, String exStr, String cacheKey, Class<T> tclass, NetCallBack<T> callback){
        resultLog(url,mRequestParams,exStr);
        boolean isFinish = checkActIsFinish(context);
        if(isFinish){
            return;
        }
        if(exStr.contains("Exception")){
            exStr = "网络链接错误";
        }
        T temp;
        if (TextUtils.isEmpty(cacheKey)) {
            temp = null;
        } else {
            String cacheStr = CacheSqlDao.getInance().query(cacheKey);
            temp = new Gson().fromJson(cacheStr, tclass);
        }
        if (callback != null) {
            callback.backError(exStr, temp);
        }
    }
    private <T>void successParse(Context context, String url, RequestParams mRequestParams, String result, String cacheKey, Class<T> tclass, NetCallBack<T> callback){
        resultLog(url,mRequestParams,result);
        boolean isFinish = checkActIsFinish(context);
        if(isFinish){
            return;
        }
        BaseResBean mBaseResBean = new Gson().fromJson(result, BaseResBean.class);
        if (!TextUtils.isEmpty(cacheKey) && mBaseResBean != null && mBaseResBean.status == 200) {
            CacheSqlDao.getInance().insert(cacheKey, result);
        }
        if (callback != null && mBaseResBean != null) {
            if(mBaseResBean.status == 200 || url.contains("http://api.fir.im/apps/latest")){
                T t = new Gson().fromJson(result, tclass);
                callback.backSuccess(t);
            }else if(mBaseResBean.status == 511){
                NormalUtils.customShowToast(mBaseResBean.msg);
                if(url.contains(NetConstants.user_exchange) || url.contains(NetConstants.promo) || url.contains(NetConstants.trade_config)){

                }else{
                    NormalUtils.jumpToLogin(context);
                }
                callback.backError(mBaseResBean.msg,null);
            }else{
                if(!TextUtils.isEmpty(cacheKey)){
                    String cacheStr = CacheSqlDao.getInance().query(cacheKey);
                    T tTemp = new Gson().fromJson(cacheStr, tclass);
                    if(tTemp != null){
                        NormalUtils.customShowToast(mBaseResBean.msg);
                        callback.backError(mBaseResBean.msg,null);
                    }else{
                        callback.backError(mBaseResBean.msg,null);
                    }
                }else{
                    callback.backError(mBaseResBean.msg,null);
                }
            }
        }
    }

    private boolean checkActIsFinish(Context context){
        boolean isFinish = false;
        if(context instanceof Activity){
            Activity act = ((Activity)context);
            if(act == null || act.isFinishing()){
                isFinish = true;
            }
        }
        return isFinish;
    }
}
