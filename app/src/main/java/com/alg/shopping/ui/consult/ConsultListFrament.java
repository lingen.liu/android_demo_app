package com.alg.shopping.ui.consult;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.ArticlesDataBean;
import com.alg.shopping.bean.res.ArticlesResBean;
import com.alg.shopping.bean.res.ArticlesRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.pub.ShowWebActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;

public class ConsultListFrament extends BaseFrament implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.mrv_recycle_consult)
    RecyclerView mrv_recycle_consult;
    MRecyclerViewAdapter adapter;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    int page = 0;

    String type = "";

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_consultlist,container,false);
    }

    @Override
    protected void initViews() {
        Bundle mBundle = getArguments();
        if(mBundle != null){
            type = mBundle.getString("index");
        }
        adapter = new MRecyclerViewAdapter(R.layout.consult_recycle_item,null);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mrv_recycle_consult.setLayoutManager(linearLayoutManager);
        mrv_recycle_consult.setAdapter(adapter);
        adapter.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                List<ArticlesDataBean> data = adapter.getData();
                if(data != null && data.size() > i){
                    ArticlesDataBean mArticlesDataBean = data.get(i);
                    if(mArticlesDataBean != null){
                        Intent intent = new Intent(getActivity(),ShowWebActivity.class);
                        intent.putExtra("titleStr",mArticlesDataBean.title);
                        intent.putExtra("linkUrl",NetConstants.baseUrl+mArticlesDataBean.url);
                        ((BaseActivity)getActivity()).jumpPage(intent);
                    }
                }
            }
        });
        srl_refresh.setOnRefreshListener(this);
        srl_refresh.setOnLoadmoreListener(this);
        srl_refresh.autoRefresh();
    }
    private void requestData(final int pageTemp){
        NetParamets.articles(getActivity(), pageTemp, type,new NetCallBack<ArticlesResBean>() {
            @Override
            public void backSuccess(ArticlesResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                ArticlesRowsBean mArticlesRowsBean = result.result;
                if(mArticlesRowsBean != null){
                    List<ArticlesDataBean> mArticlesDataBeans = mArticlesRowsBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    mrv_recycle_consult.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(mArticlesDataBeans == null || mArticlesDataBeans.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            mrv_recycle_consult.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            adapter.setNewData(mArticlesDataBeans);
                        }
                    }else{
                        if(mArticlesDataBeans != null && mArticlesDataBeans.size() > 0){
                            page = pageTemp;
                            adapter.addData(mArticlesDataBeans);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void backError(String ex, ArticlesResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(0);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestData(page+1);
    }

    class MRecyclerViewAdapter extends BaseQuickAdapter<ArticlesDataBean> {


        public MRecyclerViewAdapter(int layoutResId, List<ArticlesDataBean> data) {
            super(layoutResId, data);
        }
        @Override
        protected void convert(BaseViewHolder baseViewHolder, final ArticlesDataBean bean) {
            ImageView iv_consult_pic = baseViewHolder.getView(R.id.iv_consult_pic);
            TextView tv_item_name = baseViewHolder.getView(R.id.tv_item_name);
            TextView tv_date = baseViewHolder.getView(R.id.tv_date);
            TextView tv_look_original = baseViewHolder.getView(R.id.tv_look_original);

            LinearLayout.LayoutParams llps = (LinearLayout.LayoutParams) iv_consult_pic.getLayoutParams();
            int width = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
            int height = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
            iv_consult_pic.measure(width, height);
            float scale = iv_consult_pic.getMeasuredWidth() / 960;
            llps.height = (int)(364 * scale);

            IMGLoadUtils.LoadCircleImageTwo(R.mipmap.banner_default_pic, NetConstants.baseUrl+bean.path,iv_consult_pic);
            tv_item_name.setText(bean.title);
            tv_date.setText(bean.createDate);
            tv_look_original.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ShowWebActivity.class);
                    intent.putExtra("titleStr",bean.title);
                    intent.putExtra("linkUrl",NetConstants.baseUrl+bean.url);
                    startActivity(intent);
                }
            });
        }
    }
}
