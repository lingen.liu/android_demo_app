package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.ExchangeDataBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.view.MViewPager;
import com.androidkun.xtablayout.XTabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyTradeRecordActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tb_menu)
    XTabLayout tb_menu;
    @BindView(R.id.mvp_pager)
    MViewPager mvp_pager;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    List<String> menuList = new ArrayList<>();
    ViewPagerAdapter adapter;
    FragmentManager fm;
    Fragment f_msg,f_notice,f_msg_one,f_notice_one,currFrament;
    List<Fragment> mFragments = new ArrayList<>();
    int page = 0;
    int index = 0;//0-申购记录，1-土地权益
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_traderecord);
    }

    @Override
    protected void initViews() {
        index = getIntent().getIntExtra("index",0);
        if(index == 0){
            tv_title.setText("我的申购记录");
        }else{
            tv_title.setText("我的土地权益");
        }
        fm = getSupportFragmentManager();
        if(index == 0){
            menuList.add("申购申请中");
            menuList.add("申购完成");
            tv_menu.setText("立即申购");
            tv_menu.setVisibility(View.VISIBLE);
            tv_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.showLoadingDialog(MyTradeRecordActivity.this,"",true);
                    trade_config();
                }
            });
        }else if(index == 1){
            menuList.add("兑换中");
            menuList.add("兑换完成");
            tv_menu.setVisibility(View.INVISIBLE);
        }
        tb_menu.setTabMode(TabLayout.MODE_FIXED);
        for (int i = 0; i < menuList.size(); i++) {
            tb_menu.addTab(tb_menu.newTab().setText(menuList.get(i)));
        }
        f_msg = new TradeRecordFrament();
        Bundle mBundleOne = new Bundle();
        mBundleOne.putInt("index",index);
        mBundleOne.putInt("type",0);
        f_msg.setArguments(mBundleOne);
        f_notice = new TradeRecordFrament();
        Bundle mBundleTwo = new Bundle();
        mBundleTwo.putInt("index",index);
        mBundleTwo.putInt("type",1);
        f_notice.setArguments(mBundleTwo);
        mFragments.add(f_msg);
        mFragments.add(f_notice);
        mvp_pager.setNoScroll(true);
        adapter = new ViewPagerAdapter(fm);
        adapter.setData(mFragments);
        mvp_pager.setAdapter(adapter);
        mvp_pager.setOffscreenPageLimit(menuList.size());
        tb_menu.setupWithViewPager(mvp_pager);
        tb_menu.getTabAt(0).select();
    }
    private void trade_config() {
        NetParamets.trade_config(MyTradeRecordActivity.this, new NetCallBack<ExchangeResBean>() {
            @Override
            public void backSuccess(ExchangeResBean result) {
                ExchangeDataBean mExchangeDataBean = result.result;
                if (mExchangeDataBean != null) {
                    final String price = mExchangeDataBean.price;
                    NetParamets.account(MyTradeRecordActivity.this, new NetCallBack<AccountResBean>() {

                        @Override
                        public void backSuccess(AccountResBean result) {
                            if(result.result != null){
                                AccountBean mAccountBean = result.result.account;
                                mAccountBean.price = price;
                                DialogUtils.tradeDialog(MyTradeRecordActivity.this,1,"兑换土地申购农合链",mAccountBean,true);
                            }
                        }

                        @Override
                        public void backError(String ex, AccountResBean result) {

                        }
                    });
                }
            }

            @Override
            public void backError(String ex, ExchangeResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> mFraments = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public void setData(List<Fragment> mFraments){
            this.mFraments = mFraments;
        }
        @Override
        public Fragment getItem(int position) {
            return mFraments.get(position);
        }

        @Override
        public int getCount() {
            return mFraments != null ? mFraments.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return menuList.get(position);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        currFrament.onActivityResult(requestCode, resultCode, data);
    }
}
