package com.alg.shopping.ui.mine;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.CardDataBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.AccountValidatorUtils;
import com.alg.shopping.tools.DateUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.pub.EditAddressActivity;
import com.bigkoo.pickerview.OptionsPickerView;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

/**
 * 绑定银行卡
 */
public class AddBankActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindViews({R.id.et_real_name, R.id.et_bank_num})
    List<EditText> editTexts;
    @BindView(R.id.et_bank_name)
    EditText et_bank_name;
    String id;

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_mine_addbank);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("绑定银行卡");
        CardDataBean mCardDataBean = (CardDataBean) getIntent().getSerializableExtra("mCardDataBean");
        if (mCardDataBean != null) {
            id = mCardDataBean.id;
            editTexts.get(0).setText(mCardDataBean.realName);
            editTexts.get(0).setSelection(mCardDataBean.realName.length());
            editTexts.get(1).setText(mCardDataBean.cardNumber);
            editTexts.get(1).setSelection(mCardDataBean.cardNumber.length());

            et_bank_name.setText(mCardDataBean.kh);
            et_bank_name.setSelection(mCardDataBean.kh.length());
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_submit://提  交
                String realName = editTexts.get(0).getText().toString();
                if (TextUtils.isEmpty(realName)) {
                    NormalUtils.customShowToast("请输入真实姓名");
                    return;
                }
                String bankNum = editTexts.get(1).getText().toString();
                if (TextUtils.isEmpty(bankNum)) {
                    NormalUtils.customShowToast("请输入银行卡号");
                    return;
                }
                if (!AccountValidatorUtils.isBANK(bankNum)) {
                    NormalUtils.customShowToast("请输入正确的银行卡号");
                    return;
                }
                String bankName = et_bank_name.getText().toString();
                if (TextUtils.isEmpty(bankName)) {
                    NormalUtils.customShowToast("请输入开户行");
                    return;
                }
                requestAddData(id, realName, bankNum, bankName);
                break;
        }
    }

    private void requestAddData(String id, String realName, String cardNumber, String kh) {
        NetParamets.cardAdd(AddBankActivity.this, id, realName, cardNumber, kh, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ADDBANKSUCCESS,null);
                finish();
            }

            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
