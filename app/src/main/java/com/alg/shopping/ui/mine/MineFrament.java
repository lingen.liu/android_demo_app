package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.bean.res.UserInfoBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.sql.CacheSqlDao;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.ImageCompressionUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.trade.TradeRecordActivity;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

public class MineFrament extends BaseFrament {
    @BindViews({R.id.tv_promotion_qr, R.id.tv_bank, R.id.tv_recommend_relation, R.id.tv_earnings_relation, R.id.tv_withdrawal_record, R.id.tv_help_center_pic, R.id.tv_exit_login_pic})
    List<TextView> textViews;
    @BindView(R.id.tv_consumption_integral)
    TextView tv_consumption_integral;
    @BindView(R.id.tv_farming_chain)
    TextView tv_farming_chain;
    @BindView(R.id.tv_normal_integral)
    TextView tv_normal_integral;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_user_type)
    TextView tv_user_type;
    final int REQUEST_ALBUM_OK = 1001;//拍照
    final int REQUEST_CAMERA = 1002;//选择照片
    final int REQUEST_CROP = 1003;//图片裁剪

    File mFile;//当前选择的图片

    String filePath = "";
    String filePathTemp2 = "";
    String filePathTemp1 = "";
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(),ActionFlag.LOGINLSUCCESS)){
            requestData();
        }
    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_mine, container, false);
    }

    @Override
    protected void initViews() {
        requestData();
    }

    @OnClick({R.id.tv_consumption_integral,R.id.tv_farming_chain,R.id.tv_normal_integral,R.id.iv_head_pic,R.id.tv_promotion_qr, R.id.tv_bank, R.id.tv_recommend_relation, R.id.tv_earnings_relation
            , R.id.tv_withdrawal_record, R.id.tv_help_center_pic, R.id.tv_exit_login_pic, R.id.tv_withdraw_pic
            , R.id.tv_all_order,R.id.tv_wait_pay,R.id.tv_wait_send,R.id.tv_wait_revice,R.id.tv_wait_praise,R.id.tv_transaction_record,R.id.tv_account_desc,
    R.id.tv_auction_record,R.id.tv_trade_record,R.id.tv_buy_record,R.id.tv_equity_record})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_head_pic:
//                filePath = Constants.DB_DATA_PATH+System.currentTimeMillis()+".jpg";
//                filePathTemp2 = Constants.DB_DATA_PATH+System.currentTimeMillis()+"temp2"+".jpg";
//                filePathTemp1 = Constants.DB_DATA_PATH+System.currentTimeMillis()+"temp1"+".jpg";
//                DialogUtils.picChooseDialog(getActivity(),REQUEST_ALBUM_OK,REQUEST_CAMERA,filePathTemp1,1);
                break;
            case R.id.tv_consumption_integral:
                //消费积分明细
                Intent intent_integral = new Intent(getActivity(), AccountListActivity.class);
                intent_integral.putExtra("index",1);
                ((BaseActivity) getActivity()).jumpPage(intent_integral);
                break;
            case R.id.tv_farming_chain:
                Intent intent_chain = new Intent(getActivity(), AccountListActivity.class);
                intent_chain.putExtra("index",2);
                ((BaseActivity) getActivity()).jumpPage(intent_chain);
                break;
            case R.id.tv_normal_integral:
                //普通积分
                Intent intent_normal = new Intent(getActivity(), AccountListActivity.class);
                intent_normal.putExtra("index",3);
                ((BaseActivity) getActivity()).jumpPage(intent_normal);
                break;
            case R.id.tv_account_desc:
                ((BaseActivity) getActivity()).jumpPage(new Intent(getActivity(), AccountListActivity.class));
                break;
            case R.id.tv_transaction_record:
                Intent intentThree = new Intent(getActivity(), TradeRecordActivity.class);
                intentThree.putExtra("index",1);
                ((BaseActivity) getActivity()).jumpPage(intentThree);
                break;
            case R.id.tv_promotion_qr:
                ((BaseActivity) getActivity()).jumpPage(new Intent(getActivity(), GeneralizeCodeActivity.class));
                break;
            case R.id.tv_bank:
                requestBankData();
                break;
            case R.id.tv_recommend_relation:
                ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),PromotionRecordActivity.class));
                break;
            case R.id.tv_earnings_relation:
                ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),GainRecordActivity.class));
                break;
            case R.id.tv_withdrawal_record:
                ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),WithdrawalRecordActivity.class));
                break;

            case R.id.tv_auction_record:
                NormalUtils.customShowToast("该功能暂未开通");
                break;
            case R.id.tv_trade_record:
                Intent intent = new Intent(getActivity(), PointRecordActivity.class);
                intent.putExtra("index",0);
                ((BaseActivity) getActivity()).jumpPage(intent);
                break;
            case R.id.tv_buy_record:
                Intent intentOne = new Intent(getActivity(), MyTradeRecordActivity.class);
                intentOne.putExtra("index",0);
                ((BaseActivity)getActivity()).jumpPage(intentOne);
                break;
            case R.id.tv_equity_record:
                Intent intentTwo = new Intent(getActivity(), MyTradeRecordActivity.class);
                intentTwo.putExtra("index",1);
                ((BaseActivity)getActivity()).jumpPage(intentTwo);
                break;

            case R.id.tv_help_center_pic:
                NormalUtils.customShowToast("该功能暂未开通");
                break;
            case R.id.tv_exit_login_pic:
                DialogUtils.normalDialog(getActivity(), "", "确认退出当前登录状态么？", "确定", "取消", true, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CacheSqlDao.getInance().delete(Constants.USERINFOKEY);
                        NormalUtils.sendBroadcast(ActionFlag.LOGINLOUT,null);
                    }
                },null);
                break;
            case R.id.tv_withdraw_pic://提现
                ((BaseActivity) getActivity()).jumpPage(new Intent(getActivity(), WithdrawalActivity.class));
                break;
            case R.id.tv_all_order://查看全部订单
                Intent intent_all = new Intent(getActivity(),MyOrderListActivity.class);
                intent_all.putExtra("index",0);
                ((BaseActivity)getActivity()).jumpPage(intent_all);
                break;
            case R.id.tv_wait_pay:
                Intent intent_wait_pay = new Intent(getActivity(),MyOrderListActivity.class);
                intent_wait_pay.putExtra("index",1);
                ((BaseActivity)getActivity()).jumpPage(intent_wait_pay);
                break;
            case R.id.tv_wait_send:
                Intent intent_wait_send = new Intent(getActivity(),MyOrderListActivity.class);
                intent_wait_send.putExtra("index",2);
                ((BaseActivity)getActivity()).jumpPage(intent_wait_send);
                break;
            case R.id.tv_wait_revice:
                Intent intent_wait_revice = new Intent(getActivity(),MyOrderListActivity.class);
                intent_wait_revice.putExtra("index",3);
                ((BaseActivity)getActivity()).jumpPage(intent_wait_revice);
                break;
            case R.id.tv_wait_praise:
                Intent intent_wait_praise = new Intent(getActivity(),MyOrderListActivity.class);
                intent_wait_praise.putExtra("index",4);
                ((BaseActivity)getActivity()).jumpPage(intent_wait_praise);
                break;
        }
    }
    public UserInfoBean user;
    public AccountBean account;
    private void requestData(){
        if(TextUtils.isEmpty(NormalUtils.getLoginUserToken())){
            return;
        }
        NetParamets.account(getActivity(), new NetCallBack<AccountResBean>() {
            @Override
            public void backSuccess(AccountResBean result) {
                DialogUtils.dismissLoadingDialog();
                if(result.result != null){
                    user = result.result.user;
                    account = result.result.account;
                    initUiData();
                }
            }
            @Override
            public void backError(String ex, AccountResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
                initUiData();
            }
        });
    }
    private void initUiData(){
        if(user != null){
            tv_user_name.setText(user.name);
            if(TextUtils.equals(user.user_type,"leader")){
                tv_user_type.setText("合伙人");
            }else if(TextUtils.equals(user.user_type,"gd")){
                tv_user_type.setText("高级合伙人");
            }else {
                tv_user_type.setText("普通用户");
            }
        }
        if(account != null){
            tv_consumption_integral.setText(NormalUtils.priceGrayStyle(account.xiaofeiJifen,"消费积分(元)",true));
            tv_farming_chain.setText(NormalUtils.priceGrayStyle(account.integral,"农合链",true));
            tv_normal_integral.setText(NormalUtils.priceGrayStyle(account.balance,"普通积分(元)",true));
        }
    }
//    @Override
//    public void onResume() {
//        super.onResume();
//        requestData();
//    }
    private void requestBankData() {
        NetParamets.card(getActivity(), new NetCallBack<CardResBean>() {
            @Override
            public void backSuccess(CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                if (result.result != null) {
                    Intent intent = new Intent(getActivity(),MyBankActivity.class);
                    intent.putExtra("CardDataBean",result.result);
                    ((BaseActivity)getActivity()).jumpPage(intent);
                } else {
                    Intent intent = new Intent(getActivity(),AddBankActivity.class);
                    ((BaseActivity)getActivity()).jumpPage(intent);
                }
            }

            @Override
            public void backError(String ex, CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (filePathTemp1 != null) {
                    if(new File(filePathTemp1).exists()){
                        Uri imageUri;
                        //判断当前Android版本号是否大于等于24
                        if (Build.VERSION.SDK_INT >= 24){
                            //如果是则使用FileProvider
                            imageUri = FileProvider.getUriForFile(getActivity(),"com.alg.shopping.inter.MyFileProvider", new File(filePathTemp1));
                        } else {
                            //否则，使用原来的fromFile()
                            imageUri = Uri.fromFile(new File(filePathTemp1));
                        }
                        IMGLoadUtils.cropImage(getActivity(),imageUri,filePathTemp2,REQUEST_CROP);
                    }else{
                        NormalUtils.customShowToast("图片不存在");
                    }
                }
                break;
            case REQUEST_ALBUM_OK:
                if(data != null){
                    Uri mUri = data.getData();
                    if(mUri != null){
                        filePathTemp1 = IMGLoadUtils.getRealFilePath(mUri);
                        if(new File(filePathTemp1).exists()){
                            //判断当前Android版本号是否大于等于24
                            if (Build.VERSION.SDK_INT >= 24){
                                //如果是则使用FileProvider
                                mUri = FileProvider.getUriForFile(getActivity(),"com.alg.shopping.inter.MyFileProvider", new File(filePathTemp1));
                            } else {
                                //否则，使用原来的fromFile()
                                mUri = Uri.fromFile(new File(filePathTemp1));
                            }
                            IMGLoadUtils.cropImage(getActivity(),mUri,filePathTemp2,REQUEST_CROP);
                        }else{
                            NormalUtils.customShowToast("图片不存在");
                        }
                    }
                }
                break;
            case REQUEST_CROP:
                if(new File(filePathTemp2).exists()){
                    try {
                        ImageCompressionUtils.compressAndGenImage(filePathTemp2,filePath,500,false);
                        CacheUtils.getInstance().deleteFolderFile(filePathTemp2,true);
                        CacheUtils.getInstance().deleteFolderFile(filePathTemp1,true);
                        mFile = new File(filePath);
                        if(mFile != null && mFile.exists()){
                            uploadPicToService(mFile);
                        }else{
                            NormalUtils.customShowToast("未找到图片");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        NormalUtils.customShowToast("图片选择失败");
                    }
                }else{
                    NormalUtils.customShowToast("图片不存在");
                }
                break;

        }
    }

    /**
     * 上传图片
     * @param mFile
     */
    private void uploadPicToService(File mFile){
        // TODO: 2018/8/31 上传图片
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            DialogUtils.showLoadingDialog(getActivity(),"",true);
            requestData();
        }
    }
}
