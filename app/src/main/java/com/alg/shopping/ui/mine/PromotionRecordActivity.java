package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.bean.res.PromoResBean;
import com.alg.shopping.bean.res.PromosResBean;
import com.alg.shopping.bean.res.PromosRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DateUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PromotionRecordActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_promotioned)
    TextView tv_promotioned;
    @BindView(R.id.tv_promotion_income)
    TextView tv_promotion_income;
    @BindView(R.id.tv_a_count)
    TextView tv_a_count;
    @BindView(R.id.tv_b_count)
    TextView tv_b_count;
    @BindView(R.id.elv_list)
    ExpandableListView elv_list;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_empty)
    TextView tv_empty;

    int page = 0;
    MyExpandableListView adapter;

    List<PromosRowsBean> rows;


    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_promotion_record);
    }

    @Override
    protected void initViews() {
        tv_title.setText("我的推荐关系");
        tv_menu.setVisibility(View.INVISIBLE);
        adapter = new MyExpandableListView();
        elv_list.setAdapter(adapter);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        DialogUtils.showLoadingDialog(PromotionRecordActivity.this,"",true);
        requestData();
    }
    @OnClick({R.id.iv_back,R.id.tv_generalize})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_generalize:
                jumpPage(new Intent(PromotionRecordActivity.this,GeneralizeCodeActivity.class));
                break;
        }
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestListData(page + 1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestListData(0);
    }

    private class MyExpandableListView extends BaseExpandableListAdapter {
        List<PromosRowsBean> rows;
        public void setData(List<PromosRowsBean> rows){
            this.rows = rows;
        }
        @Override
        public int getGroupCount() {
            return rows != null ? rows.size() : 0;
        }

        @Override
        public int getChildrenCount(int i) {
            List<PromosRowsBean> subItems = rows.get(i).subItems;
            return subItems != null ? subItems.size() : 0;
        }

        @Override
        public PromosRowsBean getGroup(int i) {
            return rows.get(i);
        }

        @Override
        public PromosRowsBean getChild(int i, int i1) {
            return rows.get(i).subItems.get(i1);
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int i1) {
            return i1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(PromotionRecordActivity.this).inflate(R.layout.group_item, null);
            }
            TextView tv_username =  view.findViewById(R.id.tv_username);
            TextView tv_date = view.findViewById(R.id.tv_date);
            PromosRowsBean mPromosRowsBean = getGroup(i);
            tv_username.setText(mPromosRowsBean.tel);
            tv_date.setText("注册时间："+ mPromosRowsBean.createDate);
            return view;
        }

        @Override
        public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(PromotionRecordActivity.this).inflate(R.layout.child_item, null);
            }
            TextView tv_username = view.findViewById(R.id.tv_username);
            TextView tv_date = view.findViewById(R.id.tv_date);
            PromosRowsBean mPromosRowsBean = getChild(i,i1);
            tv_username.setText(mPromosRowsBean.tel);
            tv_date.setText("注册时间："+ mPromosRowsBean.createDate);
            return view;
        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return false;
        }
    }
    private void requestData(){
        NetParamets.promo(PromotionRecordActivity.this, new NetCallBack<PromoResBean>() {
            @Override
            public void backSuccess(PromoResBean result) {
                if(result.result != null){
                    tv_promotioned.setText(result.result.promoter);
                    tv_promotion_income.setText("推广收益："+result.result.promo_sum);
                    tv_a_count.setText("A级推荐人数：6人");
                    tv_b_count.setText("B级推荐人数：8人");
                }
                requestListData(0);
            }
            @Override
            public void backError(String ex, PromoResBean result) {
                NormalUtils.customShowToast(ex);
                requestListData(0);
            }
        });
    }
    private void requestListData(final int pageTemp){
        NetParamets.promos(PromotionRecordActivity.this, pageTemp,new NetCallBack<PromosResBean>() {
            @Override
            public void backSuccess(PromosResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                if(result.result != null){
                    tv_empty.setVisibility(View.GONE);
                    elv_list.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(result.result.rows == null || result.result.rows.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            elv_list.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            rows = result.result.rows;
                        }
                    }else{
                        if(rows != null && rows.size() > 0){
                            page = pageTemp;
                            rows.addAll(result.result.rows);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.setData(rows);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void backError(String ex, PromosResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
            }
        });
    }
}
