package com.alg.shopping.ui.login;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.config.CountTimer;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.AccountValidatorUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.ZxingUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.pub.ShowWebActivity;
import com.yzq.zxinglibrary.common.Constant;


import butterknife.BindView;
import butterknife.OnClick;

public class RegiestActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_find_code)
    TextView tv_find_code;
    @BindView(R.id.et_username)
    EditText et_username;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_code)
    EditText et_code;
    @BindView(R.id.et_scan_qr)
    EditText et_scan_qr;

    @BindView(R.id.et_login_pwd)
    EditText et_login_pwd;
    @BindView(R.id.et_sure_pwd)
    EditText et_sure_pwd;

    @BindView(R.id.ctv_login_pwd)
    CheckedTextView ctv_login_pwd;
    @BindView(R.id.ctv_sure_pwd)
    CheckedTextView ctv_sure_pwd;
    @BindView(R.id.cb_box)
    CheckBox cb_box;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_regiest);
    }

    @Override
    protected void initViews() {
        tv_title.setText("注册");
        tv_menu.setVisibility(View.INVISIBLE);
    }

    @OnClick({R.id.iv_back, R.id.tv_find_code, R.id.iv_qr, R.id.tv_regiest, R.id.tv_user_agreement, R.id.ctv_login_pwd, R.id.ctv_sure_pwd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ctv_login_pwd:
                ctv_login_pwd.toggle();
                if (ctv_login_pwd.isChecked()) {
                    et_login_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_login_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if (!TextUtils.isEmpty(et_login_pwd.getText().toString())) {
                    et_login_pwd.setSelection(et_login_pwd.getText().toString().length());
                }
                break;
            case R.id.ctv_sure_pwd:
                ctv_sure_pwd.toggle();
                if (ctv_sure_pwd.isChecked()) {
                    et_sure_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_sure_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if (!TextUtils.isEmpty(et_sure_pwd.getText().toString())) {
                    et_sure_pwd.setSelection(et_sure_pwd.getText().toString().length());
                }
                break;
            case R.id.iv_qr:
                ZxingUtils.jumpToZxing(RegiestActivity.this, Constants.REQUEST_CODE_SCAN);
                break;
            case R.id.tv_regiest:
                String username = et_username.getText().toString();
                String phone = et_phone.getText().toString();
                String code = et_code.getText().toString();
                String pwd = et_login_pwd.getText().toString();
                String sure_pwd = et_sure_pwd.getText().toString();
                String qr = et_scan_qr.getText().toString();
                if (TextUtils.isEmpty(username)) {
                    NormalUtils.customShowToast("请输入姓名");
                    return;
                }
                if (!AccountValidatorUtils.isMobile(phone)) {
                    NormalUtils.customShowToast("请正确输入手机号码");
                    return;
                }
                if (TextUtils.isEmpty(code)) {
                    NormalUtils.customShowToast("请验证码");
                    return;
                }
                if (TextUtils.isEmpty(pwd)) {
                    NormalUtils.customShowToast("请输入密码");
                    return;
                }
                if (TextUtils.isEmpty(sure_pwd)) {
                    NormalUtils.customShowToast("请输入确认密码");
                    return;
                }
                if (!TextUtils.equals(pwd, sure_pwd)) {
                    NormalUtils.customShowToast("2次密码输入不一致");
                    return;
                }
                if (TextUtils.isEmpty(qr)) {
                    NormalUtils.customShowToast("请扫描二维码获取邀请码或输入邀请码");
                    return;
                }
                if (qr.length() < 11) {
                    NormalUtils.customShowToast("请输入正确邀请码");
                    return;
                }
                if(!cb_box.isChecked()){
                    NormalUtils.customShowToast("请输入同意用户服务协议");
                    return;
                }
                DialogUtils.showLoadingDialog(RegiestActivity.this, "", true);
                NetParamets.regist(RegiestActivity.this, username, phone, qr, code, pwd, sure_pwd, new NetCallBack<BaseResBean>() {
                    @Override
                    public void backSuccess(BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                        finish();
                    }

                    @Override
                    public void backError(String ex, BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                        NormalUtils.customShowToast(ex);
                    }
                });

                break;
            case R.id.tv_user_agreement:
                Intent intent = new Intent(RegiestActivity.this, ShowWebActivity.class);
                intent.putExtra("titleStr","用户服务协议");
                intent.putExtra("linkUrl", NetConstants.baseUrl+NetConstants.protocol);
                jumpPage(intent);
                break;
            case R.id.tv_find_code:
                String phoneStrCode = et_phone.getText().toString();
                if (!AccountValidatorUtils.isMobile(phoneStrCode)) {
                    NormalUtils.customShowToast("请正确输入手机号码");
                    return;
                }
                tv_find_code.setClickable(false);
                CountTimer mCountTimer = new CountTimer(Constants.CODELONG, Constants.countDownInterval);
                mCountTimer.setView(tv_find_code);
                mCountTimer.start();
                DialogUtils.showLoadingDialog(RegiestActivity.this, getResources().getString(R.string.loading), true);
                NetParamets.queryCode(RegiestActivity.this, phoneStrCode, new NetCallBack<BaseResBean>() {
                    @Override
                    public void backSuccess(BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                    }

                    @Override
                    public void backError(String ex, BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                        NormalUtils.customShowToast(ex);
                    }
                });
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == Constants.REQUEST_CODE_SCAN) {
            if (data != null) {
                Message msg = Message.obtain();
                msg.what = 10011;
                msg.obj = data.getStringExtra(Constant.CODED_CONTENT);
                mHandler.sendMessageDelayed(msg, 500);
            }
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                String content = (String) msg.obj;
                if(!TextUtils.isEmpty(content) && content.contains("?p=")){
                    content = content.substring(content.indexOf("?p=")+3,content.length());
                    if(content.contains("&")){
                        content = content.substring(0,content.indexOf("&"));
                    }
                    et_scan_qr.setText(content);
                }else{
                    NormalUtils.customShowToast("无法识别二维码");
                }
            } catch (Exception e) {
                NormalUtils.customShowToast("无法识别二维码");
            }
        }
    };
}
