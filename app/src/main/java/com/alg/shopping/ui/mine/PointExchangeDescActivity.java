package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.SellOrBuyResBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.trade.PointTradeActivity;
import com.alg.shopping.ui.trade.TradeOutInActivity;
import com.alg.shopping.ui.view.MlistView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PointExchangeDescActivity extends BaseActivity implements OnRefreshListener{
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.mlv_list)
    MlistView mlv_list;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_title_desc)
    TextView tv_title_desc;

    List<String> listStr;
    String id = "";
    SellOrBuyResultBean mSellOrBuyResultBean;
    ListOneAdapter adapter_one;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activiyt_equitydesc);
    }

    @Override
    protected void initViews() {
        tv_title.setText("点对点兑换详情");
        id = getIntent().getStringExtra("id");
        String[] strArray = getResources().getStringArray(R.array.point_menu);
        listStr = Arrays.asList(strArray);
        adapter_one = new ListOneAdapter(PointExchangeDescActivity.this,listStr,R.layout.list_item_item);
        mlv_list.setAdapter(adapter_one);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setEnableLoadmore(false);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    private void requestData(String id){
        NetParamets.exchange_details(PointExchangeDescActivity.this, id, new NetCallBack<SellOrBuyResBean>() {
            @Override
            public void backSuccess(SellOrBuyResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishRefresh();
                mSellOrBuyResultBean = result.result;
                adapter_one.setOtherData(mSellOrBuyResultBean);
                adapter_one.notifyDataSetChanged();
                float total_amount = 0f;
                try{
                    total_amount = Float.parseFloat(mSellOrBuyResultBean.count) * Float.parseFloat(mSellOrBuyResultBean.price);
                }catch (Exception e){}
                tv_num.setText(NormalUtils.getDecmiTo(total_amount));
                if(TextUtils.equals(mSellOrBuyResultBean.type,"BUY")){
                    tv_title_desc.setText("兑换总额（积分）");
                }else{
                    tv_title_desc.setText("兑换总额（农合链）");
                }
            }

            @Override
            public void backError(String ex, SellOrBuyResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishRefresh();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(id);
    }

    private class ListOneAdapter extends CommonBaseAdapter<String>{
        SellOrBuyResultBean mSellOrBuyResultBean;
        public ListOneAdapter(Context context, List<String> datas, int layoutId) {
            super(context, datas, layoutId);
        }
        public void setOtherData(SellOrBuyResultBean mSellOrBuyResultBean){
            this.mSellOrBuyResultBean = mSellOrBuyResultBean;
        }
        @Override
        public void convert(CommonBaseHolder holder, int position, String bean) {
            TextView tv_name = holder.getView(R.id.tv_name);
            TextView tv_value = holder.getView(R.id.tv_value);
            tv_value.setTextColor(getResources().getColor(R.color.color_black));
            tv_name.setText(bean);
            if(mSellOrBuyResultBean != null){
                switch (position){
                    case 0:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.create_date) ? mSellOrBuyResultBean.create_date : "-");
                        break;
                    case 1:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.price) ? mSellOrBuyResultBean.price : "-");
                        break;
                    case 2:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.count) ? mSellOrBuyResultBean.count : "-");
                        break;
                    case 3:
                        tv_value.setText(TextUtils.equals(mSellOrBuyResultBean.type,"BUY") ? "换入" : "换出");
                        break;
                    case 4:
                        String statuStr = "";
                        if(TextUtils.equals(mSellOrBuyResultBean.status,"PENDING")){
                            statuStr = "挂单中";
                            tv_value.setTextColor(getResources().getColor(R.color.color_black));
                        }else if(TextUtils.equals(mSellOrBuyResultBean.status,"PENDED")){
                            statuStr = "已成交";
                            tv_value.setTextColor(getResources().getColor(R.color.shallowcenter_color));
                        }else if(TextUtils.equals(mSellOrBuyResultBean.status,"CANCEL")){
                            statuStr = "已撤销";
                            tv_value.setTextColor(getResources().getColor(R.color.color_red_one));
                        }
                        tv_value.setText(statuStr);
                        break;
                    case 5:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.limit_time) ? mSellOrBuyResultBean.limit_time : "-");
                        break;
                }
            }
        }
    }
}
