package com.alg.shopping.ui.pub;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.req.OrderItemBean;
import com.alg.shopping.bean.res.AddressCurrResBean;
import com.alg.shopping.bean.res.AddressDataBean;
import com.alg.shopping.bean.res.GoodDetailDataBean;
import com.alg.shopping.bean.res.OrderResBean;
import com.alg.shopping.bean.res.ShippingsDataBean;
import com.alg.shopping.bean.res.ShippingsResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.sql.CartDataTable;
import com.alg.shopping.tools.DateUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.view.MlistView;
import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SureOrderActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_address_str)
    TextView tv_address_str;
    @BindView(R.id.mlv_list)
    MlistView mlv_list;
    OrderListAdapter adapter;
    @BindView(R.id.et_remark)
    EditText et_remark;
    @BindView(R.id.tv_distribution_model)
    TextView tv_distribution_model;
    @BindView(R.id.tv_good_price)
    TextView tv_good_price;
    @BindView(R.id.tv_freight_price)
    TextView tv_freight_price;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.llt_add_address)
    LinearLayout llt_add_address;
    @BindView(R.id.llt_address)
    LinearLayout llt_address;
    ShippingsDataBean currShippingsDataBean;
    AddressDataBean currAddressDataBean;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(), ActionFlag.ADDRESSCHANGE) && obj.getObj() instanceof AddressDataBean){
            setAddress((AddressDataBean) obj.getObj());
        }else if(TextUtils.equals(ActionFlag.PAYSUCCESS,obj.getFlag())){
            finish();
        }
    }
    private void setAddress(AddressDataBean mAddressDataBean){
        if(mAddressDataBean == null){
            llt_add_address.setVisibility(View.VISIBLE);
            llt_address.setVisibility(View.GONE);
        }else{
            currAddressDataBean = mAddressDataBean;
            llt_add_address.setVisibility(View.GONE);
            llt_address.setVisibility(View.VISIBLE);
            tv_name.setText(currAddressDataBean.receiver+"    "+currAddressDataBean.mobile);
            tv_address_str.setText(currAddressDataBean.address);
        }
    }
    private String[] beanToString(List<ShippingsDataBean> shippingList){
        String[] shippingArray = new String[shippingList.size()];
        for (int i = 0; i < shippingList.size(); i++) {
            shippingArray[i] = shippingList.get(i).name;
        }
        return shippingArray;
    }



    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_sureorder);
    }

    @Override
    protected void initViews() {
        tv_title.setText("确认订单");
        tv_menu.setVisibility(View.INVISIBLE);
        List<CartDataTable> mCartDataTables = (List<CartDataTable>) getIntent().getSerializableExtra("goods");
        adapter = new OrderListAdapter(SureOrderActivity.this,mCartDataTables,R.layout.inventory_item);
        mlv_list.setAdapter(adapter);
        tv_total_price.setText(NormalUtils.priceRedStyle("合计",(calcGoodTotalPrice(mCartDataTables)+calcFreightTotalPrice(mCartDataTables))+""));
        tv_good_price.setText(NormalUtils.priceGrayStyle("",calcGoodTotalPrice(mCartDataTables)+""));
        tv_freight_price.setText(NormalUtils.priceGrayStyle("",calcFreightTotalPrice(mCartDataTables)+""));
        DialogUtils.showLoadingDialog(SureOrderActivity.this,"",true);
        requestData();
    }
    @OnClick({R.id.tv_submit_order,R.id.iv_back,R.id.llt_address,R.id.tv_distribution_model,R.id.llt_add_address})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.llt_add_address:
                jumpPage(new Intent(SureOrderActivity.this,EditAddressActivity.class));
                break;
            case R.id.tv_submit_order:
                if(currAddressDataBean == null){
                    NormalUtils.customShowToast("请选择收货地址");
                    return;
                }
                if(currShippingsDataBean == null){
                    NormalUtils.customShowToast("请选择配送方式");
                    return;
                }
                String remarkStr = et_remark.getText().toString();
                String addressId = currAddressDataBean.id;
                String shippingId = currShippingsDataBean.id;
                List<CartDataTable> mCartDataTables = adapter.getData();
                if(mCartDataTables == null){
                    NormalUtils.customShowToast("未获取到商品信息");
                    return;
                }
                List<OrderItemBean> mOrderItemBeans = new ArrayList<>();
                for (CartDataTable mCartDataTable:mCartDataTables) {
                    OrderItemBean mOrderItemBean = new OrderItemBean();
                    mOrderItemBean.productId = mCartDataTable.getGoodId();
                    mOrderItemBean.quantity = mCartDataTable.getNum()+"";
                    mOrderItemBeans.add(mOrderItemBean);
                }
                DialogUtils.showLoadingDialog(SureOrderActivity.this,"",true);
                requestOrderData(remarkStr,addressId,shippingId,mOrderItemBeans);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.llt_address:
                jumpPage(new Intent(SureOrderActivity.this,AddressChooseActivity.class));
                break;
            case R.id.tv_distribution_model:
                DialogUtils.showLoadingDialog(SureOrderActivity.this,"",true);
                requestShiping();
                break;
        }
    }
    private class OrderListAdapter extends CommonBaseAdapter<CartDataTable> {

        public OrderListAdapter(Context context, List<CartDataTable> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, CartDataTable bean) {
            ImageView iv_good_pic = holder.getView(R.id.iv_good_pic);
            TextView tv_good_name = holder.getView(R.id.tv_good_name);
            TextView tv_good_desc = holder.getView(R.id.tv_good_desc);
            TextView tv_price = holder.getView(R.id.tv_price);
            TextView tv_num = holder.getView(R.id.tv_num);
            GoodDetailDataBean mGoodDetailDataBean = new Gson().fromJson(bean.getValueStr(),GoodDetailDataBean.class);
            if(mGoodDetailDataBean != null){
                IMGLoadUtils.loadCenterCropPic(mGoodDetailDataBean.photo != null ? (NetConstants.baseUrl+mGoodDetailDataBean.photo.thumb) : R.mipmap.good_default_pic,iv_good_pic,R.mipmap.good_default_pic);
                tv_good_desc.setText("("+mGoodDetailDataBean.sub_title+")");
                tv_good_name.setText(mGoodDetailDataBean.name);
                tv_price.setText(NormalUtils.priceRedStyle("",mGoodDetailDataBean.originalPrice));
                tv_num.setText("x"+bean.getNum());
            }
        }
    }
    private void requestData(){
        NetParamets.currAddress(SureOrderActivity.this, new NetCallBack<AddressCurrResBean>() {
            @Override
            public void backSuccess(AddressCurrResBean result) {
                DialogUtils.dismissLoadingDialog();
                if(result.result != null){
                    setAddress(result.result);
                }else{
                    setAddress(null);
                }
            }
            @Override
            public void backError(String ex, AddressCurrResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private void requestShiping(){
        NetParamets.shippings(SureOrderActivity.this, new NetCallBack<ShippingsResBean>() {
            @Override
            public void backSuccess(ShippingsResBean result) {
                DialogUtils.dismissLoadingDialog();
                final List<ShippingsDataBean> shippingList = result.result;
                if(shippingList != null && shippingList.size() > 0){
                    currShippingsDataBean = shippingList.get(0);
                    if(currShippingsDataBean != null){
                        tv_distribution_model.setText(currShippingsDataBean.name);
                    }
                    String[] shippingArray = beanToString(shippingList);
                    if(shippingArray == null || shippingArray.length == 0){
                        NormalUtils.customShowToast("未获取到商品配送方式");
                        return;
                    }
                    OptionsPickerView mOptionsProvince = DateUtils.initOptionPicker(SureOrderActivity.this, shippingArray, 0, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            currShippingsDataBean = shippingList.get(options1);
                            tv_distribution_model.setText(currShippingsDataBean.name);
                        }
                    });
                    mOptionsProvince.show(true);
                }else{
                    NormalUtils.customShowToast("未获取到商品配送方式");
                }
            }

            @Override
            public void backError(String ex, ShippingsResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
    private void requestOrderData(String guestMessage,String receiverId,String shippingId,List<OrderItemBean> items){
        NetParamets.order(SureOrderActivity.this, guestMessage, receiverId, shippingId, items, new NetCallBack<OrderResBean>() {
            @Override
            public void backSuccess(OrderResBean result) {
                DialogUtils.dismissLoadingDialog();
                Intent intent = new Intent(SureOrderActivity.this,PayActivity.class);
                intent.putExtra("order_data",result.result);
                jumpPage(intent);
            }

            @Override
            public void backError(String ex, OrderResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private float calcGoodTotalPrice(List<CartDataTable> mCartDataTables){
        float totalPrice = 0f;
        if(mCartDataTables != null && mCartDataTables.size() > 0){
            for (CartDataTable mCartDataTable:mCartDataTables) {
                GoodDetailDataBean mGoodDetailDataBean = new Gson().fromJson(mCartDataTable.getValueStr(),GoodDetailDataBean.class);
                totalPrice = totalPrice+(Float.parseFloat(mGoodDetailDataBean.originalPrice) * mCartDataTable.getNum());
            }
        }
        return totalPrice;
    }
    private float calcFreightTotalPrice(List<CartDataTable> mCartDataTables){
        float totalPrice = 0f;
        if(mCartDataTables != null && mCartDataTables.size() > 0){
            for (CartDataTable mCartDataTable:mCartDataTables) {
                GoodDetailDataBean mGoodDetailDataBean = new Gson().fromJson(mCartDataTable.getValueStr(),GoodDetailDataBean.class);
                totalPrice = totalPrice+(Float.parseFloat(mGoodDetailDataBean.freight_price) * mCartDataTable.getNum());
            }
        }
        return totalPrice;
    }
}
