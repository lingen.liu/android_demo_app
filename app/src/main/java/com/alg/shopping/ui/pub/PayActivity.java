package com.alg.shopping.ui.pub;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.PayResult;
import com.alg.shopping.bean.res.OrderDataBean;
import com.alg.shopping.bean.res.PayDataBean;
import com.alg.shopping.bean.res.PayInfoBean;
import com.alg.shopping.bean.res.PayResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class PayActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_order_no)
    TextView tv_order_no;
    @BindView(R.id.tv_order_money)
    TextView tv_order_money;
    @BindView(R.id.tv_order_chain)
    TextView tv_order_chain;
    @BindView(R.id.tv_consumption_integral)
    TextView tv_consumption_integral;
    @BindView(R.id.rg_group)
    RadioGroup rg_group;

    @BindView(R.id.rb_alipay)
    RadioButton rb_alipay;
    @BindView(R.id.rb_wx)
    RadioButton rb_wx;
    @BindView(R.id.rb_yinlian)
    RadioButton rb_yinlian;
    @BindView(R.id.rb_yue)
    RadioButton rb_yue;
    @BindView(R.id.v_view_one)
    View v_view_one;
    @BindView(R.id.v_view_two)
    View v_view_two;
    @BindView(R.id.v_view_three)
    View v_view_three;
    @BindView(R.id.v_view_four)
    View v_view_four;

    @BindView(R.id.rb_offline)
    RadioButton rb_offline;
    @BindView(R.id.llt_desc)
    LinearLayout llt_desc;



    int curr_id = R.id.rb_offline;

    String pay_way = "CHINA_PAY";

    OrderDataBean mOrderDataBean;
    private IWXAPI api;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if (TextUtils.equals(ActionFlag.PAYSUCCESS, obj.getFlag())) {
            finish();
        }
    }
    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_pay);
    }

    @Override
    protected void initViews() {
        api = WXAPIFactory.createWXAPI(PayActivity.this, Constants.WX_APP_ID);
        tv_title.setText("支付订单");
        tv_menu.setVisibility(View.INVISIBLE);
        mOrderDataBean = (OrderDataBean) getIntent().getSerializableExtra("order_data");
        if (mOrderDataBean != null) {
            tv_order_no.setText(mOrderDataBean.order_number);
            tv_order_money.setText(mOrderDataBean.total_price + "元");
            tv_order_chain.setText((TextUtils.isEmpty(mOrderDataBean.bi_count) ? "0" : mOrderDataBean.bi_count) + "个");
            tv_consumption_integral.setText((TextUtils.isEmpty(mOrderDataBean.jifen_count) ? "0" : mOrderDataBean.jifen_count) + "个");
            if (TextUtils.equals(mOrderDataBean.need_money, "false")) {
                rb_alipay.setVisibility(View.GONE);
                v_view_one.setVisibility(View.GONE);

                rb_wx.setVisibility(View.GONE);
                v_view_two.setVisibility(View.GONE);

                rb_yinlian.setVisibility(View.GONE);
                v_view_three.setVisibility(View.GONE);

                rb_yue.setVisibility(View.GONE);
                v_view_four.setVisibility(View.GONE);

                rb_offline.setVisibility(View.GONE);
                llt_desc.setVisibility(View.GONE);

                pay_way = "BALANCE";
                rg_group.check(R.id.rb_yue);
            } else {

                pay_way = "CHINA_PAY";
                rg_group.check(R.id.rb_offline);
            }
        }
        rg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.rb_alipay:
                        rg_group.check(curr_id);
                        NormalUtils.customShowToast("暂未开通");
                        break;
                    case R.id.rb_wx:
                        rg_group.check(curr_id);
                        NormalUtils.customShowToast("暂未开通");
                        break;
                    case R.id.rb_yinlian:
//                        curr_id = R.id.rb_yinlian;
                        rg_group.check(curr_id);
                        NormalUtils.customShowToast("暂未开通");
                        break;
                    case R.id.rb_yue:
                        rg_group.check(curr_id);
                        NormalUtils.customShowToast("暂未开通");
                        break;
                    case R.id.rb_offline:
                        curr_id = R.id.rb_offline;
                        break;
                }
            }
        });
    }

    @OnClick({R.id.iv_back, R.id.tv_once_pay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_once_pay:
                switch (rg_group.getCheckedRadioButtonId()) {
                    case R.id.rb_alipay:
                        pay_way = "ALIPAY";
                        break;
                    case R.id.rb_wx:
                        pay_way = "WEIXIN";
                        break;
                    case R.id.rb_yinlian:
                        pay_way = "CHINA_PAY";
                        break;
                    case R.id.rb_yue:
                        pay_way = "BALANCE";
                        break;
                    case R.id.rb_offline:
                        pay_way = "CHINA_PAY";
                        break;
                }
                if (mOrderDataBean != null) {
                    if(TextUtils.equals(pay_way,"CHINA_PAY")){
                        DialogUtils.normalDialog(PayActivity.this, "", "您是否已经付款", "已经付款", "取消", true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogUtils.showLoadingDialog(PayActivity.this, "", false);
                                pay(mOrderDataBean.order_number, pay_way);
                            }
                        },null);
                    }else{
                        pay(mOrderDataBean.order_number, pay_way);
                    }
                } else {
                    NormalUtils.customShowToast("未获取到订单信息");
                }
                break;
        }
    }

    private void pay(final String order_number, final String paymentType) {
        NetParamets.pay(PayActivity.this, order_number, paymentType, new NetCallBack<PayResBean>() {
            @Override
            public void backSuccess(PayResBean result) {
                DialogUtils.dismissLoadingDialog();
                PayDataBean mPayDataBean = result.result;
                if (mPayDataBean != null) {
//                    if (TextUtils.equals(mPayDataBean.type, "1")) {
//                        NormalUtils.customShowToast("支付成功");
//                        NormalUtils.sendBroadcast(ActionFlag.PAYSUCCESS, order_number);
//                        finish();
//                    } else {
                    if(TextUtils.equals("WEIXIN",paymentType)){
                        String payInfo = mPayDataBean.xml;
                        PayInfoBean mPayInfoBean = new Gson().fromJson(payInfo,PayInfoBean.class);
                        WXPay(mPayInfoBean);
                    }else if(TextUtils.equals("CHINA_PAY",paymentType)){
                        NormalUtils.sendBroadcast(ActionFlag.PAYSUCCESS, order_number);
                        finish();
                    }else if(TextUtils.equals("ALIPAY",paymentType)){
                        alipay(mPayDataBean.xml);
                    }else{
                        Intent intent = new Intent(PayActivity.this, ShowWebActivity.class);
                        intent.putExtra("titleStr", "支付");
                        intent.putExtra("linkUrl", mPayDataBean.action);
                        intent.putExtra("order_num", order_number);
                        jumpPage(intent);
                    }
//                    }
                }
            }

            @Override
            public void backError(String ex, PayResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private final int SDK_ALIPAY_FLAG = 10002;
    /**
     * 支付宝支付
     * @param orderInfo 订单信息
     */
    private void alipay(String orderInfo){
        if (!TextUtils.isEmpty(orderInfo)) {
            Thread payThread = new Thread(new payRunnable(orderInfo));
            payThread.start();
        }
    }
    class payRunnable implements Runnable{
        String orderInfo = "";
        payRunnable(String orderInfo){
            this.orderInfo = orderInfo;
        }
        @Override
        public void run() {
            PayTask alipay = new PayTask(PayActivity.this);
            Map<String, String> result = alipay.payV2(orderInfo, true);
            Message msg = new Message();
            msg.what = SDK_ALIPAY_FLAG;
            msg.obj = result;
            mHandler.sendMessage(msg);
        }
    }

    private final int SDK_WXPAY_FLAG = 10002;
    private void WXPay(PayInfoBean mPayInfoBean){
        if (!api.isWXAppInstalled()) {
            NormalUtils.customShowToast("未检测到微信客户端");
            return;
        }
        Message msg = Message.obtain();
        msg.obj = mPayInfoBean;
        msg.what = SDK_WXPAY_FLAG;
        mHandler.sendMessageDelayed(msg, 20);
    }
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if(msg.what == SDK_WXPAY_FLAG){
                PayInfoBean mPayInfoBean = (PayInfoBean) msg.obj;
                if(mPayInfoBean != null){
                    PayReq req = new PayReq();
                    req.appId = mPayInfoBean.appId;
                    req.partnerId = mPayInfoBean.partnerId;
                    req.prepayId = mPayInfoBean.prepayId;
                    req.packageValue = mPayInfoBean.pack;
                    req.nonceStr = mPayInfoBean.nonce;
                    req.timeStamp = mPayInfoBean.timestamp;
                    req.sign = mPayInfoBean.signature;
                    CacheUtils.getInstance().saveShared("moneyStr",mOrderDataBean != null ? mOrderDataBean.total_price : "0");
                    api.sendReq(req);
                }else{
                    Intent intent = new Intent(PayActivity.this,PayResultActivity.class);
                    intent.putExtra("price",mOrderDataBean != null ? mOrderDataBean.need_money : "0");
                    jumpPage(intent);
                }
            }else if(msg.what == SDK_ALIPAY_FLAG){
                PayResult result = new PayResult((Map<String, String>) msg.obj);
                String resultStr = result.getResultStatus();
                CacheUtils.getInstance().saveShared("moneyStr",mOrderDataBean != null ? mOrderDataBean.total_price : "0");
                if (TextUtils.equals(resultStr, "9000")) {
                    jumpPage(new Intent(PayActivity.this,PayResultActivity.class));
                } else if (TextUtils.equals(resultStr, "8000")) {
                    NormalUtils.customShowToast("支付结果确认中");
                } else if(TextUtils.equals(resultStr, "6001")){
                    NormalUtils.customShowToast("支付取消："+resultStr);
                }else{
                    jumpPage(new Intent(PayActivity.this,PayResultActivity.class));
                }
            }
        }
    };
}
