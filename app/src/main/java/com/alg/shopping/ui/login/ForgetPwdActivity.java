package com.alg.shopping.ui.login;

import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.UserInfoResBean;
import com.alg.shopping.config.CountTimer;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.AccountValidatorUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgetPwdActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_code)
    EditText et_code;
    @BindView(R.id.et_rest_pwd)
    EditText et_rest_pwd;
    @BindView(R.id.ctv_rest_pwd)
    CheckedTextView ctv_rest_pwd;
    @BindView(R.id.et_pwd)
    EditText et_pwd;
    @BindView(R.id.ctv_pwd)
    CheckedTextView ctv_pwd;
    @BindView(R.id.tv_find_code)
    TextView tv_find_code;

    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(), ActionFlag.LOGINLSUCCESS)){
            finish();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_forget);
    }

    @Override
    protected void initViews() {
        tv_title.setText("忘记密码");
        tv_find_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneStrCode = et_phone.getText().toString();
                if(!AccountValidatorUtils.isMobile(phoneStrCode)){
                    NormalUtils.customShowToast("请正确输入手机号码");
                    return;
                }
                tv_find_code.setClickable(false);
                CountTimer mCountTimer = new CountTimer(Constants.CODELONG,Constants.countDownInterval);
                mCountTimer.setView(tv_find_code);
                mCountTimer.start();
                DialogUtils.showLoadingDialog(ForgetPwdActivity.this,getResources().getString(R.string.loading),true);
                NetParamets.forget_password_code(ForgetPwdActivity.this, phoneStrCode, new NetCallBack<BaseResBean>() {
                    @Override
                    public void backSuccess(BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                    }

                    @Override
                    public void backError(String ex, BaseResBean result) {
                        DialogUtils.dismissLoadingDialog();
                        NormalUtils.customShowToast(ex);
                    }
                });
            }
        });
    }
    private void resetPwd(final String phone, final String password, final String codeStr){
        NetParamets.forget_password(ForgetPwdActivity.this, password,password,codeStr,phone, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                NormalUtils.customShowToast("设置密码成功");
                finish();
//                login(phone,password);
            }

            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @OnClick({R.id.iv_back,R.id.tv_reset_login,R.id.ctv_pwd,R.id.ctv_rest_pwd})
    public void onClick(View view) {
        FastClickCheck.check(view);
        switch (view.getId()) {
            case R.id.ctv_rest_pwd:
                ctv_rest_pwd.toggle();
                if(ctv_rest_pwd.isChecked()){
                    et_rest_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    et_rest_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if(!TextUtils.isEmpty(et_rest_pwd.getText().toString())){
                    et_rest_pwd.setSelection(et_rest_pwd.getText().toString().length());
                }
                break;
            case R.id.ctv_pwd:
                ctv_pwd.toggle();
                if(ctv_pwd.isChecked()){
                    et_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    et_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                if(!TextUtils.isEmpty(et_pwd.getText().toString())){
                    et_pwd.setSelection(et_pwd.getText().toString().length());
                }
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_reset_login:
                String phoneStr = et_phone.getText().toString();
                String codeStr = et_code.getText().toString();
                String pwdStr = et_pwd.getText().toString();
                String restPwdStr = et_rest_pwd.getText().toString();
                if(!AccountValidatorUtils.isMobile(phoneStr)){
                    NormalUtils.customShowToast("请正确输入手机号码");
                    return;
                }
                if(!AccountValidatorUtils.isPassword(pwdStr)){
                    NormalUtils.customShowToast("请正确输入密码");
                    return;
                }
                if(!TextUtils.equals(restPwdStr,pwdStr)){
                    NormalUtils.customShowToast("2次密码不一致");
                    return;
                }
                if(!AccountValidatorUtils.isVerificationCode(codeStr)){
                    NormalUtils.customShowToast("请正确输入验证码");
                    return;
                }
                DialogUtils.showLoadingDialog(ForgetPwdActivity.this,getResources().getString(R.string.loading),true);
                resetPwd(phoneStr,pwdStr,codeStr);
                break;
        }
    }
    private void login(String phoneStr,String pwdStr){
        NetParamets.login(ForgetPwdActivity.this, phoneStr,pwdStr, new NetCallBack<UserInfoResBean>() {
            @Override
            public void backSuccess(UserInfoResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.LOGINLSUCCESS,null);
            }

            @Override
            public void backError(String ex, UserInfoResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
