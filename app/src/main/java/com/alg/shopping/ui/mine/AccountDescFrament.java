package com.alg.shopping.ui.mine;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountRecordResBean;
import com.alg.shopping.bean.res.AccountRecordResultBean;
import com.alg.shopping.bean.res.AccountRecordRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountDescFrament extends BaseFrament implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.lv_list)
    ListView lv_list;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    ListAdapter adapter;

    int page = 0;
    String type = "";
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_account, container, false);
    }

    @Override
    protected void initViews() {
        Bundle mBundle = getArguments();
        if(mBundle != null){
            type = mBundle.getString("type");
        }
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        adapter = new ListAdapter(getActivity(),null,R.layout.account_item);
        lv_list.setAdapter(adapter);
        srf_refresh.autoRefresh();
    }
    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestRecordRewards(0);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestRecordRewards(page+1);
    }
    private class ListAdapter extends CommonBaseAdapter<AccountRecordRowsBean> {

        public ListAdapter(Context context, List<AccountRecordRowsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, AccountRecordRowsBean bean) {
            TextView tv_item0 = holder.getView(R.id.tv_item0);
            TextView tv_item1 = holder.getView(R.id.tv_item1);
            TextView tv_item2 = holder.getView(R.id.tv_item2);
            TextView tv_item3 = holder.getView(R.id.tv_item3);
            String typeStr;
            if(bean.type == 0){
                typeStr = "积分";
            }else if(bean.type == 1){
                typeStr = "农合链";
            }else{
                typeStr = "消费积分";
            }
            tv_item0.setText(typeStr+"("+bean.quantity+")");
            tv_item1.setText(TextUtils.equals(bean.rec_type,"SELF") ? "自身交易" : "他人交易");
            tv_item2.setText(TextUtils.equals(bean.rec_type,"SELF") ? bean.receiptor_tel : bean.who_tel);
            tv_item3.setText(bean.create_date);
        }
    }
    private void requestRecordRewards(final int pageTemp){
        NetParamets.accountRecord(getActivity(), pageTemp, type,new NetCallBack<AccountRecordResBean>() {
            @Override
            public void backSuccess(AccountRecordResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                AccountRecordResultBean mAccountRecordResultBean = result.result;
                if(mAccountRecordResultBean != null){
                    List<AccountRecordRowsBean> rows = mAccountRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, AccountRecordResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
