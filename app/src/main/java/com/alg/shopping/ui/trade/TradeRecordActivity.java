package com.alg.shopping.ui.trade;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.ConcDataBean;
import com.alg.shopping.bean.res.ConcResBean;
import com.alg.shopping.bean.res.ConcResultBean;
import com.alg.shopping.bean.res.ExchangesUserDataBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.sql.CartDataTable;
import com.alg.shopping.sql.CartSqlDao;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class TradeRecordActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener{

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lv_list)
    ListView listView;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_empty)
    TextView tv_empty;

    EntrustListAdapter entrustListAdapter;
    int index = 0;//0-点对点兑换记录，1-成交记录
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_trade_record);
    }

    @Override
    protected void initViews() {
        index = getIntent().getIntExtra("index",0);
        if(index == 0){
            tv_title.setText("点对点兑换记录");
        }else{
            tv_title.setText("成交记录");
        }
        entrustListAdapter = new EntrustListAdapter(this, null, R.layout.trade_record_item);
        listView.setAdapter(entrustListAdapter);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        srf_refresh.autoRefresh();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        conc_record(page+1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        conc_record(0);
    }

    private class EntrustListAdapter extends CommonBaseAdapter<ConcDataBean> {

        public EntrustListAdapter(Context context, List<ConcDataBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final ConcDataBean bean) {
            TextView tv_item0 = holder.getView(R.id.tv_item0);
            TextView tv_item1 = holder.getView(R.id.tv_item1);
            TextView tv_item2 = holder.getView(R.id.tv_item2);
            TextView tv_item3 = holder.getView(R.id.tv_item3);
            TextView tv_item5 = holder.getView(R.id.tv_item5);
            tv_item5.setVisibility(View.GONE);
            LinearLayout llt_menu = holder.getView(R.id.llt_menu);
            llt_menu.setVisibility(View.GONE);
            if(bean != null){
                tv_item0.setText(bean.createDate);
                tv_item1.setText(bean.count);
                tv_item2.setText(TextUtils.equals(bean.type,"BUY") ? "换入" : "换出");
                tv_item3.setText("成交");
            }
        }
    }
    int page = 0;
    private void conc_record(final int pageTemp){
        NetParamets.conc_record(TradeRecordActivity.this, pageTemp,new NetCallBack<ConcResBean>() {
            @Override
            public void backSuccess(ConcResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                ConcResultBean mConcResultBean = result.result;
                if(mConcResultBean != null){
                    List<ConcDataBean> rows = mConcResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(rows == null || rows.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            entrustListAdapter.setData(rows);
                        }
                    }else{
                        if(rows != null && rows.size() > 0){
                            page = pageTemp;
                            entrustListAdapter.addData(rows);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    entrustListAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void backError(String ex, ConcResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
