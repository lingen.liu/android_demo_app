package com.alg.shopping.ui.trade;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.bean.res.SgResBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class TradeOutInActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_exchange_type)
    TextView tv_exchange_type;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.tv_trade_object)
    TextView tv_trade_object;
    @BindView(R.id.tv_trade_num)
    TextView tv_trade_num;
    @BindView(R.id.tv_desc)
    TextView tv_desc;
    @BindView(R.id.tv_exchange)
    TextView tv_exchange;
    SellOrBuyResultBean mSellOrBuyResultBean;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_trade);
    }

    @Override
    protected void initViews() {
        tv_title.setText("点对点兑换");
        mSellOrBuyResultBean = (SellOrBuyResultBean) getIntent().getSerializableExtra("SellOrBuyResultBean");
        if(mSellOrBuyResultBean != null){
            Drawable drawable;
            if(TextUtils.equals(mSellOrBuyResultBean.type,"BUY")){
                tv_exchange_type.setText("换出农合链");
                tv_exchange.setText("立即换出");
                drawable = getResources().getDrawable(R.mipmap.trade_out);
            }else{
                tv_exchange.setText("立即换入");
                tv_exchange_type.setText("换入农合链");
                drawable = getResources().getDrawable(R.mipmap.trade_in);
            }
            drawable.setBounds(0, 0, drawable.getMinimumWidth(),drawable.getMinimumHeight());
            tv_exchange_type.setCompoundDrawables(null, drawable, null, null);
//            float totalP = 0f;
//            try{
//                totalP = Float.parseFloat(mSellOrBuyResultBean.count) * Float.parseFloat(mSellOrBuyResultBean.price);
//            }catch (Exception e){}
            tv_price.setText(mSellOrBuyResultBean.price+"");
            tv_trade_object.setText(mSellOrBuyResultBean.create_user_name);
            tv_trade_num.setText(mSellOrBuyResultBean.count+"个");
            tv_desc.setText("-");
        }
    }
    @OnClick({R.id.iv_back,R.id.tv_exchange})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_exchange:
                // TODO: 2018/9/25 立即换入或换出
                if(mSellOrBuyResultBean == null || TextUtils.isEmpty(mSellOrBuyResultBean.id)){
                    NormalUtils.customShowToast("未获取到交易详情");
                    return;
                }
                DialogUtils.showLoadingDialog(TradeOutInActivity.this,"",false);
                exchange(mSellOrBuyResultBean.id);
                break;
        }
    }
    private void exchange(String id){
        NetParamets.exchange(TradeOutInActivity.this, id, new NetCallBack<SgResBean>() {
            @Override
            public void backSuccess(SgResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast("交易成功");
                finish();
            }

            @Override
            public void backError(String ex, SgResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
