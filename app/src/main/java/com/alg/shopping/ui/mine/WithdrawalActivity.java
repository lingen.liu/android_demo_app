package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.BalanceResBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 提现申请
 */
public class WithdrawalActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_total_limit)
    TextView tv_total_limit;
    @BindView(R.id.et_limit)
    EditText et_limit;
    @BindView(R.id.tv_unit)
    TextView tv_unit;

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_mine_withdrawal);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("提现申请");
        tv_unit.setText(Html.fromHtml("<font color='#333333'><big>&yen;</big></font>"));
        DialogUtils.showLoadingDialog(WithdrawalActivity.this,"",true);
        requestMoney();
    }

    @OnClick({R.id.iv_back, R.id.tv_look_bank, R.id.tv_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_look_bank://查看或绑定我的银行卡
                DialogUtils.showLoadingDialog(WithdrawalActivity.this,"",true);
                requestBankData();
                break;
            case R.id.tv_submit://提  交
                String limit = et_limit.getText().toString();
                if(TextUtils.isEmpty(limit)){
                    NormalUtils.customShowToast(et_limit.getHint().toString());
                    return;
                }
                int limitNum = 0;
                try{
                    limitNum = Integer.parseInt(limit);
                }catch (Exception e){}
                if(limitNum % 100 != 0){
                    NormalUtils.customShowToast("提现额度必须是100的整数倍");
                    return;
                }
                if(limitNum > balance){
                    NormalUtils.customShowToast("提现额度不足");
                    return;
                }
                DialogUtils.showLoadingDialog(WithdrawalActivity.this,"",true);
                submit(limitNum);
                break;
        }
    }
    private void requestBankData() {
        NetParamets.card(WithdrawalActivity.this, new NetCallBack<CardResBean>() {
            @Override
            public void backSuccess(CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                if (result.result != null) {
                    Intent intent = new Intent(WithdrawalActivity.this,MyBankActivity.class);
                    intent.putExtra("CardDataBean",result.result);
                    jumpPage(intent);
                } else {
                    Intent intent = new Intent(WithdrawalActivity.this,AddBankActivity.class);
                    jumpPage(intent);
                }
            }

            @Override
            public void backError(String ex, CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    float balance = 0f;
    private void requestMoney(){
        NetParamets.balance(WithdrawalActivity.this, new NetCallBack<BalanceResBean>() {
            @Override
            public void backSuccess(BalanceResBean result) {
                DialogUtils.dismissLoadingDialog();
                try{
                    balance = Float.parseFloat(result.result.balance);
                }catch (Exception e){}
                tv_total_limit.setText(NormalUtils.priceRedStyle("",result.result.balance));
            }

            @Override
            public void backError(String ex, BalanceResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }


    private void submit(int limitNum){
        NetParamets.withdraw_apply(WithdrawalActivity.this, limitNum, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                NormalUtils.customShowToast("提现已申请");
                requestMoney();
            }

            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });


    }
}
