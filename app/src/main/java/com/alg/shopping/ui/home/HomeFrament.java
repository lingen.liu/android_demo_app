package com.alg.shopping.ui.home;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.BannersBean;
import com.alg.shopping.bean.res.HomeDataBean;
import com.alg.shopping.bean.res.HomeResBean;
import com.alg.shopping.bean.res.HomekjsBean;
import com.alg.shopping.bean.res.PhotoBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.main.MainActivity;
import com.alg.shopping.ui.mine.GainRecordActivity;
import com.alg.shopping.ui.mine.MyOrderListActivity;
import com.alg.shopping.ui.mine.PromotionRecordActivity;
import com.alg.shopping.ui.pub.ShowWebActivity;
import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeFrament extends BaseFrament implements OnRefreshListener {
    @BindView(R.id.mrv_recycle_shortcut)
    RecyclerView mrv_recycle_shortcut;
    @BindView(R.id.mrv_recycle_integral)
    RecyclerView mrv_recycle_integral;
    @BindView(R.id.mrv_recycle_convert)
    RecyclerView mrv_recycle_convert;
    @BindView(R.id.cb_home_banner)
    ConvenientBanner cb_home_banner;
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.llt_convert)
    LinearLayout llt_convert;
    @BindView(R.id.llt_integral)
    LinearLayout llt_integral;
    @BindView(R.id.llt_shortcut)
    LinearLayout llt_shortcut;
    MRecyclerViewAdapter adapter_shortcut;
    MRecyclerViewAdapter adapter_integral;
    MRecyclerViewAdapter adapter_convert;
    List<Object> imageList = new ArrayList<>();
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_home,container,false);
    }

    @Override
    protected void initViews() {
        srl_refresh.setEnableLoadmore(false);
        srl_refresh.setOnRefreshListener(this);
        ininRecycle(mrv_recycle_shortcut);
        ininRecycle(mrv_recycle_integral);
        ininRecycle(mrv_recycle_convert);
        adapter_shortcut = new MRecyclerViewAdapter(R.layout.home_recycle_item,null);
        adapter_integral = new MRecyclerViewAdapter(R.layout.home_recycle_item,null);
        adapter_convert = new MRecyclerViewAdapter(R.layout.home_recycle_item,null);
        adapter_shortcut.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                List<HomekjsBean> mHomekjsBeans = adapter_shortcut.getData();
                if( mHomekjsBeans!= null && mHomekjsBeans.size() > i && !TextUtils.isEmpty(mHomekjsBeans.get(i).id)){
                    Intent intent = new Intent(getActivity(),GoodDescActivity.class);
                    intent.putExtra("goodId",mHomekjsBeans.get(i).id);
                    ((BaseActivity)getActivity()).jumpPage(intent);
                }
            }
        });
        adapter_integral.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                List<HomekjsBean> mHomekjsBeans = adapter_integral.getData();
                if( mHomekjsBeans!= null && mHomekjsBeans.size() > i && !TextUtils.isEmpty(mHomekjsBeans.get(i).id)){
                    Intent intent = new Intent(getActivity(),GoodDescActivity.class);
                    intent.putExtra("goodId",mHomekjsBeans.get(i).id);
                    ((BaseActivity)getActivity()).jumpPage(intent);
                }
            }
        });
        adapter_convert.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                List<HomekjsBean> mHomekjsBeans = adapter_convert.getData();
                if( mHomekjsBeans!= null && mHomekjsBeans.size() > i && !TextUtils.isEmpty(mHomekjsBeans.get(i).id)){
                    Intent intent = new Intent(getActivity(),GoodDescActivity.class);
                    intent.putExtra("goodId",mHomekjsBeans.get(i).id);
                    ((BaseActivity)getActivity()).jumpPage(intent);
                }
            }
        });
        mrv_recycle_shortcut.setAdapter(adapter_shortcut);
        mrv_recycle_integral.setAdapter(adapter_integral);
        mrv_recycle_convert.setAdapter(adapter_convert);
        cb_home_banner.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(imgUrl != null && imgUrl.size() > position){
                    BannersBean mBannersBean = imgUrl.get(position);
                    if(mBannersBean != null && !TextUtils.isEmpty(mBannersBean.url)){
                        Intent intent = new Intent(getActivity(), ShowWebActivity.class);
                        intent.putExtra("titleStr",mBannersBean.title);
                        intent.putExtra("linkUrl",mBannersBean.url);
                        ((BaseActivity)getActivity()).jumpPage(intent);
                    }
                }
            }
        });
        srl_refresh.autoRefresh();
    }
    private void ininRecycle(RecyclerView rcv_view){
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcv_view.setLayoutManager(layoutManager);
        rcv_view.setNestedScrollingEnabled(false);
    }
    @OnClick({R.id.tv_consult,R.id.tv_my_orders,R.id.tv_gain_recorde,R.id.tv_promotion_recorde})
    public void onClick(View view){
        String token = NormalUtils.getLoginUserToken();
        switch (view.getId()){
            case R.id.tv_consult:
                ((MainActivity)getActivity()).rb_consult.setChecked(true);
                break;
            case R.id.tv_my_orders:
                if(!TextUtils.isEmpty(token)){
                    ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),MyOrderListActivity.class));
                }else{
                    NormalUtils.jumpToLogin(getActivity());
                }
                break;
            case R.id.tv_gain_recorde:
                if(!TextUtils.isEmpty(token)){
                    ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),GainRecordActivity.class));
                }else{
                    NormalUtils.jumpToLogin(getActivity());
                }
                break;
            case R.id.tv_promotion_recorde:
                if(!TextUtils.isEmpty(token)){
                    ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),PromotionRecordActivity.class));
                }else{
                    NormalUtils.jumpToLogin(getActivity());
                }
                break;
        }
    }
    @Override
    public void onRefresh(final RefreshLayout refreshlayout) {
        NetParamets.products(getActivity(), new NetCallBack<HomeResBean>() {
            @Override
            public void backSuccess(HomeResBean result) {
                srl_refresh.finishRefresh();
                initUiData(result);
            }

            @Override
            public void backError(String ex, HomeResBean result) {
                NormalUtils.customShowToast(ex);
                srl_refresh.finishRefresh();
                initUiData(result);
            }
        });
    }
    List<BannersBean> imgUrl = new ArrayList<>();
    private void initUiData(HomeResBean result){
        if(result == null){
            return;
        }
        HomeDataBean mHomeDataBean = result.result;
        if(mHomeDataBean == null){
            return;
        }
        List<BannersBean> banners = mHomeDataBean.banners;
        if(banners != null && banners.size() > 0){
            imageList.clear();
            for (BannersBean mBannersBean:banners) {
                if(mBannersBean.photo != null){
                    imageList.add(NetConstants.baseUrl+mBannersBean.photo.path);
                    imgUrl.add(mBannersBean);
                }
            }
        }
        NormalUtils.initBanner(cb_home_banner,imageList,false);

        List<HomekjsBean> kjs = mHomeDataBean.kjs;
        List<HomekjsBean> jifens = mHomeDataBean.jifens;
        List<HomekjsBean> bis = mHomeDataBean.bis;
        if(kjs == null || kjs.size() == 0){
            llt_shortcut.setVisibility(View.GONE);
        }else{
            llt_shortcut.setVisibility(View.VISIBLE);
        }
        if(jifens == null || jifens.size() == 0){
            llt_integral.setVisibility(View.GONE);
        }else{
            llt_integral.setVisibility(View.VISIBLE);
        }
        if(bis == null || bis.size() == 0){
            llt_convert.setVisibility(View.GONE);
        }else{
            llt_convert.setVisibility(View.VISIBLE);
        }
        adapter_shortcut.setNewData(kjs);
        adapter_shortcut.notifyDataSetChanged();
        adapter_integral.setNewData(jifens);
        adapter_integral.notifyDataSetChanged();
        adapter_convert.setNewData(bis);
        adapter_convert.notifyDataSetChanged();
    }
    class MRecyclerViewAdapter extends BaseQuickAdapter<HomekjsBean> {


        public MRecyclerViewAdapter(int layoutResId, List<HomekjsBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, HomekjsBean bean) {
            ImageView iv_good_pic = baseViewHolder.getView(R.id.iv_good_pic);
            TextView tv_good_name = baseViewHolder.getView(R.id.tv_good_name);
            TextView tv_good_price = baseViewHolder.getView(R.id.tv_good_price);
            int imageWidth = (DensityUtil.getScreenWidth()-DensityUtil.dip2px(30))/2;
            ViewGroup.LayoutParams vlps = iv_good_pic.getLayoutParams();
            vlps.width = imageWidth;
            vlps.height = imageWidth;
            iv_good_pic.setLayoutParams(vlps);
            PhotoBean mPhotoBean = bean.photo;
            IMGLoadUtils.LoadCircleImageTwo(R.mipmap.good_default_pic,mPhotoBean != null ? (NetConstants.baseUrl+mPhotoBean.thumb) : R.mipmap.good_default_pic,iv_good_pic);
            tv_good_name.setText(bean.name);

            if(TextUtils.equals("KJ",bean.type)){
                String priceStr ="";
                if(!TextUtils.isEmpty(bean.originalPrice) && !TextUtils.equals(bean.originalPrice,"0")){
                    priceStr = NormalUtils.priceRedStyle("",bean.originalPrice).toString();
                }
                tv_good_price.setText(priceStr);
            }
            if(TextUtils.equals("XF",bean.type)){
                String priceStr ="";
                if(!TextUtils.isEmpty(bean.jifenPrice) && !TextUtils.equals(bean.jifenPrice,"0")){
                    if(TextUtils.isEmpty(priceStr)){
                        priceStr = bean.jifenPrice+"消费积分";
                    }else{
                        priceStr = priceStr+"+"+bean.jifenPrice+"消费积分";
                    }
                }
                tv_good_price.setText(priceStr);
            }
            if(TextUtils.equals("BI",bean.type)){
                String priceStr ="";
                if(!TextUtils.isEmpty(bean.originalPrice) && !TextUtils.equals(bean.originalPrice,"0")){
                    priceStr = NormalUtils.priceRedStyle("",bean.originalPrice).toString();
                }
                if(!TextUtils.isEmpty(bean.biPrice) && !TextUtils.equals(bean.biPrice,"0")){
                    if(TextUtils.isEmpty(priceStr)){
                        priceStr = bean.biPrice+"个农合链";
                    }else{
                        priceStr = priceStr+"+"+bean.biPrice+"个农合链";
                    }
                }
                tv_good_price.setText(priceStr);
            }
        }
    }
}
