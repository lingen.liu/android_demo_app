package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountRecordResultBean;
import com.alg.shopping.bean.res.AccountRecordRowsBean;
import com.alg.shopping.bean.res.SubscribeRecordResBean;
import com.alg.shopping.bean.res.SubscribeRecordResultBean;
import com.alg.shopping.bean.res.SubscribeRecordRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.trade.BuyDescActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TradeRecordFrament extends BaseFrament implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.lv_list)
    ListView lv_list;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    ListAdapter adapter;
    int page = 0;
    int index = 0;//0-申购记录，1-土地权益
    int type = 0;//0-申购申请中/兑换中，1-申请完成/兑换完成
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_tradeframent,container,false);
    }

    @Override
    public void onResume() {
        super.onResume();
        srl_refresh.autoRefresh();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            srl_refresh.autoRefresh();
        }
    }

    @Override
    protected void initViews() {
        Bundle mBundle = getArguments();
        if(mBundle != null){
            index = mBundle.getInt("index",0);
            type = mBundle.getInt("type",0);
        }
        srl_refresh.setOnRefreshListener(this);
        srl_refresh.setOnLoadmoreListener(this);
        adapter = new ListAdapter(getActivity(),null,R.layout.record_item);
        lv_list.setAdapter(adapter);
        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<SubscribeRecordRowsBean> datas = adapter.getData();
                if(datas != null && datas.size() > position){
                    SubscribeRecordRowsBean mSubscribeRecordRowsBean = datas.get(position);
                    if(!TextUtils.isEmpty(mSubscribeRecordRowsBean.id)){
                        if(index == 0){
                            Intent intent = new Intent(getActivity(), BuyDescActivity.class);
                            intent.putExtra("id",mSubscribeRecordRowsBean.id);
                            ((BaseActivity)getActivity()).jumpPage(intent);
                        }else{
                            Intent intent = new Intent(getActivity(), EquityDescActivity.class);
                            intent.putExtra("id",mSubscribeRecordRowsBean.id);
                            ((BaseActivity)getActivity()).jumpPage(intent);
                        }
                    }
                }
            }
        });
        srl_refresh.autoRefresh();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        if(index == 0){
            requestSubscribeData(type,page+1);
        }else if(index == 1){
            requestEquityData(type,page+1);
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        if(index == 0){
            requestSubscribeData(type,0);
        }else if(index == 1){
            requestEquityData(type,0);
        }
    }
    private class ListAdapter extends CommonBaseAdapter<SubscribeRecordRowsBean>{

        public ListAdapter(Context context, List<SubscribeRecordRowsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, SubscribeRecordRowsBean bean) {
            TextView tv_no = holder.getView(R.id.tv_no);
            TextView tv_num = holder.getView(R.id.tv_num);
            TextView tv_type = holder.getView(R.id.tv_type);
            TextView tv_price = holder.getView(R.id.tv_price);
            TextView tv_date = holder.getView(R.id.tv_date);
            if(index == 0){
                tv_no.setText(stringApped("申购数量：",bean.count));
                tv_num.setText(stringApped("申购行情价：",bean.price));
                tv_type.setText(stringAppedOne("申购总价：",bean.total_amount));
                tv_price.setVisibility(View.GONE);
                tv_date.setText(stringApped("申购时间：",bean.create_date));
            }else if(index == 1){
                tv_no.setText(stringApped("序号：",bean.id));
                tv_num.setText(stringApped("交易数量：",bean.count));
                if(TextUtils.equals(bean.type,"JP")){
                    tv_type.setText(stringAppedOne("兑换类型：","竞拍"));
                }else if(TextUtils.equals(bean.type,"DH")){
                    tv_type.setText(stringAppedOne("兑换类型：","兑换"));
                }
                tv_price.setText(stringApped("兑换行情：",bean.price));
                tv_date.setText(stringApped("成交时间：",bean.occ_date));
            }
        }
    }
    private CharSequence stringApped(String name,String value){
        String priceStr = name+"<font color='#222222'>"+value+"</font>";
        return Html.fromHtml(priceStr);
    }
    private CharSequence stringAppedOne(String name,String value){
        String priceStr = name+"<font color='#FE7449'>"+value+"</font>";
        return Html.fromHtml(priceStr);
    }
    private void requestSubscribeData(int type, final int pageTemp){
        NetParamets.subscribe_records(getActivity(), type, pageTemp, new NetCallBack<SubscribeRecordResBean>() {
            @Override
            public void backSuccess(SubscribeRecordResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                SubscribeRecordResultBean mSubscribeRecordResultBean = result.result;
                if(mSubscribeRecordResultBean != null){
                    List<SubscribeRecordRowsBean> rows = mSubscribeRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, SubscribeRecordResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private void requestEquityData(int type, final int pageTemp){
        NetParamets.equity_records(getActivity(), type, pageTemp, new NetCallBack<SubscribeRecordResBean>() {
            @Override
            public void backSuccess(SubscribeRecordResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                SubscribeRecordResultBean mSubscribeRecordResultBean = result.result;
                if(mSubscribeRecordResultBean != null){
                    List<SubscribeRecordRowsBean> rows = mSubscribeRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, SubscribeRecordResBean result) {
                srl_refresh.finishRefresh();
                srl_refresh.finishLoadmore();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
