package com.alg.shopping.ui.trade;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.ExchangeDataBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.bean.res.SellOrBuyResBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.ZxingUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.mine.PointRecordActivity;
import com.yzq.zxinglibrary.common.Constant;

import butterknife.BindView;
import butterknife.OnClick;

public class PointTradeActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_pointtrade);
    }

    @Override
    protected void initViews() {
        tv_title.setText("点对点兑换");
        tv_menu.setVisibility(View.VISIBLE);
        tv_menu.setText("兑换记录");
    }
    @OnClick({R.id.tv_trade_in,R.id.iv_back,R.id.tv_trade_out,R.id.tv_qr_scan,R.id.tv_menu})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_menu:
                Intent intent = new Intent(PointTradeActivity.this, PointRecordActivity.class);
                intent.putExtra("index",0);
                jumpPage(intent);
                break;
            case R.id.tv_trade_in:
                DialogUtils.showLoadingDialog(PointTradeActivity.this,"",true);
                trade_config(view.getId());
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_trade_out:
                DialogUtils.showLoadingDialog(PointTradeActivity.this,"",true);
                trade_config(view.getId());
                break;
            case R.id.tv_qr_scan:
                ZxingUtils.jumpToZxing(PointTradeActivity.this, Constants.REQUEST_CODE_SCAN);
                break;
        }
    }

    private void trade_config(final int id) {
        NetParamets.trade_config(PointTradeActivity.this, new NetCallBack<ExchangeResBean>() {
            @Override
            public void backSuccess(ExchangeResBean result) {
                ExchangeDataBean mExchangeDataBean = result.result;
                if (mExchangeDataBean != null) {
                    final String price = mExchangeDataBean.price;
                    NetParamets.account(PointTradeActivity.this, new NetCallBack<AccountResBean>() {
                        @Override
                        public void backSuccess(AccountResBean result) {
                            DialogUtils.dismissLoadingDialog();
                            if(result.result != null && result.result.account != null) {
                                AccountBean mAccountBean = result.result.account;
                                mAccountBean.price = price;
                                if(id == R.id.tv_trade_in){
                                    DialogUtils.tradeDialog(PointTradeActivity.this,2,"我要换入",mAccountBean,true);
                                }else if(id == R.id.tv_trade_out){
                                    DialogUtils.tradeDialog(PointTradeActivity.this,3,"我要换出",mAccountBean,true);
                                }

                            }
                        }

                        @Override
                        public void backError(String ex, AccountResBean result) {
                            DialogUtils.dismissLoadingDialog();
                            NormalUtils.customShowToast(ex);
                        }
                    });
                }
            }

            @Override
            public void backError(String ex, ExchangeResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == Constants.REQUEST_CODE_SCAN) {
            if (data != null) {
                Message msg = Message.obtain();
                msg.what = 10011;
                msg.obj = data.getStringExtra(Constant.CODED_CONTENT);
                mHandler.sendMessageDelayed(msg, 500);
            }
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
               String id = (String) msg.obj;
               DialogUtils.showLoadingDialog(PointTradeActivity.this,"",false);
               requestData(id);
            } catch (Exception e) {
                NormalUtils.customShowToast("无法识别二维码");
            }
        }
    };

    private void requestData(String id){
        NetParamets.exchange_details(PointTradeActivity.this, id, new NetCallBack<SellOrBuyResBean>() {
            @Override
            public void backSuccess(SellOrBuyResBean result) {
                DialogUtils.dismissLoadingDialog();
                Intent intent = new Intent(PointTradeActivity.this, TradeOutInActivity.class);
                intent.putExtra("SellOrBuyResultBean",result.result);
                jumpPage(intent);
            }

            @Override
            public void backError(String ex, SellOrBuyResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast("二维码已失效");
            }
        });
    }

}
