package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.CardDataBean;
import com.alg.shopping.bean.res.CardResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

/**
 * 我的银行卡
 */
public class MyBankActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;

    @BindViews({R.id.tv_bank_name, R.id.tv_user_name, R.id.tv_bank_num})
    List<TextView> textViews;
    @BindView(R.id.tv_state)
    TextView tv_state;

    CardDataBean mCardDataBean;

    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(), ActionFlag.ADDBANKSUCCESS)){
            requestBankData();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_mine_mybank);
    }

    @Override
    protected void initViews() {
        tv_title.setText("我的银行卡");
        tv_menu.setText("编辑");
        mCardDataBean = (CardDataBean) getIntent().getSerializableExtra("CardDataBean");
        if(mCardDataBean != null){
            tv_menu.setVisibility(View.VISIBLE);
            textViews.get(0).setText(mCardDataBean.kh);
            textViews.get(1).setText("姓名：" + mCardDataBean.realName);
            String bankNum = mCardDataBean.cardNumber;
            if (!TextUtils.isEmpty(bankNum) && bankNum.length() > 4) {
                textViews.get(2).setText(bankNum.substring(bankNum.length() - 4, bankNum.length()));
            }
            tv_state.setText("绑定成功");
        }
    }
    private void requestBankData() {
        NetParamets.card(MyBankActivity.this, new NetCallBack<CardResBean>() {
            @Override
            public void backSuccess(CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                CardDataBean mCardDataBean = result.result;
                if (mCardDataBean != null) {
                    tv_menu.setText("编辑");
                    textViews.get(0).setText(mCardDataBean.kh);
                    textViews.get(1).setText("姓名：" + mCardDataBean.realName);
                    String bankNum = mCardDataBean.cardNumber;
                    if (!TextUtils.isEmpty(bankNum) && bankNum.length() > 4) {
                        textViews.get(2).setText(bankNum.substring(bankNum.length() - 4, bankNum.length()));
                    }
                    tv_state.setText("绑定成功");
                }
            }

            @Override
            public void backError(String ex, CardResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @OnClick({R.id.iv_back, R.id.tv_menu})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_menu:
                Intent intent = new Intent(MyBankActivity.this, AddBankActivity.class);
                intent.putExtra("mCardDataBean",mCardDataBean);
                startActivity(intent);
                break;
        }
    }
}
