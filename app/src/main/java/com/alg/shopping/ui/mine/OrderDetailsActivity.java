package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.AddressDataBean;
import com.alg.shopping.bean.res.OrderDataBean;
import com.alg.shopping.bean.res.OrderDetailResBean;
import com.alg.shopping.bean.res.OrderRowsBean;
import com.alg.shopping.bean.res.OrdersItemsBean;
import com.alg.shopping.bean.res.PhotoBean;
import com.alg.shopping.bean.res.ShippingsBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.pub.PayActivity;
import com.alg.shopping.ui.view.MlistView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

/**
 * 订单详情
 */
public class OrderDetailsActivity extends BaseActivity implements OnRefreshListener{

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindViews({R.id.tv_order_num, R.id.tv_order_status, R.id.tv_order_time, R.id.tv_pay_way, R.id.tv_send_way})
    List<TextView> orderTextViews;
    @BindViews({R.id.tv_name, R.id.tv_phone, R.id.tv_address})
    List<TextView> addrTextViews;
    @BindView(R.id.mlv_list)
    MlistView mlv_list;
    @BindViews({R.id.tv_total, R.id.tv_points, R.id.tv_chain, R.id.tv_freight, R.id.tv_order_money})
    List<TextView> costTextViews;

    @BindView(R.id.tv_btn_gray)
    TextView tv_btn_gray;
    @BindView(R.id.tv_btn_red)
    TextView tv_btn_red;
    @BindView(R.id.tv_btn_green)
    TextView tv_btn_green;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    OrderRowsBean mOrderRowsBean;
    ListAdapter adapter;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_myorder_details);
    }

    @Override
    protected void initViews() {
        tv_title.setText("订单详情");
        mOrderRowsBean = (OrderRowsBean) getIntent().getSerializableExtra("order_desc");
        if(mOrderRowsBean != null){
            adapter = new ListAdapter(OrderDetailsActivity.this, null, R.layout.mine_order_details_item);
            mlv_list.setAdapter(adapter);
        }
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setEnableLoadmore(false);
        srf_refresh.autoRefresh();
    }
    private void btnStatus(String status,TextView tv_btn_gray,TextView tv_btn_red,TextView tv_btn_green){
        tv_btn_gray.setVisibility(View.GONE);
        tv_btn_red.setVisibility(View.VISIBLE);
        tv_btn_green.setVisibility(View.VISIBLE);
        if(TextUtils.equals(status,Constants.orderType[0])){
            tv_btn_green.setText("取消订单");
        }else if(TextUtils.equals(status,Constants.orderType[1])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }else if(TextUtils.equals(status,Constants.orderType[2])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_gray.setVisibility(View.GONE);
            tv_btn_green.setText("签收");
        }else if(TextUtils.equals(status,Constants.orderType[3])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }else{
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }
    }
    private void requestData(String order_id){
        NetParamets.order_details(OrderDetailsActivity.this, order_id, new NetCallBack<OrderDetailResBean>() {
            @Override
            public void backSuccess(OrderDetailResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishRefresh();
                OrderRowsBean mOrderRowsBean = result.result;
                if(mOrderRowsBean != null){
                    adapter.setData(mOrderRowsBean.order_items);
                    adapter.notifyDataSetChanged();
                    orderTextViews.get(0).setText(mOrderRowsBean.order_number);
                    orderTextViews.get(1).setText(mOrderRowsBean.order_status_text);
                    orderTextViews.get(2).setText(mOrderRowsBean.occurred_on);
                    orderTextViews.get(3).setText(TextUtils.isEmpty(mOrderRowsBean.paymentTypeText) ? "-" : mOrderRowsBean.paymentTypeText);//支付方式
                    ShippingsBean mShippingsBean = mOrderRowsBean.shipping;
                    if(mShippingsBean != null){
                        orderTextViews.get(4).setText(mShippingsBean.shipping_name);//配送方式
                    }
                    AddressDataBean mAddressDataBean = mOrderRowsBean.receive_info;
                    if(mAddressDataBean != null){
                        addrTextViews.get(0).setText(mAddressDataBean.receiver);
                        addrTextViews.get(1).setText(mAddressDataBean.mobile);
                        addrTextViews.get(2).setText(mAddressDataBean.province_text+mAddressDataBean.city_text+mAddressDataBean.borough_text+mAddressDataBean.address);
                    }
                    costTextViews.get(0).setText(NormalUtils.priceGrayStyle("",mOrderRowsBean.total_price));//商品总计
                    costTextViews.get(1).setText(mOrderRowsBean.jifen_count);//消费积分
                    costTextViews.get(2).setText(mOrderRowsBean.bi_count);//农合链
                    ShippingsBean mShippingsBeanTemp = mOrderRowsBean.shipping;
                    if(mShippingsBeanTemp != null){
                        costTextViews.get(3).setText(NormalUtils.priceGrayStyle("",mShippingsBeanTemp.shipping_fee));//运费
                    }else{
                        costTextViews.get(3).setText(NormalUtils.priceGrayStyle("",0+""));//运费
                    }
                    costTextViews.get(4).setText(NormalUtils.priceRedStyle("",mOrderRowsBean.total_price));//订单总额
                    btnStatus(mOrderRowsBean.order_status,tv_btn_gray,tv_btn_red,tv_btn_green);
                }
            }
            @Override
            public void backError(String ex, OrderDetailResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishRefresh();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @OnClick({R.id.iv_back, R.id.tv_btn_gray, R.id.tv_btn_red,R.id.tv_btn_green})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_btn_green:
                if(mOrderRowsBean != null){
                    if(TextUtils.equals(tv_btn_green.getText().toString(),"签收")){
                        DialogUtils.showLoadingDialog(OrderDetailsActivity.this,"",true);
                        deliveredOrder(mOrderRowsBean.order_number);
                    }else{
                        jumpToDesc(tv_btn_green.getText().toString(),mOrderRowsBean);
                    }
                }else{
                    NormalUtils.customShowToast("未获取到订单编号");
                }
                break;
            case R.id.tv_btn_gray://取消订单
                if(mOrderRowsBean != null){
                    jumpToDesc(tv_btn_gray.getText().toString(),mOrderRowsBean);
                }else{
                    NormalUtils.customShowToast("未获取到订单编号");
                }
                break;
            case R.id.tv_btn_red://去付款
                if(mOrderRowsBean != null){
                    jumpToDesc(tv_btn_red.getText().toString(),mOrderRowsBean);
                }else{
                    NormalUtils.customShowToast("未获取到订单编号");
                }
                break;
        }
    }
    private void deliveredOrder(String order_id){
        NetParamets.deliveredOrder(OrderDetailsActivity.this, order_id, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ORDERCHANGE,null);
            }
            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private void jumpToDesc(String contentStr, final OrderRowsBean bean){
        if(TextUtils.equals(contentStr,"查看详情")){
            Intent intent = new Intent(OrderDetailsActivity.this, OrderDetailsActivity.class);
            intent.putExtra("order_desc", bean);
            jumpPage(intent);
        }else if(TextUtils.equals(contentStr,"去付款")){
            Intent intent = new Intent(OrderDetailsActivity.this, PayActivity.class);
            OrderDataBean mOrderDataBean = new OrderDataBean();
            mOrderDataBean.id = bean.id;
            mOrderDataBean.order_number = bean.order_number;
            mOrderDataBean.total_price = bean.total_price;
            mOrderDataBean.bi_count = bean.bi_count;
            mOrderDataBean.jifen_count = bean.jifen_count;
            intent.putExtra("order_data",mOrderDataBean);
            jumpPage(intent);
        }else if(TextUtils.equals(contentStr,"取消订单")){
            DialogUtils.normalDialog(OrderDetailsActivity.this, "", "是否确定取消订单", "确定", "取消", true, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.showLoadingDialog(OrderDetailsActivity.this,"",true);
                    cancleOrder(bean.order_number);
                }
            },null);
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        if(mOrderRowsBean != null){
            requestData(mOrderRowsBean.order_number);
        }else{
            srf_refresh.finishRefresh();
        }
    }

    private class ListAdapter extends CommonBaseAdapter<OrdersItemsBean> {

        public ListAdapter(Context context, List<OrdersItemsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, OrdersItemsBean bean) {
            ImageView iv_pic = holder.getView(R.id.iv_pic);
            TextView tv_goods_name = holder.getView(R.id.tv_goods_name);
            TextView tv_deduction = holder.getView(R.id.tv_deduction);
            TextView tv_money = holder.getView(R.id.tv_money);
            TextView tv_number = holder.getView(R.id.tv_number);
            if (bean != null) {
                PhotoBean mPhotoBean = bean.productPhoto;
                if (mPhotoBean != null) {
                    IMGLoadUtils.loadCenterCropPic(NetConstants.baseUrl + mPhotoBean.thumb, iv_pic, R.mipmap.good_default_pic);
                }
                tv_goods_name.setText(bean.product_name);
                String sub_desc = "";
                try {
                    float product_bi_price = Float.parseFloat(bean.product_bi_price);
                    if (product_bi_price != 0) {
                        tv_deduction.setText("(消费农合链：" + product_bi_price);
                    }
                } catch (Exception e) {}
                try {
                    float product_jifen_price = Float.parseFloat(bean.product_jifen_price);
                    if (product_jifen_price != 0) {
                        if (TextUtils.isEmpty(sub_desc)) {
                            tv_deduction.setText("(消费积分：" + product_jifen_price + ")");
                        } else {
                            tv_deduction.setText(tv_deduction.getText().toString() + "消费积分：" + product_jifen_price + ")");
                        }
                    } else {
                        if (TextUtils.isEmpty(sub_desc)) {
                            tv_deduction.setText("");
                        } else {
                            tv_deduction.setText(tv_deduction.getText().toString() + ")");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv_money.setText(Html.fromHtml("<font color='#b3282d'>" + bean.unit_price + "</font>"));
                tv_number.setText("x" + bean.product_quantity);
            }
        }
    }
    private void cancleOrder(String order_id){
        NetParamets.cancleOrder(OrderDetailsActivity.this, order_id, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ORDERCHANGE,null);
                finish();
            }
            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
