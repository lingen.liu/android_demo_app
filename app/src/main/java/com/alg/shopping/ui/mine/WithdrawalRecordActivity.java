package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.UserInfoBean;
import com.alg.shopping.bean.res.WithdrawsRecordDataBean;
import com.alg.shopping.bean.res.WithdrawsRecordResBean;
import com.alg.shopping.bean.res.WithdrawsRecordResultBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.DateUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

public class WithdrawalRecordActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.lv_list)
    ListView lv_list;
    ListAdapter adapter;
    int page = 0;

    @Override
    protected void reciverMesssage(EventBean obj) {

    }
    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_gain_record);
    }

    @Override
    protected void initViews() {
        tv_title.setText("我的提现记录");
        tv_menu.setVisibility(View.INVISIBLE);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        adapter = new ListAdapter(WithdrawalRecordActivity.this,null,R.layout.withdrawal_item);
        lv_list.setAdapter(adapter);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back,R.id.tv_apply_winthdraw})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_apply_winthdraw:
                jumpPage(new Intent(WithdrawalRecordActivity.this, WithdrawalActivity.class));
                break;
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(0);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestData(page+1);
    }
    private class ListAdapter extends CommonBaseAdapter<WithdrawsRecordDataBean> {

        public ListAdapter(Context context, List<WithdrawsRecordDataBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, WithdrawsRecordDataBean bean) {
            TextView tv_user_name = holder.getView(R.id.tv_user_name);
            TextView tv_date = holder.getView(R.id.tv_date);
            TextView tv_money = holder.getView(R.id.tv_money);
            TextView tv_wathdrawal_state = holder.getView(R.id.tv_wathdrawal_state);
            tv_user_name.setText("提现");
            tv_date.setText(bean.createDate);
            tv_money.setText(NormalUtils.priceGrayStyle("",bean.requestPrice));
            if(TextUtils.equals(bean.status,"REQUESTED")){
                tv_wathdrawal_state.setText("已申请");
            }else if(TextUtils.equals(bean.status,"PAID")){
                tv_wathdrawal_state.setText("已完成");
            }
        }
    }
    private void requestData(final int pageTemp){
        NetParamets.withdraws_record(WithdrawalRecordActivity.this, pageTemp, new NetCallBack<WithdrawsRecordResBean>() {
            @Override
            public void backSuccess(WithdrawsRecordResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                WithdrawsRecordResultBean mWithdrawsRecordResultBean = result.result;
                if(mWithdrawsRecordResultBean != null){
                    List<WithdrawsRecordDataBean> rows = mWithdrawsRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(rows == null || rows.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            adapter.setData(rows);
                        }
                    }else{
                        if(rows != null && rows.size() > 0){
                            page = pageTemp;
                            adapter.addData(rows);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                }
                adapter.notifyDataSetChanged();

            }

            @Override
            public void backError(String ex, WithdrawsRecordResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
