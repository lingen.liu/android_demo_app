package com.alg.shopping.ui.trade;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.ConcResBean;
import com.alg.shopping.bean.res.ExchangeDataBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.bean.res.ExchangesBean;
import com.alg.shopping.bean.res.ExchangesDataBean;
import com.alg.shopping.bean.res.ExchangesResBean;
import com.alg.shopping.bean.res.ExchangesUserDataBean;
import com.alg.shopping.bean.res.ExchangesUserResBean;
import com.alg.shopping.bean.res.ExchangesUserResultBean;
import com.alg.shopping.config.CountTimer;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 交易中心
 */
public class TradeFrament extends BaseFrament implements OnLoadmoreListener,OnRefreshListener{
    @BindView(R.id.mlv_list0)
    ListView mlv_list0;
    @BindView(R.id.mlv_list1)
    ListView mlv_list1;
    @BindView(R.id.mlv_entrust)
    ListView mlv_entrust;

    @BindView(R.id.tv_poundage)
    TextView tv_poundage;
    @BindView(R.id.tv_volume_remark)
    TextView tv_volume_remark;
    @BindView(R.id.tv_available_integral)
    TextView tv_available_integral;
    @BindView(R.id.tv_btn_exchange)
    TextView tv_btn_exchange;
    @BindView(R.id.tv_out)
    TextView tv_out;
    @BindView(R.id.tv_in)
    TextView tv_in;
    @BindView(R.id.tv_normal_price)
    TextView tv_normal_price;
    @BindView(R.id.et_volume)
    EditText et_volume;
    @BindView(R.id.tv_arithmetic)
    TextView tv_arithmetic;
    @BindView(R.id.tv_market)
    TextView tv_market;
    @BindView(R.id.tv_jifen_num)
    TextView tv_jifen_num;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_empty)
    TextView tv_empty;

    List<ExchangesBean> buyOrders;
    List<ExchangesBean> sellOrders;
    int page = 0;
    ExchangeDataBean mExchangeDataBean;//配置数据
    boolean isIn = true;
    float num = 0f;//最高换出量

    ListAdapter listAdapter0;
    ListAdapter listAdapter1;
    EntrustListAdapter entrustListAdapter;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_trade, container, false);
    }

    @Override
    protected void initViews() {
        listAdapter0 = new ListAdapter(getActivity(), null, R.layout.trade_list_item);
        listAdapter1 = new ListAdapter(getActivity(), null, R.layout.trade_list_item);
        entrustListAdapter = new EntrustListAdapter(getActivity(), null, R.layout.trade_entrust_item);
        mlv_list0.setAdapter(listAdapter0);
        mlv_list1.setAdapter(listAdapter1);
        mlv_entrust.setAdapter(entrustListAdapter);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        et_volume.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(isIn){
                    String numStr = editable.toString();
                    float numF = 0f;
                    try{
                        numF = Float.parseFloat(numStr);
                    }catch (Exception e){}
                    if(numF > 0){
                        tv_jifen_num.setVisibility(View.VISIBLE);
                        Float f = 0f;
                        try{
                            f = Float.parseFloat(mExchangeDataBean.buySxf);
                        }catch (Exception e){}

                        String priceStr = "0";
                        try{
                            Float priceF = Float.parseFloat(mExchangeDataBean.price);
                            priceStr = NormalUtils.getDecmiTo(numF * (f+1) * priceF);
                        }catch (Exception e){}
                        tv_jifen_num.setText("需扣除"+priceStr+"积分");
                    }else{
                        tv_jifen_num.setVisibility(View.GONE);
                    }
                }
            }
        });
        trade_config();
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            if(!mHandler.hasMessages(10001)){
                mHandler.sendEmptyMessage(10001);
            }
        }else{
            if(mHandler.hasMessages(10001)){
                mHandler.removeMessages(10001);
            }
        }
    }
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 10001:
                    trade_config();
                    mHandler.sendEmptyMessageDelayed(10001,20*1000);
                    break;
            }
        }
    };
    @Override
    public void onPause() {
        super.onPause();
        if(mHandler.hasMessages(10001)){
            mHandler.removeMessages(10001);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mHandler.hasMessages(10001)){
            mHandler.removeMessages(10001);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!mHandler.hasMessages(10001)){
            mHandler.sendEmptyMessageDelayed(10001,20*1000);
        }
    }
    @OnClick({R.id.tv_in, R.id.tv_out,R.id.tv_btn_exchange,R.id.iv_record})
    public void onClick(View view) {
        String token = NormalUtils.getLoginUserToken();
        switch (view.getId()) {
            case R.id.iv_record:
                if(TextUtils.isEmpty(token)){
                    NormalUtils.jumpToLogin(getActivity());
                    return;
                }
                ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),TradeRecordActivity.class));
                break;
            case R.id.tv_in://转入
                isIn = true;
                et_volume.setHint("请输入换入量");
                tv_out.setBackground(getResources().getDrawable(R.drawable.bg_right_corners));
                tv_in.setBackground(getResources().getDrawable(R.drawable.bg_left_corners));
                tv_in.setTextColor(getResources().getColor(R.color.colorwhite));
                tv_out.setTextColor(getResources().getColor(R.color.shallowcenter_color));
                if(mExchangeDataBean != null){
                    Float f = 0f;
                    try{
                        f = Float.parseFloat(mExchangeDataBean.buySxf);
                    }catch (Exception e){}
                    tv_poundage.setText("当前交易手续费"+(f*100)+"%");
                    tv_volume_remark.setText("当日最高换入量：不限");
                    tv_available_integral.setText("可用积分："+mExchangeDataBean.jifenCount);
                    tv_btn_exchange.setText("立即换入");
                }
                tv_jifen_num.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_out://转出
                et_volume.setHint("请输入换出量");
                isIn = false;
                tv_in.setTextColor(getResources().getColor(R.color.shallowcenter_color));
                tv_out.setTextColor(getResources().getColor(R.color.colorwhite));
                tv_out.setBackground(getResources().getDrawable(R.drawable.bg_right_uncorners));
                tv_in.setBackground(getResources().getDrawable(R.drawable.bg_left_uncorners));
                if(mExchangeDataBean != null){
                    Float f = 0f;
                    try{
                        f = Float.parseFloat(mExchangeDataBean.sellSxf);
                    }catch (Exception e){}
                    tv_poundage.setText("当前交易手续费"+(f*100)+"%");
                    try{
                        float nhjCount = Float.parseFloat(mExchangeDataBean.nhjCount);
                        float sellPercent = Float.parseFloat(mExchangeDataBean.sellPercent);
                        num = nhjCount * sellPercent;
                    }catch (Exception e){}
                    tv_volume_remark.setText("当日最高换出量："+num+"个");
                    tv_available_integral.setText("可用农合链："+mExchangeDataBean.nhjCount+"个");
                    tv_btn_exchange.setText("立即换出");
                }
                tv_jifen_num.setVisibility(View.GONE);
                break;
            case R.id.tv_btn_exchange:
                if(TextUtils.isEmpty(token)){
                    NormalUtils.jumpToLogin(getActivity());
                    return;
                }
                String numStr = et_volume.getText().toString();
                Float numF = 0f;
                try{
                    numF = Float.parseFloat(numStr);
                }catch (Exception e){}

                Float price = 0f;
                try{
                    price = Float.parseFloat(mExchangeDataBean.price);
                }catch (Exception e){}
                if(isIn){
                    if(numF <= 0){
                        NormalUtils.customShowToast("购买数量不能少于等于0");
                        return;
                    }
                    DialogUtils.showLoadingDialog(getActivity(),"",true);
                    trade_buy(price+"",numF+"");
                }else{
                    if(numF <= 0){
                        NormalUtils.customShowToast("换出数量不能小于等于0");
                        return;
                    }
                    if(numF > num){
                        NormalUtils.customShowToast("最高换出数量为："+num);
                        return;
                    }
                    DialogUtils.showLoadingDialog(getActivity(),"",true);
                    trade_sell(price+"",numF+"");
                }
                break;
        }
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        user_exchange(page+1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        user_exchange(0);
    }

    private class ListAdapter extends CommonBaseAdapter<ExchangesBean> {

        public ListAdapter(Context context, List<ExchangesBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, ExchangesBean bean) {
            TextView tv_item0 = holder.getView(R.id.tv_item0);
            TextView tv_item1 = holder.getView(R.id.tv_item1);
            TextView tv_item2 = holder.getView(R.id.tv_item2);
            tv_item0.setText(position+1+"");
            tv_item1.setText(bean.price);
            tv_item2.setText(bean.count);
        }
    }

    private class EntrustListAdapter extends CommonBaseAdapter<ExchangesUserDataBean> {

        public EntrustListAdapter(Context context, List<ExchangesUserDataBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final ExchangesUserDataBean bean) {
            TextView tv_item0 = holder.getView(R.id.tv_item0);
            TextView tv_item1 = holder.getView(R.id.tv_item1);
            TextView tv_item2 = holder.getView(R.id.tv_item2);
            TextView tv_item3 = holder.getView(R.id.tv_item3);
            final TextView tv_item4 = holder.getView(R.id.tv_item4);
            TextView tv_item5 = holder.getView(R.id.tv_item5);
            if(bean != null){
                tv_item0.setText(bean.createDate);
                tv_item1.setText(bean.count);
                tv_item5.setText(bean.price);
                tv_item2.setText(TextUtils.equals(bean.type,"BUY") ? "换入" : "换出");
                if(TextUtils.equals(bean.status,"PENDING")){
                    tv_item3.setText("挂单中");
                    tv_item4.setText("撤单");
                    tv_item4.setBackground(getResources().getDrawable(R.drawable.bg_round_corners));
                }else if(TextUtils.equals(bean.status,"PENDED")){
                    tv_item3.setText("已成交");
                    tv_item4.setText("-");
                    tv_item4.setBackground(null);
                }else if(TextUtils.equals(bean.status,"CANCEL")){
                    tv_item3.setText("已撤销");
                    tv_item4.setText("-");
                    tv_item4.setBackground(null);
                }
            }
            tv_item4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TextUtils.equals(tv_item4.getText().toString(),"撤单")){
                        DialogUtils.showLoadingDialog(getActivity(),"",true);
                        UndoTrade(bean.id);
                    }
                }
            });
        }
    }
    private void trade_config(){
        NetParamets.trade_config(getActivity(), new NetCallBack<ExchangeResBean>() {
            @Override
            public void backSuccess(ExchangeResBean result) {
                mExchangeDataBean = result.result;
                if(mExchangeDataBean != null){
                    tv_arithmetic.setText("(一个农合链="+mExchangeDataBean.price+"积分)");
                    tv_market.setText("今日行情价："+mExchangeDataBean.price);
                    if(isIn){
                        String numStr = et_volume.getText().toString();
                        float numF = 0f;
                        try{
                            numF = Float.parseFloat(numStr);
                        }catch (Exception e){}
                        if(numF > 0){
                            Float f_price = 0f;
                            try{
                                f_price = Float.parseFloat(mExchangeDataBean.buySxf);
                            }catch (Exception e){}

                            String priceStr = "0";
                            try{
                                Float priceF = Float.parseFloat(mExchangeDataBean.price);
                                priceStr = NormalUtils.getDecmiTo(numF * (f_price+1) * priceF);
                            }catch (Exception e){}
                            tv_jifen_num.setText("需扣除"+priceStr+"积分");
                            tv_jifen_num.setVisibility(View.VISIBLE);
                        }else{
                            tv_jifen_num.setVisibility(View.GONE);
                        }
                        tv_volume_remark.setText("当日最高换入量：不限");
                        tv_available_integral.setText("可用积分："+mExchangeDataBean.jifenCount+"个");
                        tv_normal_price.setText(NormalUtils.priceGrayStyle("",mExchangeDataBean.price));
                        Float f = 0f;
                        try{
                            f = Float.parseFloat(mExchangeDataBean.buySxf);
                        }catch (Exception e){}
                        tv_poundage.setText("当前交易手续费"+(f*100)+"%");
                    }else{
                        tv_jifen_num.setVisibility(View.GONE);
                        tv_volume_remark.setText("当日最高换出量："+num+"个");
                        tv_available_integral.setText("可用农合链："+mExchangeDataBean.nhjCount+"个");
                        tv_btn_exchange.setText("立即换出");
                        Float f = 0f;
                        try{
                            f = Float.parseFloat(mExchangeDataBean.sellSxf);
                        }catch (Exception e){}
                        tv_poundage.setText("当前交易手续费"+(f*100)+"%");
                    }
                }
                trade_exchanges();
            }

            @Override
            public void backError(String ex, ExchangeResBean result) {
                NormalUtils.customShowToast(ex);
                trade_exchanges();
            }
        });
    }
    private void trade_exchanges(){
        NetParamets.trade_exchanges(getActivity(), new NetCallBack<ExchangesResBean>() {
            @Override
            public void backSuccess(ExchangesResBean result) {
                DialogUtils.dismissLoadingDialog();
                ExchangesDataBean mExchangesDataBean = result.result;
                if(mExchangesDataBean != null){
                    buyOrders = mExchangesDataBean.buyOrders;
                    sellOrders = mExchangesDataBean.sellOrders;
                    listAdapter0.setData(buyOrders);
                    listAdapter1.setData(sellOrders);
                }
                listAdapter0.notifyDataSetChanged();
                listAdapter1.notifyDataSetChanged();
                user_exchange(0);
            }

            @Override
            public void backError(String ex, ExchangesResBean result) {
                NormalUtils.customShowToast(ex);
                user_exchange(0);
            }
        });
    }
    private void user_exchange(final int pageTemp){
        NetParamets.user_exchange(getActivity(), pageTemp,new NetCallBack<ExchangesUserResBean>() {
            @Override
            public void backSuccess(ExchangesUserResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                ExchangesUserResultBean mExchangesUserResultBean = result.result;
                if(mExchangesUserResultBean != null){
                    List<ExchangesUserDataBean> rows = mExchangesUserResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    mlv_entrust.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(rows == null || rows.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            mlv_entrust.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            entrustListAdapter.setData(rows);
                        }
                    }else{
                        if(rows != null && rows.size() > 0){
                            page = pageTemp;
                            entrustListAdapter.addData(rows);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    entrustListAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void backError(String ex, ExchangesUserResBean result) {
                NormalUtils.customShowToast(ex);
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
            }
        });
    }
    private void UndoTrade(String tradeId){
        NetParamets.UndoTrade(getActivity(), tradeId,new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                user_exchange(0);
            }

            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
            }
        });
    }

    private void trade_buy(String singlePrice,String count){
        NetParamets.trade_buy(getActivity(), singlePrice,count,new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.autoRefresh();
            }

            @Override
            public void backError(String ex, BaseResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
    private void trade_sell(String singlePrice,String count){
        NetParamets.trade_sell(getActivity(), singlePrice,count,new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                srf_refresh.autoRefresh();
            }
            @Override
            public void backError(String ex, BaseResBean result) {
                NormalUtils.customShowToast(ex);
                srf_refresh.autoRefresh();
                DialogUtils.dismissLoadingDialog();
            }
        });
    }

}
