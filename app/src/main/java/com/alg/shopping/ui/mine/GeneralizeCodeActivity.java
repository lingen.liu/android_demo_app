package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.ZxingUtils;
import com.alg.shopping.ui.base.BaseActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

public class GeneralizeCodeActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.iv_code)
    ImageView iv_code;
    Bitmap bitmap;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_mine_code);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("我的推广二维码");
        try {
            String contentStr = NetConstants.baseUrl+"qr?p="+NormalUtils.getLoginUserRecommandCode();
            bitmap = ZxingUtils.generateQR(contentStr);
            iv_code.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
            NormalUtils.customShowToast("二维码生成失败");
            IMGLoadUtils.loadCenterCropPic(R.mipmap.qr_code_pic,iv_code,R.mipmap.qr_code_pic);
        }
    }
    @OnClick({R.id.iv_back,R.id.tv_save})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_save:
                try{
                    if(bitmap != null && !bitmap.isRecycled()){
                        try{
                            saveImageToGallery(bitmap);
                            NormalUtils.customShowToast("二维码保存成功");
                        }catch (Exception e){
                            NormalUtils.customShowToast("二维码保存失败");
                        }
                    }else{
                        NormalUtils.customShowToast("未获取到二维码");
                    }
                }catch (Exception e){
                    NormalUtils.customShowToast("未获取到二维码");
                }
                break;
        }
    }

    /*
     * 保存文件，文件名为当前日期
     */
    public void saveImageToGallery(Bitmap bitmap){
        String fileName ;
        File file ;
        String photoName = System.currentTimeMillis()+".jpg";
        if(Build.BRAND .equals("Xiaomi") ){ // 小米手机
            fileName = Environment.getExternalStorageDirectory().getPath()+"/DCIM/Camera/"+photoName;
        }else{  // Meizu 、Oppo
            fileName = Environment.getExternalStorageDirectory().getPath()+"/DCIM/"+photoName;
        }
        file = new File(fileName);

        if(file.exists()){
            file.delete();
        }
        FileOutputStream out;
        try{
            out = new FileOutputStream(file);
            // 格式为 JPEG，照相机拍出的图片为JPEG格式的，PNG格式的不能显示在相册中
            if(bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)){
                out.flush();
                out.close();
                MediaStore.Images.Media.insertImage(this.getContentResolver(), file.getAbsolutePath(),photoName, null);
            }
            // 发送广播，通知刷新图库的显示
            this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + fileName)));
            NormalUtils.customShowToast("保存成功，请在相册中查看");
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(bitmap!= null && !bitmap.isRecycled()){
                bitmap.recycle();
                bitmap = null;
            }
        }catch (Exception e){

        }
    }
}
