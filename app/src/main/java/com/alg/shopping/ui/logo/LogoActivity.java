package com.alg.shopping.ui.logo;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.WindowManager;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.sql.CacheSqlDao;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.main.MainActivity;

/**
 * Created by XB on 2018/2/24.
 */

public class LogoActivity extends BaseActivity {
    final int MSG_FINISH_LAUNCHERACTIVITY = 1001;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_logo);
    }

    @Override
    protected void initViews() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mHandler.sendEmptyMessageDelayed(MSG_FINISH_LAUNCHERACTIVITY, 3000);
        String lastTime = CacheUtils.getInstance().getShared("loginTime");
        if(!TextUtils.isEmpty(lastTime)){
            try{
                int day = (int) ((System.currentTimeMillis() -Long.parseLong(lastTime))/(1000*3600*24));
                if(day > 30){
                    CacheSqlDao.getInance().delete(Constants.USERINFOKEY);
                }
            }catch (Exception e){
                CacheSqlDao.getInance().delete(Constants.USERINFOKEY);
            }
        }else{
            CacheSqlDao.getInance().delete(Constants.USERINFOKEY);
        }
    }
    @Override
    protected void onDestroy() {
        if(mHandler != null){
            mHandler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String isFirstStr = "true";
            try{
                isFirstStr = CacheUtils.getInstance().getShared("isFirst");
            }catch (Exception e){}
            if(TextUtils.equals(isFirstStr,"true")){
                Intent intent = new Intent(LogoActivity.this, GuideActivity.class);
                jumpPage(intent);
            }else{
                Intent intent = new Intent(LogoActivity.this, MainActivity.class);
                intent.putExtra("isOnCreate",true);
                jumpPage(intent);
            }
            finish();
        }
    };
}
