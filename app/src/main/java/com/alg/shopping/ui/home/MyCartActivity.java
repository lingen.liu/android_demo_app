package com.alg.shopping.ui.home;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.GoodDetailDataBean;
import com.alg.shopping.contants.ALGAppaction;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.sql.CartDataTable;
import com.alg.shopping.sql.CartSqlDao;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.pub.SureOrderActivity;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyCartActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.lv_list)
    SwipeMenuListView lv_list;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    ListAdapter adapter;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(ActionFlag.PAYSUCCESS,obj.getFlag())){
            finish();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_mycart);
    }

    @Override
    protected void initViews() {
        List<CartDataTable> mCartDataTables = CartSqlDao.getInance().query();
        tv_total_price.setText(NormalUtils.priceRedStyle("合计",calcTotalPrice(mCartDataTables)+""));
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("我的购物车");
        adapter = new ListAdapter(MyCartActivity.this,mCartDataTables,R.layout.cart_item);
        lv_list.setAdapter(adapter);
        lv_list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, final int index) {
                switch (index) {
                    case 0:
                        List<CartDataTable> mCartDataTables = adapter.getData();
                        if(mCartDataTables != null && mCartDataTables.size() > index){
                            final CartDataTable mCartDataTable = mCartDataTables.get(index);
                            if(mCartDataTable != null){
                                DialogUtils.normalDialog(MyCartActivity.this, "删除", "是否确认从购物车中移除该商品", "确定", "取消", true, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        CartSqlDao.getInance().delete(mCartDataTable.getGoodId());
                                        List<CartDataTable> mCartDataTables = CartSqlDao.getInance().query();
                                        adapter.setData(mCartDataTables);
                                        adapter.notifyDataSetChanged();
                                        NormalUtils.sendBroadcast(ActionFlag.CARTCHANGE,null);
                                    }
                                },null);
                            }
                        }
                        break;
                }
                return false;
            }
        });
        lv_list.setMenuCreator(ALGAppaction.creator);
        srf_refresh.setEnableRefresh(false);
        srf_refresh.setEnableLoadmore(false);
    }
    @OnClick({R.id.iv_back,R.id.tv_clear_cart,R.id.tv_once_buy})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_clear_cart:
                CartSqlDao.getInance().clearAll();
                NormalUtils.sendBroadcast(ActionFlag.CARTCHANGE,null);
                finish();
                break;
            case R.id.tv_once_buy:
                String token = NormalUtils.getLoginUserToken();
                if(TextUtils.isEmpty(token)){
                    NormalUtils.jumpToLogin(MyCartActivity.this);
                    return;
                }
                List<CartDataTable> mCartDataTables = adapter.getData();
                if(mCartDataTables != null && mCartDataTables.size() > 0){
                    Intent intent = new Intent(MyCartActivity.this, SureOrderActivity.class);
                    intent.putExtra("goods",(Serializable) mCartDataTables);
                    jumpPage(intent);
                }else{
                    NormalUtils.customShowToast("暂无商品");
                }
                break;
        }
    }
    private class ListAdapter extends CommonBaseAdapter<CartDataTable>{

        public ListAdapter(Context context, List<CartDataTable> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final CartDataTable bean) {
            ImageView iv_good_pic = holder.getView(R.id.iv_good_pic);
            TextView tv_good_name = holder.getView(R.id.tv_good_name);
            TextView tv_price = holder.getView(R.id.tv_price);
            TextView tv_less = holder.getView(R.id.tv_less);
            TextView tv_good_desc = holder.getView(R.id.tv_good_desc);
            final TextView tv_num = holder.getView(R.id.tv_num);
            TextView tv_more = holder.getView(R.id.tv_more);

            final GoodDetailDataBean mGoodDetailDataBean = new Gson().fromJson(bean.getValueStr(),GoodDetailDataBean.class);
            tv_less.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String numStr = tv_num.getText().toString();
                    int num = Integer.parseInt(numStr);
                    if (num > 1) {
                        num = num - 1;
                        tv_num.setText(num + "");
                        bean.setNum(num);
                        tv_total_price.setText(NormalUtils.priceRedStyle("合计",calcTotalPrice(adapter.getData())+""));
                    }else{
                        NormalUtils.customShowToast("请至少选择一件商品");
                    }
                }
            });
            tv_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mGoodDetailDataBean != null){
                        int inve = mGoodDetailDataBean.inventoryCount;
                        String numMoreStr = tv_num.getText().toString();
                        int numMore = Integer.parseInt(numMoreStr);
                        if (numMore < inve) {
                            numMore = numMore + 1;
                            tv_num.setText(numMore + "");
                            bean.setNum(numMore);
                            tv_total_price.setText(NormalUtils.priceRedStyle("合计",calcTotalPrice(adapter.getData())+""));
                        }else{
                            NormalUtils.customShowToast("库存不足");
                        }
                    }
                }
            });
            if(mGoodDetailDataBean != null){
                tv_good_name.setText(mGoodDetailDataBean.name);
                tv_good_desc.setText(mGoodDetailDataBean.sub_title);
                tv_price.setText(NormalUtils.priceRedStyle("",mGoodDetailDataBean.originalPrice));
                tv_num.setText(bean.getNum()+"");
                if(mGoodDetailDataBean.photo != null){
                    IMGLoadUtils.loadCenterCropPic(NetConstants.baseUrl+mGoodDetailDataBean.photo.thumb,iv_good_pic,R.mipmap.good_default_pic);
                }
            }
        }
    }
    private float calcTotalPrice(List<CartDataTable> mCartDataTables){
        float totalPrice = 0f;
        if(mCartDataTables != null && mCartDataTables.size() > 0){
            for (CartDataTable mCartDataTable:mCartDataTables) {
                GoodDetailDataBean mGoodDetailDataBean = new Gson().fromJson(mCartDataTable.getValueStr(),GoodDetailDataBean.class);
                if(mGoodDetailDataBean != null){
                    totalPrice = totalPrice+(Float.parseFloat(mGoodDetailDataBean.originalPrice) * mCartDataTable.getNum());
                }
            }
        }
        return totalPrice;
    }
}
