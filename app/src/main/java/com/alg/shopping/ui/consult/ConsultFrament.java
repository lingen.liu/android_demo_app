package com.alg.shopping.ui.consult;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseFrament;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;

public class ConsultFrament extends BaseFrament{

    @BindView(R.id.rg_group)
    RadioGroup rg_group;
    @BindView(R.id.f_frament)
    FrameLayout f_frament;
    FragmentManager fm;
    Fragment f_all,f_notice,f_action,f_promotion,f_news,currFrament;
    List<Fragment> mFragments = new ArrayList<>();
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_consult,container,false);
    }

    @Override
    protected void initViews() {
        fm = getChildFragmentManager();
        f_all = new ConsultListFrament();
        Bundle mBundle_all = new Bundle();
        mBundle_all.putString("index", Constants.articlesArray[0]);
        f_all.setArguments(mBundle_all);

        f_notice = new ConsultListFrament();
        Bundle mBundle_notice = new Bundle();
        mBundle_notice.putString("index",Constants.articlesArray[2]);
        f_notice.setArguments(mBundle_notice);

        f_action = new ConsultListFrament();
        Bundle mBundle_action = new Bundle();
        mBundle_action.putString("index",Constants.articlesArray[2]);
        f_action.setArguments(mBundle_action);

        f_promotion = new ConsultListFrament();
        Bundle mBundle_promotion = new Bundle();
        mBundle_promotion.putString("index",Constants.articlesArray[3]);
        f_promotion.setArguments(mBundle_promotion);

        f_news = new ConsultListFrament();
        Bundle mBundle_news = new Bundle();
        mBundle_news.putString("index",Constants.articlesArray[4]);
        f_news.setArguments(mBundle_news);
        mFragments.add(f_all);
        mFragments.add(f_notice);
        mFragments.add(f_action);
        mFragments.add(f_promotion);
        mFragments.add(f_news);
        rg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_all:
                        showBottomFrament(0);
                        break;
                    case R.id.rb_notice:
                        showBottomFrament(1);
                        break;
                    case R.id.rb_action:
                        showBottomFrament(2);
                        break;
                    case R.id.rb_promotion:
                        showBottomFrament(3);
                        break;
                    case R.id.rb_news:
                        showBottomFrament(4);
                        break;
                }
            }
        });
        rg_group.check(R.id.rb_all);
    }
    /**
     *  Frament得显示隐藏
     * @param position 跳转到的Frament的位置
     */
    public void showBottomFrament(int position){
        FragmentTransaction transaction = fm.beginTransaction();
        if(mFragments.size() > position){
            try{
                Fragment to = mFragments.get(position);
                if (currFrament != to) {
                    if (!to.isAdded()) {
                        if(currFrament != null){
                            transaction.hide(currFrament);
                        }
                        transaction.add(R.id.f_frament, to).commit();
                    } else {
                        if(currFrament != null){
                            transaction.hide(currFrament);
                        }
                        transaction.show(to).commit();
                    }
                    currFrament = to;
                }
            }catch (Exception e){
                NormalUtils.LogI(e.toString());
            }
        }
    }
}
