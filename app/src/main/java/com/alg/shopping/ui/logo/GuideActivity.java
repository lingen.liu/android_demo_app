package com.alg.shopping.ui.logo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.main.MainActivity;
import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.holder.Holder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Lenovo on 2017/6/1.
 * 引导界面
 */

public class GuideActivity extends BaseActivity {
    @BindView(R.id.logo_cb_banner)
    ConvenientBanner logo_cb_banner;
    int position;
    float positionOffset;
    int positionOffsetPixels;
    int laststate;

    List<Object> imageList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_guide);
        super.onCreate(savedInstanceState);
        logo_cb_banner.setCanLoop(true);
        imageList.add(R.mipmap.guide_one_pic);
        imageList.add(R.mipmap.guide_two_pic);
        imageList.add(R.mipmap.guide_three_pic);
        logo_cb_banner.setPages(new CBViewHolderCreator() {
            @Override
            public Object createHolder() {
                return new LocalImageHolderView();
            }
        }, imageList);
        logo_cb_banner.setPointViewVisible(true);
        logo_cb_banner.setPageIndicator(new int[]{R.mipmap.ic_page_indicator, R.mipmap.ic_page_indicator_focused});
        logo_cb_banner.setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        logo_cb_banner.setManualPageable(true);
        logo_cb_banner.setCanLoop(false);
        logo_cb_banner.getViewPager().setOffscreenPageLimit(4);
//        logo_cb_banner.setPageTransformer(new AccordionTransformer());
        logo_cb_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int positions, float positionOffsets, int positionOffsetPixelss) {
                position = positions;
                positionOffset = positionOffsets;
                positionOffsetPixels = positionOffsetPixelss;
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (laststate == 1 && state == 0 && position == (imageList.size() - 1) && positionOffset == 0f && positionOffsetPixels == 0) {
                    DialogUtils.dissDialog();
                    Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                    jumpPage(intent);
                    finish();
                } else {
                    laststate = state;
                }
            }
        });
    }

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {

    }

    @Override
    protected void initViews() {

    }

    public void jumpPage(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_from_right, R.anim.fade_out_to_left);
    }


    class LocalImageHolderView implements Holder<Object> {
        private ImageView imageView;

        @Override
        public View createView(Context context) {
            imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            return imageView;
        }

        @Override
        public void UpdateUI(Context context, int position, Object data) {
            IMGLoadUtils.loadCenterCropPic(data,imageView, R.mipmap.img_fail_pic);
        }
    }
    @OnClick({R.id.tv_once_tiyan})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_once_tiyan:
                Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                intent.putExtra("isOnCreate",true);
                jumpPage(intent);
                break;
        }
    }
}
