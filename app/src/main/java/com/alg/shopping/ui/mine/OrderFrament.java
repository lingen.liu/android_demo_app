package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.OrderDataBean;
import com.alg.shopping.bean.res.OrderResBean;
import com.alg.shopping.bean.res.OrderRowsBean;
import com.alg.shopping.bean.res.OrdersItemsBean;
import com.alg.shopping.bean.res.OrdersResBean;
import com.alg.shopping.bean.res.PhotoBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.pub.PayActivity;
import com.alg.shopping.ui.view.MlistView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;

public class OrderFrament extends BaseFrament implements OnRefreshListener, OnLoadmoreListener {
    @BindView(R.id.lv_order)
    ListView lv_order;

    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    ListAdapter adapter;

    String state = "";
    int page = 0;

    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(),ActionFlag.ORDERCHANGE)){
            srf_refresh.autoRefresh();
        }
    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_order, container, false);
    }

    @Override
    protected void initViews() {
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        Bundle mBundle = getArguments();
        if (mBundle != null) {
            state = mBundle.getString("state");
        }
        adapter = new ListAdapter(getActivity(), null, R.layout.mine_order_item);
        lv_order.setAdapter(adapter);
        srf_refresh.autoRefresh();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestData(page + 1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(0);
    }

    private class ListAdapter extends CommonBaseAdapter<OrderRowsBean> {

        public ListAdapter(Context context, List<OrderRowsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final OrderRowsBean bean) {
            TextView tv_order_num = holder.getView(R.id.tv_order_num);
            TextView tv_order_status = holder.getView(R.id.tv_order_status);
            MlistView mlv_list = holder.getView(R.id.mlv_list);
            TextView tv_total_money = holder.getView(R.id.tv_total_money);
            final TextView tv_btn_gray = holder.getView(R.id.tv_btn_gray);
            final TextView tv_btn_red = holder.getView(R.id.tv_btn_red);
            final TextView tv_btn_green = holder.getView(R.id.tv_btn_green);
            tv_order_num.setText(bean.order_number);
            tv_order_status.setText(bean.status_text);
            String freightMoney = bean.shipping_fee;
            if (TextUtils.isEmpty(freightMoney) || "0".equals(freightMoney)) {
                freightMoney = " (包邮)";
            } else {
                freightMoney = " (" + NormalUtils.priceRedStyle("含运费：", bean.shipping_fee) + ")";
            }
            String totalMoney = "总金额：<font color='#b3282d'>" + NormalUtils.priceRedStyle("", bean.total_price) + "</font>" + freightMoney;
            tv_total_money.setText(Html.fromHtml(totalMoney));
            List<OrdersItemsBean> items = bean.items;
            OrderListAdapter mOrderListAdapter = new OrderListAdapter(getActivity(), items, R.layout.mine_order_details_item);
            mlv_list.setAdapter(mOrderListAdapter);

            tv_btn_gray.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String contentStr = tv_btn_gray.getText().toString();
                    jumpToDesc(contentStr,bean);
                }
            });
            tv_btn_red.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String contentStr = tv_btn_red.getText().toString();
                    jumpToDesc(contentStr,bean);
                }
            });
            tv_btn_green.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String contentStr = tv_btn_green.getText().toString();
                    jumpToDesc(contentStr,bean);
                }
            });
            btnStatus(bean.status,tv_btn_gray,tv_btn_red,tv_btn_green);
        }
    }
    private void btnStatus(String status,TextView tv_btn_gray,TextView tv_btn_red,TextView tv_btn_green){
        tv_btn_gray.setVisibility(View.VISIBLE);
        tv_btn_red.setVisibility(View.VISIBLE);
        tv_btn_green.setVisibility(View.VISIBLE);
        if(TextUtils.equals(status,Constants.orderType[0])){
            tv_btn_green.setText("取消订单");
        }else if(TextUtils.equals(status,Constants.orderType[1])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }else if(TextUtils.equals(status,Constants.orderType[2])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_gray.setVisibility(View.VISIBLE);
            tv_btn_green.setText("签收");
        }else if(TextUtils.equals(status,Constants.orderType[3])){
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }else{
            tv_btn_red.setVisibility(View.GONE);
            tv_btn_green.setVisibility(View.GONE);
        }
    }
    private void jumpToDesc(String contentStr, final OrderRowsBean bean){
        if(TextUtils.equals(contentStr,"查看详情")){
            Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
            intent.putExtra("order_desc", bean);
            ((BaseActivity) getActivity()).jumpPage(intent);
        }else if(TextUtils.equals(contentStr,"去付款")){
            Intent intent = new Intent(getActivity(), PayActivity.class);
            OrderDataBean mOrderDataBean = new OrderDataBean();
            mOrderDataBean.id = bean.id;
            mOrderDataBean.order_number = bean.order_number;
            mOrderDataBean.total_price = bean.total_price;
            mOrderDataBean.bi_count = bean.bi_count;
            mOrderDataBean.jifen_count = bean.jifen_count;
            mOrderDataBean.need_money = bean.need_money;
            intent.putExtra("order_data",mOrderDataBean);
            ((BaseActivity) getActivity()).jumpPage(intent);
        }else if(TextUtils.equals(contentStr,"取消订单")){
            DialogUtils.normalDialog(getActivity(), "", "是否确定取消订单", "确定", "取消", true, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.showLoadingDialog(getActivity(),"",true);
                    cancleOrder(bean.order_number);
                }
            },null);
        }else if(TextUtils.equals(contentStr,"签收")){
            DialogUtils.showLoadingDialog(getActivity(),"",true);
            deliveredOrder(bean.order_number);
        }
    }
    private class OrderListAdapter extends CommonBaseAdapter<OrdersItemsBean> {

        public OrderListAdapter(Context context, List<OrdersItemsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, OrdersItemsBean bean) {
            ImageView iv_pic = holder.getView(R.id.iv_pic);
            TextView tv_goods_name = holder.getView(R.id.tv_goods_name);
            TextView tv_deduction = holder.getView(R.id.tv_deduction);
            TextView tv_money = holder.getView(R.id.tv_money);
            TextView tv_number = holder.getView(R.id.tv_number);
            if (bean != null) {
                PhotoBean mPhotoBean = bean.productPhoto;
                if (mPhotoBean != null) {
                    IMGLoadUtils.loadCenterCropPic(NetConstants.baseUrl + mPhotoBean.thumb, iv_pic, R.mipmap.good_default_pic);
                }
                tv_goods_name.setText(bean.product_name);
                String sub_desc = "";
                try {
                    float product_bi_price = Float.parseFloat(bean.product_bi_price);
                    if (product_bi_price != 0) {
                        tv_deduction.setText("(消费农合链：" + product_bi_price);
                    }
                } catch (Exception e) {}
                try {
                    float product_jifen_price = Float.parseFloat(bean.product_jifen_price);
                    if (product_jifen_price != 0) {
                        if (TextUtils.isEmpty(sub_desc)) {
                            tv_deduction.setText("(消费积分：" + product_jifen_price + ")");
                        } else {
                            tv_deduction.setText(tv_deduction.getText().toString() + "消费积分：" + product_jifen_price + ")");
                        }
                    } else {
                        if (TextUtils.isEmpty(sub_desc)) {
                            tv_deduction.setText("");
                        } else {
                            tv_deduction.setText(tv_deduction.getText().toString() + ")");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv_money.setText(Html.fromHtml("<font color='#b3282d'>" + bean.unit_price + "</font>"));
                tv_number.setText("x" + bean.product_quantity);
            }
        }
    }

    private void requestData(final int pageTemp) {
        NetParamets.orders(getActivity(), pageTemp, state, new NetCallBack<OrdersResBean>() {
            @Override
            public void backSuccess(OrdersResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                if (result.result != null) {
                    List<OrderRowsBean> rows = result.result.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_order.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_order.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, OrdersResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
            }
        });
    }
    private void deliveredOrder(String order_id){
        NetParamets.deliveredOrder(getActivity(), order_id, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ORDERCHANGE,null);
            }
            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private void cancleOrder(String order_id){
        NetParamets.cancleOrder(getActivity(), order_id, new NetCallBack<BaseResBean>() {
            @Override
            public void backSuccess(BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ORDERCHANGE,null);
            }
            @Override
            public void backError(String ex, BaseResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
