package com.alg.shopping.ui.pub;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AddressDataBean;
import com.alg.shopping.bean.res.AddressResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by XB on 2018/3/6.
 */

public class AddressChooseActivity extends BaseActivity implements OnRefreshListener {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.lv_list)
    ListView lv_list;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    ListViewAdapter adapter;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(), ActionFlag.ADDRESSCHANGE)){
            queryAddress();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_addresschoose);
    }

    @Override
    protected void initViews() {
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setEnableLoadmore(false);
        tv_title.setText("选择收货地址");
        tv_menu.setVisibility(View.INVISIBLE);
        adapter = new ListViewAdapter(AddressChooseActivity.this,null,R.layout.address_show_layout);
        lv_list.setAdapter(adapter);
        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                List<AddressDataBean> datas = adapter.getData();
                if(datas != null && datas.size() > i){
                    AddressDataBean mAddressDataBean = datas.get(i);
                    NormalUtils.sendBroadcast(ActionFlag.ADDRESSCHANGE,mAddressDataBean);
                    finish();
                }
            }
        });
        DialogUtils.showLoadingDialog(AddressChooseActivity.this,getResources().getString(R.string.loading),true);
        queryAddress();
    }
    private void queryAddress(){
        NetParamets.queryAddress(AddressChooseActivity.this, new NetCallBack<AddressResBean>() {
            @Override
            public void backSuccess(AddressResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                ArrayList<AddressDataBean> mAddressDataBeans = result.result;
                if(mAddressDataBeans != null && mAddressDataBeans.size() > 0){
                    tv_empty.setVisibility(View.GONE);
                    srf_refresh.setVisibility(View.VISIBLE);
                    adapter.setData(mAddressDataBeans);
                }else{
                    adapter.setData(null);
                    tv_empty.setVisibility(View.VISIBLE);
                    srf_refresh.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void backError(String ex, AddressResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        queryAddress();
    }

    class ListViewAdapter extends CommonBaseAdapter<AddressDataBean> {

        public ListViewAdapter(Context context, List<AddressDataBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final AddressDataBean bean) {
            ImageView iv_edit = holder.getView(R.id.iv_edit);
            TextView tv_address_str = holder.getView(R.id.tv_address_str);
            TextView tv_name = holder.getView(R.id.tv_name);
            final CheckBox cb_box = holder.getView(R.id.cb_box);
            tv_name.setText(bean.receiver+"            "+bean.mobile);
            tv_address_str.setText(bean.address);
            iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FastClickCheck.check(view);
                    Intent intent = new Intent(AddressChooseActivity.this, EditAddressActivity.class);
                    intent.putExtra("AddressDataBean",bean);
                    jumpPage(intent);
                }
            });
            cb_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                    if(isChecked){
                        DialogUtils.normalDialog(AddressChooseActivity.this, "", "您确定要更换此收件人地址吗?", "确定", "取消", true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                NormalUtils.sendBroadcast(ActionFlag.ADDRESSCHANGE,bean);
                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                cb_box.setChecked(!isChecked);
                            }
                        });
                    }
                }
            });
        }
    }
    @OnClick({R.id.iv_back,R.id.tv_add_address})
    public void onViewClicked(View view) {
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_address:
                Intent intent = new Intent(AddressChooseActivity.this,EditAddressActivity.class);
                jumpPage(intent);
                break;
        }
    }
}
