package com.alg.shopping.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.OrderRowsBean;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyOrderListActivity extends BaseActivity{
    @BindView(R.id.tb_order_menu)
    TabLayout tb_order_menu;
    @BindView(R.id.vp_pager)
    ViewPager vp_pager;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;

    List<String> menuList = new ArrayList<>();
    FragmentManager fm;
    Fragment f_all_order,f_wait_pay,f_already_pay,f_wait_receive,f_wait_praise,currFrament;
    List<Fragment> mFragments = new ArrayList<>();

    ViewPagerAdapter adapter;

    String order_num = "";

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_myorder_list);
    }

    @Override
    protected void initViews() {
        int index = getIntent().getIntExtra("index",0);
        order_num = getIntent().getStringExtra("order_num");
        fm = getSupportFragmentManager();
        tv_title.setText("我的订单");
        menuList.clear();
        menuList.add("全部");
        menuList.add("待付款");
        menuList.add("待发货");
        menuList.add("待签收");
        menuList.add("待评分");
        tb_order_menu.setTabMode(TabLayout.MODE_FIXED);
        for (int i = 0; i < menuList.size(); i++) {
            tb_order_menu.addTab(tb_order_menu.newTab().setText(menuList.get(i)));
        }
        f_wait_pay = new OrderFrament();
        Bundle mBundle_wait_pay = new Bundle();
        mBundle_wait_pay.putString("state","SUBMITTED");
        f_wait_pay.setArguments(mBundle_wait_pay);
        f_already_pay = new OrderFrament();
        Bundle mBundle_already_pay = new Bundle();
        mBundle_already_pay.putString("state","PAID");
        f_already_pay.setArguments(mBundle_already_pay);
        f_wait_receive = new OrderFrament();
        Bundle mBundle_wait_receive = new Bundle();
        mBundle_wait_receive.putString("state","SHIPPING");
        f_wait_receive.setArguments(mBundle_wait_receive);
        f_wait_praise = new OrderFrament();
        Bundle mBundle_wait_praise = new Bundle();
        mBundle_wait_praise.putString("state","EVALUATE");
        f_wait_praise.setArguments(mBundle_wait_praise);
        f_all_order = new OrderFrament();
        mFragments.add(f_all_order);
        mFragments.add(f_wait_pay);
        mFragments.add(f_already_pay);
        mFragments.add(f_wait_receive);
        mFragments.add(f_wait_praise);

        adapter = new ViewPagerAdapter(fm);
        adapter.setData(mFragments);
        vp_pager.setOffscreenPageLimit(mFragments.size());
        vp_pager.setAdapter(adapter);
        tb_order_menu.setupWithViewPager(vp_pager);
        tb_order_menu.getTabAt(index).select();

        if(!TextUtils.isEmpty(order_num)){
            Intent intent = new Intent(MyOrderListActivity.this,OrderDetailsActivity.class);
            OrderRowsBean mOrderRowsBean = new OrderRowsBean();
            mOrderRowsBean.order_number = order_num;
            intent.putExtra("order_desc", mOrderRowsBean);
            jumpPage(intent);
        }

    }
    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ((OrderFrament)currFrament).onActivityResult(requestCode, resultCode, data);
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> mFraments = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public void setData(List<Fragment> mFraments){
            this.mFraments = mFraments;
        }
        @Override
        public Fragment getItem(int position) {
            return mFraments.get(position);
        }

        @Override
        public int getCount() {
            return mFraments != null ? mFraments.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return menuList.get(position);
        }
    }
}
