package com.alg.shopping.ui.mine;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountListActivity extends BaseActivity{
    @BindView(R.id.tb_order_menu)
    TabLayout tb_order_menu;
    @BindView(R.id.vp_pager)
    ViewPager vp_pager;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;

    List<String> menuList = new ArrayList<>();
    FragmentManager fm;
    Fragment f_all_acount,f_cons_acount,f_bi_acount,f_inte_acount,currFrament;
    List<Fragment> mFragments = new ArrayList<>();

    ViewPagerAdapter adapter;

    String order_num = "";

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_acount_desc);
    }

    @Override
    protected void initViews() {
        int index = getIntent().getIntExtra("index",0);
        order_num = getIntent().getStringExtra("order_num");
        fm = getSupportFragmentManager();
        tv_title.setText("账户明细");
        menuList.clear();
        menuList.add("全部");
        menuList.add("消费积分");
        menuList.add("农合链");
        menuList.add("普通积分");
        tb_order_menu.setTabMode(TabLayout.MODE_FIXED);
        for (int i = 0; i < menuList.size(); i++) {
            tb_order_menu.addTab(tb_order_menu.newTab().setText(menuList.get(i)));
        }
        f_all_acount = new AccountDescFrament();
        f_cons_acount = new AccountDescFrament();
        Bundle mBundle_already_pay = new Bundle();
        mBundle_already_pay.putString("type","1");
        f_cons_acount.setArguments(mBundle_already_pay);
        f_bi_acount = new AccountDescFrament();
        Bundle mBundle_wait_receive = new Bundle();
        mBundle_wait_receive.putString("type","2");
        f_bi_acount.setArguments(mBundle_wait_receive);
        f_inte_acount = new AccountDescFrament();
        Bundle mBundle_wait_praise = new Bundle();
        mBundle_wait_praise.putString("type","3");
        f_inte_acount.setArguments(mBundle_wait_praise);
        mFragments.add(f_all_acount);
        mFragments.add(f_cons_acount);
        mFragments.add(f_bi_acount);
        mFragments.add(f_inte_acount);

        adapter = new ViewPagerAdapter(fm);
        adapter.setData(mFragments);
        vp_pager.setOffscreenPageLimit(mFragments.size());
        vp_pager.setAdapter(adapter);
        tb_order_menu.setupWithViewPager(vp_pager);
        tb_order_menu.getTabAt(index).select();
    }
    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> mFraments = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public void setData(List<Fragment> mFraments){
            this.mFraments = mFraments;
        }
        @Override
        public Fragment getItem(int position) {
            return mFraments.get(position);
        }

        @Override
        public int getCount() {
            return mFraments != null ? mFraments.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return menuList.get(position);
        }
    }
}
