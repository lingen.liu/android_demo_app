package com.alg.shopping.ui.home;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.GoodDetailDataBean;
import com.alg.shopping.bean.res.GoodDetailResBean;
import com.alg.shopping.bean.res.PhotoBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.sql.CartDataTable;
import com.alg.shopping.sql.CartSqlDao;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.bigkoo.convenientbanner.ConvenientBanner;
import com.google.gson.Gson;

import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class GoodDescActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_join_cart)
    TextView tv_join_cart;
    @BindView(R.id.tv_once_buy)
    TextView tv_once_buy;

    @BindView(R.id.wv_view)
    WebView wv_view;
    @BindView(R.id.tv_good_name)
    TextView tv_good_name;
    @BindView(R.id.tv_good_desc)
    TextView tv_good_desc;
    @BindView(R.id.tv_good_price)
    TextView tv_good_price;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.cb_good_banner)
    ConvenientBanner cb_good_banner;

    @BindView(R.id.tv_cart_num)
    TextView tv_cart_num;

    GoodDetailDataBean mGoodDetailDataBean;
    List<Object> imgPath = new ArrayList<>();
    String goodId = "";
    int inventoryCount = 1;//库存总数
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(), ActionFlag.CARTCHANGE)){
            queryCartNum();
        }else if(TextUtils.equals(ActionFlag.PAYSUCCESS,obj.getFlag())){
            finish();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_gooddesc);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("商品详情");
        initWebView();
        goodId = getIntent().getStringExtra("goodId");
        queryCartNum();
        DialogUtils.showLoadingDialog(GoodDescActivity.this,"",true);
        requestData(goodId);
    }
    private void queryCartNum(){
        int num = queryNum();
        if(num != 0){
            tv_cart_num.setVisibility(View.VISIBLE);
            tv_cart_num.setText(num+"");
        }else{
            tv_cart_num.setVisibility(View.GONE);
        }
    }
    private int queryNum(){
        int num = 0;
        List<CartDataTable> mCartDataTables = CartSqlDao.getInance().query();
        if(mCartDataTables != null && mCartDataTables.size() > 0){
            num = mCartDataTables.size();
        }
        return num;
    }
    private void initWebView(){
        WebSettings webSettings = wv_view.getSettings();
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        //设置此属性，可任意比例缩放
        webSettings.setUseWideViewPort(false);
        // 设置不可以缩放
        webSettings.setSupportZoom(false);
        webSettings.setDisplayZoomControls(false);
        // 设置的WebView是否支持变焦
        webSettings.setBuiltInZoomControls(true);
//        wwebSettings.setJavaScriptEnabled(true);//支持js
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        // 自适应 屏幕大小界面
        webSettings.setLoadWithOverviewMode(true);
        wv_view.clearCache(true);
        wv_view.clearHistory();
        wv_view.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                int w = DensityUtil.getScreenWidth();
                int h = View.MeasureSpec.makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED);
                wv_view.measure(w, h);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return true;
            }
        });
        WebChromeClient wvcc = new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
            }
        };
        wv_view.setWebChromeClient(wvcc);
    }
    @OnClick({R.id.iv_back,R.id.tv_less,R.id.tv_more,R.id.llt_cart,R.id.tv_join_cart,R.id.tv_once_buy})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_less:
                String numStr = tv_num.getText().toString();
                int num = Integer.parseInt(numStr);
                if (num > 1) {
                    num = num - 1;
                    tv_num.setText(num + "");
                }else{
                    NormalUtils.customShowToast("请至少选择一件商品");
                }
                break;
            case R.id.tv_more:
                String numMoreStr = tv_num.getText().toString();
                int numMore = Integer.parseInt(numMoreStr);
                if (numMore < inventoryCount) {
                    numMore = numMore + 1;
                    tv_num.setText(numMore + "");
                }else{
                    NormalUtils.customShowToast("库存不足");
                }
                break;
            case R.id.llt_cart:
                int cartNum = queryNum();
                if(cartNum != 0){
                    Intent intent = new Intent(GoodDescActivity.this,MyCartActivity.class);
                    jumpPage(intent);
                }else{
                    NormalUtils.customShowToast("购物车暂无商品");
                }
                break;
            case R.id.tv_join_cart:
                if(mGoodDetailDataBean != null){
                    int goodNum = 1;
                    String goodStr = tv_num.getText().toString();
                    try{
                        goodNum = Integer.parseInt(goodStr);
                    }catch (Exception e){}
                    CartSqlDao.getInance().insert(mGoodDetailDataBean.id,new Gson().toJson(mGoodDetailDataBean),goodNum);
                }
                queryCartNum();
                break;
            case R.id.tv_once_buy:
                if(mGoodDetailDataBean != null){
                    int goodNum = 1;
                    String goodStr = tv_num.getText().toString();
                    try{
                        goodNum = Integer.parseInt(goodStr);
                    }catch (Exception e){}
                    CartSqlDao.getInance().insert(mGoodDetailDataBean.id,new Gson().toJson(mGoodDetailDataBean),goodNum);
                }
                queryCartNum();
                jumpPage(new Intent(GoodDescActivity.this,MyCartActivity.class));
                break;
        }
    }
    private void requestData(String id){
        NetParamets.details(GoodDescActivity.this, id, new NetCallBack<GoodDetailResBean>() {
            @Override
            public void backSuccess(GoodDetailResBean result) {
                DialogUtils.dismissLoadingDialog();
                if(result.result != null){
                    mGoodDetailDataBean = result.result;
                    if(mGoodDetailDataBean != null){
                        List<PhotoBean> photos = mGoodDetailDataBean.photos;
                        if(photos != null && photos.size() > 0){
                            imgPath.clear();
                            for (PhotoBean mPhotoBean:photos) {
                                imgPath.add(NetConstants.baseUrl+mPhotoBean.path);
                            }
                        }
                        LinearLayout.LayoutParams llps = (LinearLayout.LayoutParams) cb_good_banner.getLayoutParams();
                        llps.height = cb_good_banner.getMeasuredWidth();
                        NormalUtils.initBanner(cb_good_banner,imgPath,false);

                        inventoryCount = mGoodDetailDataBean.inventoryCount;
                        CartSqlDao.getInance().update(mGoodDetailDataBean.id,new Gson().toJson(mGoodDetailDataBean),-1);
                        tv_good_name.setText(mGoodDetailDataBean.name);
                        tv_good_desc.setText(mGoodDetailDataBean.sub_title);
                        tv_good_price.setText(NormalUtils.priceRedStyle("",mGoodDetailDataBean.originalPrice));
                        wv_view.loadDataWithBaseURL(null,mGoodDetailDataBean.details, "text/html", "UTF-8", null);
                    }
                }
            }
            @Override
            public void backError(String ex, GoodDetailResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
