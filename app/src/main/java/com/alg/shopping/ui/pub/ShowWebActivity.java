package com.alg.shopping.ui.pub;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by QiuQiu on 2017/12/14.
 */

public class ShowWebActivity extends BaseActivity {
    @BindView(R.id.wv_view)
    WebView wv_view;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.pb_bar)
    ProgressBar pb_bar;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.llt_web_root)
    LinearLayout llt_web_root;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    String titleStr;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(ActionFlag.PAYSUCCESS,obj.getFlag())){
            finish();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_showweb);
    }

    @Override
    protected void initViews() {
        Intent intent = getIntent();
        titleStr = intent.getStringExtra("titleStr");
        String linkUrl = intent.getStringExtra("linkUrl");
        tv_title.setText(titleStr);
        if(!TextUtils.isEmpty(titleStr) && titleStr.contains("支付")){
            tv_menu.setVisibility(View.VISIBLE);
            tv_menu.setText("已支付");
        }
        initWebView();
        if(!TextUtils.isEmpty(linkUrl)){
            if(linkUrl.startsWith("http")){
                wv_view.loadUrl(linkUrl);
            }else{
                wv_view.loadDataWithBaseURL(null, linkUrl, "text/html", "utf-8", null);
            }
        }
    }

    @OnClick({R.id.iv_back,R.id.tv_menu})
    public void onViewClicked(View view){
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_menu:
                String order_num = getIntent().getStringExtra("order_num");
                NormalUtils.sendBroadcast(ActionFlag.PAYSUCCESS,order_num);
                break;
        }
    }
    private void initWebView(){
        NormalUtils.initWevViewSetting(wv_view);
        WebChromeClient wvcc = new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if(TextUtils.equals(title,"about:blank") && TextUtils.isEmpty(titleStr)){
                    tv_title.setText("");
                }else if(!TextUtils.isEmpty(titleStr)){
                    tv_title.setText(titleStr);
                }
            }
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress >= 99) {
                    pb_bar.setVisibility(View.GONE);
                } else {
                    pb_bar.setVisibility(View.VISIBLE);
                    pb_bar.setProgress(newProgress);
                }
            }
        };
        wv_view.setWebChromeClient(wvcc);
        WebViewClient wct = new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        };
        wv_view.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String s, String s1, String s2, String s3, long l) {
                try{
                    if(!TextUtils.isEmpty(s) && s.endsWith(".apk")){
                        Uri uri = Uri.parse(s);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }catch (Exception e){}
            }
        });
        wv_view.setWebViewClient(wct);
    }
}
