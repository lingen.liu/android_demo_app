package com.alg.shopping.ui.main;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.VersionResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.contants.Constants;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.JsonReaderUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.VersionDialog;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.consult.ConsultFrament;
import com.alg.shopping.ui.home.HomeFrament;
import com.alg.shopping.ui.mine.MineFrament;
import com.alg.shopping.ui.mine.MyOrderListActivity;
import com.alg.shopping.ui.trade.ExchangeFrament;
import com.alg.shopping.ui.trade.TradeFrament;
import com.allenliu.versionchecklib.v2.AllenVersionChecker;
import com.allenliu.versionchecklib.v2.builder.DownloadBuilder;
import com.allenliu.versionchecklib.v2.builder.NotificationBuilder;
import com.allenliu.versionchecklib.v2.builder.UIData;
import com.allenliu.versionchecklib.v2.callback.CustomVersionDialogListener;
import com.allenliu.versionchecklib.v2.callback.ForceUpdateListener;
import com.pgyersdk.update.PgyUpdateManager;
import com.pgyersdk.update.UpdateManagerListener;
import com.yzq.zxinglibrary.common.Constant;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {
    @BindView(R.id.f_frament)
    FrameLayout f_frament;
    FragmentManager fm;
    Fragment f_home,f_consult,f_trade,f_mine,currFrament;
    List<Fragment> mFragments = new ArrayList<>();
    int index = -1;//1-跳转到订单列表页

    @BindView(R.id.rb_home)
    RadioButton rb_home;
    @BindView(R.id.rb_consult)
    public RadioButton rb_consult;
    @BindView(R.id.rb_trade)
    RadioButton rb_trade;
    @BindView(R.id.rb_mine)
    RadioButton rb_mine;

    boolean isInCreate = false;
    RadioButton radioButtonId = rb_home;//当前展示的按钮
    int currClikcButtonId = -1;//当前点击的按钮
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(),ActionFlag.LOGINLSUCCESS)){
            mHander.sendEmptyMessageDelayed(1001,1000);
        }else if(TextUtils.equals(obj.getFlag(),ActionFlag.LOGINLOUT)){
            mHander.sendEmptyMessageDelayed(1002,1000);
        }else if(TextUtils.equals(ActionFlag.PAYSUCCESS,obj.getFlag()) && obj.getObj() instanceof String){
            Message msg = Message.obtain();
            msg.obj = obj.getObj();
            msg.what = 1005;
            mHander.sendMessageDelayed(msg,1000);
        }
    }

    private void setCheck(RadioButton rb){
        rb_home.setChecked(false);
        rb_consult.setChecked(false);
        rb_trade.setChecked(false);
        rb_mine.setChecked(false);
        rb.setChecked(true);
    }
    @Override
    protected void onDestroy() {
        if(mHander != null){
            mHander.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
    Handler mHander = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1001:
                    setCheck(rb_mine);
                break;
                case 1002:
                    setCheck(rb_home);
                    break;
                case 1005:
                    setCheck(rb_mine);
                    Message message = Message.obtain();
                    message.obj = msg.obj;
                    message.what = 1006;
                    mHander.sendMessageDelayed(message,1000);
                    break;
                case 1006:
                    Intent intent = new Intent(MainActivity.this,MyOrderListActivity.class);
                    intent.putExtra("order_num",msg.obj+"");
                    jumpPage(intent);
                    break;
            }
        }
    };
    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_main);
    }
    @Override
    protected void initViews() {
        boolean trade = JsonReaderUtils.getInstance().boolForKey("trade");
        if(trade){
            rb_trade.setVisibility(View.VISIBLE);
        }else{
            rb_trade.setVisibility(View.GONE);
        }
        isInCreate = getIntent().getBooleanExtra("isOnCreate",false);
        radioButtonId = rb_home;
        CacheUtils.getInstance().saveShared("versionCode", NormalUtils.getLocalVersionCode()+"");
        fm = getSupportFragmentManager();
        f_home = new HomeFrament();
        f_consult = new ConsultFrament();
        f_trade = new ExchangeFrament();
        f_mine = new MineFrament();
        mFragments.add(f_home);
        mFragments.add(f_consult);
        mFragments.add(f_trade);
        mFragments.add(f_mine);
        rb_home.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    showBottomFrament(0);
                    setCheck(rb_home);
                }
            }
        });
        rb_consult.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    currClikcButtonId = -1;
                    showBottomFrament(1);
                    setCheck(rb_consult);
                }
            }
        });
        rb_trade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    showBottomFrament(2);
                    setCheck(rb_trade);
                }
            }
        });
        rb_mine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    String token = NormalUtils.getLoginUserToken();
                    if(!TextUtils.isEmpty(token)){
                        radioButtonId = rb_mine;
                        showBottomFrament(3);
                        setCheck(rb_mine);
                    }else{
                        rb_mine.setChecked(false);
                        NormalUtils.jumpToLogin(MainActivity.this);
                    }
                }
            }
        });
        showBottomFrament(0);
        checkVersion("5b90ee29ca87a8184c1c7782","bf280499172f572159de3b2ecf2a3881");
    }
    static DownloadBuilder builder;
    private void checkVersion(String id,String api_token){
        NetParamets.versionUpdate(MainActivity.this, id, api_token, new NetCallBack<VersionResBean>() {
            @Override
            public void backSuccess(VersionResBean result) {
                if(result == null){
                    return;
                }
                int versionCode = 0;
                try{
                    versionCode = Integer.parseInt(result.build);
                }catch (Exception e){}
                String url = result.install_url;
                if(versionCode > NormalUtils.getLocalVersionCode()){
                    UIData uiData = UIData
                            .create()
                            .setDownloadUrl(url)
                            .setTitle("检测到新版本")
                            .setContent("发现新版本");
                    if(builder == null){
                        builder = AllenVersionChecker.getInstance().downloadOnly(uiData);
                        builder.setShowNotification(true);
                        builder.setForceRedownload(true);
                        builder.setNotificationBuilder(
                                NotificationBuilder.create()
                                        .setRingtone(false)
                                        .setIcon(R.mipmap.ic_launcher)
                                        .setTicker("农合链")
                                        .setContentTitle("版本更新")
                                        .setContentText("发现新版本")
                        );
                        builder.setShowDownloadFailDialog(true);
                    }
                    builder.setForceUpdateListener(null);
                    builder.setCustomVersionDialogListener(new CustomVersionDialogListener() {
                        @Override
                        public Dialog getCustomVersionDialog(Context context, UIData versionBundle) {
                            VersionDialog baseDialog = new VersionDialog(context, R.style.loading_dialog, R.layout.versoin_dialog_layout);
                            LinearLayout llt_root_dialog = baseDialog.findViewById(R.id.llt_root_dialog);
                            ViewGroup.LayoutParams layoutParams = llt_root_dialog.getLayoutParams();
                            layoutParams.width = DensityUtil.getScreenWidth()-DensityUtil.dip2px(90);
                            llt_root_dialog.setLayoutParams(layoutParams);
                            TextView versionchecklib_version_dialog_cancel = baseDialog.findViewById(R.id.versionchecklib_version_dialog_cancel);
                            versionchecklib_version_dialog_cancel.setText("稍后更新");
                            TextView versionchecklib_version_dialog_commit = baseDialog.findViewById(R.id.versionchecklib_version_dialog_commit);
                            versionchecklib_version_dialog_commit.setText("立即更新");
                            TextView tv_content = baseDialog.findViewById(R.id.tv_content);
                            String contentStr = versionBundle.getContent();
                            if(!TextUtils.isEmpty(contentStr)){
                                contentStr = contentStr.replace("\\n","\n");
                                tv_content.setText(contentStr);
                            }
                            TextView tv_title = baseDialog.findViewById(R.id.tv_title);
                            tv_title.setText(versionBundle.getTitle());
                            TextView tv_curr_version = baseDialog.findViewById(R.id.tv_curr_version);
                            TextView tv_target_version = baseDialog.findViewById(R.id.tv_target_version);
                            Bundle mBundle = versionBundle.getVersionBundle();
                            if(mBundle != null){
                                tv_curr_version.setText(mBundle.getString("currversion"));
                                tv_target_version.setText(mBundle.getString("rotmeversion"));
                            }
                            versionchecklib_version_dialog_commit.setBackground(context.getResources().getDrawable(R.drawable.bg_bottom_right_corners));
                            View v_ve_line = baseDialog.findViewById(R.id.v_ve_line);
                            v_ve_line.setVisibility(View.VISIBLE);
                            versionchecklib_version_dialog_commit.setVisibility(View.VISIBLE);
                            baseDialog.setCanceledOnTouchOutside(true);
                            return baseDialog;
                        }
                    });
                    builder.excuteMission(MainActivity.this);
                }
            }

            @Override
            public void backError(String ex, VersionResBean result) {

            }
        });
    }
    long firstTime = 0;

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 2000) { // 如果两次按键时间间隔大于2秒，则不退出
                    NormalUtils.customShowToast(getResources().getString(R.string.main_finish));
                    firstTime = secondTime;
                    return true;
                } else {
                    NormalUtils.sendBroadcast(ActionFlag.CLOSEALL,null);
                    onBackPressed();
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }
                break;
        }
        return super.onKeyUp(keyCode, event);
    }
    /**
     *  Frament得显示隐藏
     * @param position 跳转到的Frament的位置
     */
    public void showBottomFrament(int position){
        FragmentTransaction transaction = fm.beginTransaction();
        if(mFragments.size() > position){
            try{
                Fragment to = mFragments.get(position);
                if (currFrament != to) {
                    if (!to.isAdded()) {
                        if(currFrament != null){
                            transaction.hide(currFrament);
                        }
                        transaction.add(R.id.f_frament, to).commit();
                    } else {
                        if(currFrament != null){
                            transaction.hide(currFrament);
                        }
                        transaction.show(to).commit();
                    }
                    currFrament = to;
                }
            }catch (Exception e){
                NormalUtils.LogI(e.toString());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(currFrament != null){
            currFrament.onActivityResult(requestCode, resultCode, data);
        }
    }
}
