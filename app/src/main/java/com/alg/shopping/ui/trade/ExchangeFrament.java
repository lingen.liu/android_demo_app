package com.alg.shopping.ui.trade;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.AccountBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.ExchangeDataBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.StatusBarHelper;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.BaseFrament;
import com.bigkoo.convenientbanner.ConvenientBanner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ExchangeFrament extends BaseFrament{
    @BindView(R.id.cb_banner)
    ConvenientBanner cb_banner;
    List<Object> imageList = new ArrayList<>();
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts(LayoutInflater inflater, @Nullable ViewGroup container) {
        view = (ViewGroup) inflater.inflate(R.layout.frament_exchange,container,false);
    }

    @Override
    protected void initViews() {
//        imageList.add(R.mipmap.banner_trade_pic);
//        NormalUtils.initBanner(cb_banner,imageList,true);
//        cb_banner.setPointViewVisible(false);
    }
    @OnClick({R.id.tv_exchange_center,R.id.tv_once_center,R.id.tv_once_subscribe,R.id.tv_once_exchange})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_exchange_center:
                ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),EquityCenterActivity.class));
                break;
            case R.id.tv_once_center:
            case R.id.tv_once_subscribe:
            case R.id.tv_once_exchange:
                String token = NormalUtils.getLoginUserToken();
                if(TextUtils.isEmpty(token)){
                    NormalUtils.jumpToLogin(getActivity());
                    return;
                }
                DialogUtils.showLoadingDialog(getActivity(),"",false);
                trade_config(view.getId());
                break;
        }
    }
    private void trade_config(final int id) {
        NetParamets.trade_config(getActivity(), new NetCallBack<ExchangeResBean>() {
            @Override
            public void backSuccess(ExchangeResBean result) {
                ExchangeDataBean mExchangeDataBean = result.result;
                if (mExchangeDataBean != null) {
                    String price = mExchangeDataBean.price;
                    requestAccount(id,price);
                }
            }

            @Override
            public void backError(String ex, ExchangeResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
    private void requestAccount(final int id, final String price){
        NetParamets.account(getActivity(), new NetCallBack<AccountResBean>() {
            @Override
            public void backSuccess(AccountResBean result) {
                DialogUtils.dismissLoadingDialog();
                if(result.result != null && result.result.account != null){
                    AccountBean mAccountBean = result.result.account;
                    mAccountBean.price = price;
                    switch (id){
                        case R.id.tv_exchange_center:
                            ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),EquityCenterActivity.class));
                            break;
                        case R.id.tv_once_center:
                            DialogUtils.tradeDialog(getActivity(),0,"农合链兑换土地权益",mAccountBean,false);
                            break;
                        case R.id.tv_once_subscribe:
                            DialogUtils.tradeDialog(getActivity(),1,"兑换土地申购农合链",mAccountBean,true);
                            break;
                        case R.id.tv_once_exchange:
                            ((BaseActivity)getActivity()).jumpPage(new Intent(getActivity(),PointTradeActivity.class));
                            break;
                    }
                }
            }

            @Override
            public void backError(String ex, AccountResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
}
