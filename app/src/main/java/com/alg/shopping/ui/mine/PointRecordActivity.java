package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.ExchangesUserDataBean;
import com.alg.shopping.bean.res.ExchangesUserResBean;
import com.alg.shopping.bean.res.ExchangesUserResultBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.trade.TradeCodeActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PointRecordActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.mlv_entrust)
    ListView mlv_entrust;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    int page = 0;
    EntrustListAdapter mEntrustListAdapter;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_pointrecord);
    }

    @Override
    protected void initViews() {
        tv_title.setText("点对点兑换记录");
        mEntrustListAdapter = new EntrustListAdapter(PointRecordActivity.this,null,R.layout.trade_point_item);
        mlv_entrust.setAdapter(mEntrustListAdapter);
        mlv_entrust.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                List<ExchangesUserDataBean> datas = mEntrustListAdapter.getData();
                ExchangesUserDataBean bean = datas.get(i);
                if(TextUtils.equals(bean.status,"PENDING")){
                    Intent intent = new Intent(PointRecordActivity.this, TradeCodeActivity.class);
                    SellOrBuyResultBean mSellOrBuyResultBean = new SellOrBuyResultBean();
                    mSellOrBuyResultBean.limit_time = bean.limit_time;
                    mSellOrBuyResultBean.type = bean.type;
                    mSellOrBuyResultBean.create_user_name = NormalUtils.getLoginUserName();
                    mSellOrBuyResultBean.count = bean.count;
                    mSellOrBuyResultBean.price = bean.price;
                    mSellOrBuyResultBean.id = bean.id;
                    intent.putExtra("data",mSellOrBuyResultBean);
                    jumpPage(intent);
                }else{
                    Intent intent = new Intent(PointRecordActivity.this, PointExchangeDescActivity.class);
                    intent.putExtra("id",bean.id);
                    jumpPage(intent);
                }
            }
        });
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        user_exchange(page+1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        user_exchange(0);
    }
    private void user_exchange(final int pageTemp){
        NetParamets.user_exchange(PointRecordActivity.this, pageTemp,new NetCallBack<ExchangesUserResBean>() {
            @Override
            public void backSuccess(ExchangesUserResBean result) {
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
                ExchangesUserResultBean mExchangesUserResultBean = result.result;
                if(mExchangesUserResultBean != null){
                    List<ExchangesUserDataBean> rows = mExchangesUserResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    mlv_entrust.setVisibility(View.VISIBLE);
                    if(pageTemp == 0){
                        if(rows == null || rows.size() == 0){
                            tv_empty.setVisibility(View.VISIBLE);
                            mlv_entrust.setVisibility(View.GONE);
                        }else{
                            page = pageTemp;
                            mEntrustListAdapter.setData(rows);
                        }
                    }else{
                        if(rows != null && rows.size() > 0){
                            page = pageTemp;
                            mEntrustListAdapter.addData(rows);
                        }else{
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    mEntrustListAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void backError(String ex, ExchangesUserResBean result) {
                NormalUtils.customShowToast(ex);
                srf_refresh.finishLoadmore();
                srf_refresh.finishRefresh();
            }
        });
    }
    private class EntrustListAdapter extends CommonBaseAdapter<ExchangesUserDataBean> {

        public EntrustListAdapter(Context context, List<ExchangesUserDataBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, final ExchangesUserDataBean bean) {
            TextView tv_item_1 = holder.getView(R.id.tv_item_1);
            TextView tv_item_2 = holder.getView(R.id.tv_item_2);
            TextView tv_item_3 = holder.getView(R.id.tv_item_3);
            TextView tv_item_4 = holder.getView(R.id.tv_item_4);
            TextView tv_item_5 = holder.getView(R.id.tv_item_5);
            if(bean != null){
                tv_item_4.setText(bean.price);
                tv_item_1.setText(bean.count);
                tv_item_3.setText(bean.createDate);
                tv_item_5.setText(TextUtils.equals(bean.type,"BUY") ? "换入" : "换出");
                if(TextUtils.equals(bean.status,"PENDING")){
                    tv_item_2.setText("挂单中");
                    tv_item_2.setTextColor(getResources().getColor(R.color.color_black));
                }else if(TextUtils.equals(bean.status,"PENDED")){
                    tv_item_2.setText("已成交");
                    tv_item_2.setTextColor(getResources().getColor(R.color.shallowcenter_color));
                }else if(TextUtils.equals(bean.status,"CANCEL")){
                    tv_item_2.setText("已撤销");
                    tv_item_2.setTextColor(getResources().getColor(R.color.color_red_one));
                }
            }
        }
    }
}
