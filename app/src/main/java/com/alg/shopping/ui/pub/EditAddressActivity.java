package com.alg.shopping.ui.pub;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.AddressCurrResBean;
import com.alg.shopping.bean.res.AddressDataBean;
import com.alg.shopping.bean.res.CityResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DateUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.bigkoo.pickerview.OptionsPickerView;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by XB on 2018/3/5.
 */

public class EditAddressActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.tv_province_choose)
    TextView tv_province_choose;
    @BindView(R.id.tv_city_choose)
    TextView tv_city_choose;
    @BindView(R.id.tv_area_choose)
    TextView tv_area_choose;


    @BindView(R.id.et_address_desc)
    EditText et_address_desc;
    @BindView(R.id.ctv_check)
    CheckedTextView ctv_check;

    @BindView(R.id.tv_province)
    TextView tv_province;
    @BindView(R.id.tv_city)
    TextView tv_city;
    @BindView(R.id.tv_area)
    TextView tv_area;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    String province = "";
    String city = "";
    String area = "";

    String provinceCode = "";
    String cityCode = "";
    String areaCode = "";

    AddressDataBean mAddressDataBean;
    public Map<String,String> provinceMap;
    public Map<String,String> cityMap;
    public Map<String,String> areaMap;

    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_editaddress);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.VISIBLE);
        tv_menu.setText("提交");
        tv_title.setText("新增收货地址");
        mAddressDataBean = (AddressDataBean) getIntent().getSerializableExtra("AddressDataBean");
        if(mAddressDataBean != null){
            province = mAddressDataBean.province_text;
            provinceCode = mAddressDataBean.province;
            city = mAddressDataBean.city_text;
            cityCode = mAddressDataBean.city;
            area = mAddressDataBean.borough_text;
            areaCode = mAddressDataBean.borough;
            tv_title.setText("修改收货地址");
            et_name.setText(mAddressDataBean.receiver);
            et_phone.setText(mAddressDataBean.mobile);
            tv_province_choose.setText("修改");
            tv_city_choose.setText("修改");
            tv_area_choose.setText("修改");

            et_address_desc.setText(mAddressDataBean.address);
            if(TextUtils.equals(mAddressDataBean.is_default,"1")){
                ctv_check.setChecked(true);
            }else{
                ctv_check.setChecked(false);
            }
            tv_province.setText(mAddressDataBean.province_text);
            tv_city.setText(mAddressDataBean.city_text);
            tv_area.setText(mAddressDataBean.borough_text);
        }
        DialogUtils.showLoadingDialog(EditAddressActivity.this,"",true);
        requestCityData("","");
    }
    @OnClick({R.id.iv_back,R.id.tv_menu,R.id.ctv_check,R.id.tv_province_choose,R.id.tv_city_choose,R.id.tv_area_choose})
    public void onViewClicked(View view) {
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.tv_province_choose:
                final String[] provinceArray = mapConverToArray(provinceMap);
                if(provinceArray != null && provinceArray.length > 0){
                    OptionsPickerView mOptionsProvince = DateUtils.initOptionPicker(EditAddressActivity.this, provinceArray, 0, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            getMapNData(provinceMap,provinceArray[options1],0);
                            tv_province.setText(province);
                        }
                    });
                    mOptionsProvince.show(true);
                }else{
                    NormalUtils.customShowToast("未获取到省级数据");
                }

                break;
            case R.id.tv_city_choose:
                if(TextUtils.isEmpty(province)){
                    NormalUtils.customShowToast("未获取到省级数据");
                    return;
                }
                requestCityData(provinceCode,"");
                break;
            case R.id.tv_area_choose:
                if(TextUtils.isEmpty(cityCode)){
                    NormalUtils.customShowToast("未获取到市级数据");
                    return;
                }
                requestCityData("",cityCode);
                break;
            case R.id.ctv_check:
                ctv_check.toggle();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_menu:
                String nameStr = et_name.getText().toString();
                String phoneStr = et_phone.getText().toString();
                String addressStr = et_address_desc.getText().toString();
                String province = tv_province.getText().toString();
                String city = tv_city.getText().toString();
                String area = tv_area.getText().toString();
                if(TextUtils.isEmpty(nameStr)){
                    NormalUtils.customShowToast("请输入收件人姓名");
                    break;
                }
                if(TextUtils.isEmpty(phoneStr)){
                    NormalUtils.customShowToast("请输入手机号码");
                    break;
                }
                if(TextUtils.isEmpty(province)){
                    NormalUtils.customShowToast("请选择省份");
                    break;
                }
                if(TextUtils.isEmpty(city)){
                    NormalUtils.customShowToast("请选择城市");
                    break;
                }
                if(TextUtils.isEmpty(area)){
                    NormalUtils.customShowToast("请选择区/县");
                    break;
                }
                if(TextUtils.isEmpty(addressStr)){
                    NormalUtils.customShowToast("请输入详细地址");
                    break;
                }
                String isDefault = "0";
                if(ctv_check.isChecked()){
                    isDefault = "1";
                }else{
                    isDefault = "0";
                }
                String saId = null;
                if(mAddressDataBean != null){
                    saId = mAddressDataBean.id;
                }
                DialogUtils.showLoadingDialog(EditAddressActivity.this,getResources().getString(R.string.loading),true);
                saveToService(saId,nameStr,phoneStr,addressStr,isDefault,provinceCode,cityCode,areaCode);
                break;
        }
    }
    private void getMapNData(Map<String,String> addressMap,String addressName,int index){
        for (Map.Entry<String, String> entry : addressMap.entrySet()) {
            if(TextUtils.equals(addressName,entry.getValue())){
                if(index == 0){
                    provinceCode = entry.getKey();
                    province = addressName;
                }else if(index == 1){
                    cityCode = entry.getKey();
                    city = addressName;
                }else if(index == 2){
                    areaCode = entry.getKey();
                    area = addressName;
                }
            }
        }
    }
    private String[] mapConverToArray(Map<String,String> addressMap){
        int i = 0;
        if(addressMap != null && addressMap.size() > 0){
            String[] strArray = new String[addressMap.size()];
            for (Map.Entry<String, String> entry : addressMap.entrySet()) {
                strArray[i] = entry.getValue();
                i++;
            }
            return strArray;
        }else{
            return null;
        }
    }
    private void saveToService(String saId, String consignee, String phone, String addr, String isDefault, String province, String city, String area){
        NetParamets.editAddress(EditAddressActivity.this, saId, consignee, phone, addr, isDefault, province, city, area, new NetCallBack<AddressCurrResBean>() {
            @Override
            public void backSuccess(AddressCurrResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.ADDRESSCHANGE,result.result);
                NormalUtils.customShowToast("提交成功");
                finish();
            }

            @Override
            public void backError(String ex, AddressCurrResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
    private void requestCityData(final String province, final String cityTemp){
        NetParamets.queryAddress(EditAddressActivity.this, province, cityTemp, new NetCallBack<CityResBean>() {
            @Override
            public void backSuccess(CityResBean result) {
                DialogUtils.dismissLoadingDialog();
                if(!TextUtils.isEmpty(province)){
                    cityMap = result.result;
                    final String[] cityArray = mapConverToArray(cityMap);
                    if(cityArray != null && cityArray.length > 0){
                        OptionsPickerView mOptionsProvince = DateUtils.initOptionPicker(EditAddressActivity.this, cityArray, 0, new OptionsPickerView.OnOptionsSelectListener() {
                            @Override
                            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                                getMapNData(cityMap,cityArray[options1],1);
                                tv_city.setText(city);
                            }
                        });
                        mOptionsProvince.show(true);
                    }else{
                        NormalUtils.customShowToast("未获取到市级数据");
                    }
                }else if(!TextUtils.isEmpty(city)){
                    areaMap = result.result;
                    final String[] areaArray = mapConverToArray(areaMap);
                    if(areaArray != null && areaArray.length > 0){
                        OptionsPickerView mOptionsProvince = DateUtils.initOptionPicker(EditAddressActivity.this, areaArray, 0, new OptionsPickerView.OnOptionsSelectListener() {
                            @Override
                            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                                getMapNData(areaMap,areaArray[options1],2);
                                tv_area.setText(area);
                            }
                        });
                        mOptionsProvince.show(true);
                    }else{
                        NormalUtils.customShowToast("未获取到区级数据");
                    }
                }else{
                    provinceMap = result.result;
                }
            }
            @Override
            public void backError(String ex, CityResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
