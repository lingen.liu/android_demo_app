package com.alg.shopping.ui.pub;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.FastClickCheck;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by XB on 2018/2/28.
 */

public class PayResultActivity extends BaseActivity {
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_pay_price)
    TextView tv_pay_price;
    int type = 1;//1-成功，其他-失败
    @Override
    protected void reciverMesssage(EventBean obj) {
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_payresult);
    }

    @Override
    protected void initViews() {
        String moneyStr = CacheUtils.getInstance().getShared("moneyStr");
        tv_pay_price.setText(NormalUtils.priceBlackStyle("",moneyStr));
        type = getIntent().getIntExtra("type",0);
        Drawable drawable = null;
        if(type == 1){
            tv_status.setTextColor(getResources().getColor(R.color.switch_color));
            drawable = getResources().getDrawable(R.mipmap.pay_success_pic);
            tv_status.setText("支付成功");
        }else{
            tv_status.setTextColor(getResources().getColor(R.color.color_red));
            drawable = getResources().getDrawable(R.mipmap.pay_fail_pic);
            tv_status.setText("支付失败");
        }
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        tv_status.setCompoundDrawables(null,drawable,null,null);
    }
    @OnClick({R.id.tv_comple})
    public void onClick(View view) {
        FastClickCheck.check(view);
        switch (view.getId()){
            case R.id.tv_comple:
                if(type == 1){
                    NormalUtils.sendBroadcast(ActionFlag.PAYSUCCESS,null);
                }
                onBackPressed();
                break;
        }
    }
}
