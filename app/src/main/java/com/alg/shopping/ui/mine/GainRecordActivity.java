package com.alg.shopping.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.GainRecordResBean;
import com.alg.shopping.bean.res.GainRecordResultBean;
import com.alg.shopping.bean.res.GainRecordRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.home.MyCartActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class GainRecordActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;

    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.lv_list)
    ListView lv_list;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    ListAdapter adapter;

    int page = 0;

    @Override
    protected void reciverMesssage(EventBean obj) {

    }
    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_gain_record);
    }

    @Override
    protected void initViews() {
        tv_title.setText("我的推荐奖励");
        tv_menu.setVisibility(View.INVISIBLE);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        adapter = new ListAdapter(GainRecordActivity.this,null,R.layout.gain_item);
        lv_list.setAdapter(adapter);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestRecordRewards(0);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestRecordRewards(page+1);
    }
    private class ListAdapter extends CommonBaseAdapter<GainRecordRowsBean> {

        public ListAdapter(Context context, List<GainRecordRowsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, GainRecordRowsBean bean) {
            TextView tv_user_name = holder.getView(R.id.tv_user_name);
            TextView tv_date = holder.getView(R.id.tv_date);
            TextView tv_integral = holder.getView(R.id.tv_integral);
            String typeStr = "积分";
            if(bean.type == 0){
                typeStr = "积分";
            }else if(bean.type == 1){
                typeStr = "农合链";
            }else if(bean.type == 2){
                typeStr = "消费积分";
            }
            tv_user_name.setText("【"+bean.tel+"】"+typeStr);
            tv_date.setText(bean.createDate);
            tv_integral.setText("+"+bean.balance+typeStr);
        }
    }
    private void requestRecordRewards(final int pageTemp){
        NetParamets.recordRewards(GainRecordActivity.this, pageTemp, new NetCallBack<GainRecordResBean>() {
            @Override
            public void backSuccess(GainRecordResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                GainRecordResultBean mGainRecordResultBean = result.result;
                if(mGainRecordResultBean != null){
                    List<GainRecordRowsBean> rows = mGainRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, GainRecordResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
