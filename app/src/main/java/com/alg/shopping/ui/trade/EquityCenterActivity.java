package com.alg.shopping.ui.trade;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.base.BaseResBean;
import com.alg.shopping.bean.res.AccountBean;
import com.alg.shopping.bean.res.AccountResBean;
import com.alg.shopping.bean.res.ExchangeDataBean;
import com.alg.shopping.bean.res.ExchangeResBean;
import com.alg.shopping.bean.res.SubscribeRecordResBean;
import com.alg.shopping.bean.res.SubscribeRecordResultBean;
import com.alg.shopping.bean.res.SubscribeRecordRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.view.MlistView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class EquityCenterActivity extends BaseActivity implements OnRefreshListener,OnLoadmoreListener{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.mlv_list)
    ListView lv_list;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.tv_empty)
    TextView tv_empty;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    ListAdapter adapter;
    List<String> listStr;
    int page = 0;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_equitycenter);
    }

    @Override
    protected void initViews() {
        tv_title.setText("权益兑换中心");
        tv_menu.setVisibility(View.VISIBLE);
        tv_menu.setText("立即兑换");
        String[] strArray = getResources().getStringArray(R.array.exchange_menu);
        listStr = Arrays.asList(strArray);
        adapter = new ListAdapter(EquityCenterActivity.this,null,R.layout.equitycenter_item);
        lv_list.setAdapter(adapter);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setOnLoadmoreListener(this);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back,R.id.tv_menu})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_menu:
                // TODO: 2018/9/25 立即兑换
                DialogUtils.showLoadingDialog(EquityCenterActivity.this,"",true);
                trade_config();
                break;
        }
    }
    private void trade_config() {
        NetParamets.trade_config(EquityCenterActivity.this, new NetCallBack<ExchangeResBean>() {
            @Override
            public void backSuccess(ExchangeResBean result) {
                ExchangeDataBean mExchangeDataBean = result.result;
                if (mExchangeDataBean != null) {
                    final String price = mExchangeDataBean.price;
                    NetParamets.account(EquityCenterActivity.this, new NetCallBack<AccountResBean>() {
                        @Override
                        public void backSuccess(AccountResBean result) {
                            DialogUtils.dismissLoadingDialog();
                            AccountBean mAccountBean = result.result.account;
                            mAccountBean.price = price;
                            DialogUtils.tradeDialog(EquityCenterActivity.this,0,"农合链兑换土地权益",mAccountBean,false);
                        }
                        @Override
                        public void backError(String ex, AccountResBean result) {
                            DialogUtils.dismissLoadingDialog();
                            NormalUtils.customShowToast(ex);
                        }
                    });
                }
            }

            @Override
            public void backError(String ex, ExchangeResBean result) {
                NormalUtils.customShowToast(ex);
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        requestData(page+1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(0);
    }

    private class ListAdapter extends CommonBaseAdapter<SubscribeRecordRowsBean>{

        public ListAdapter(Context context, List<SubscribeRecordRowsBean> datas, int layoutId) {
            super(context, datas, layoutId);
        }

        @Override
        public void convert(CommonBaseHolder holder, int position, SubscribeRecordRowsBean bean) {
            TextView tv_no = holder.getView(R.id.tv_no);
            TextView tv_phone = holder.getView(R.id.tv_phone);
            TextView tv_status = holder.getView(R.id.tv_status);

            tv_no.setText(bean.tq_number);
            tv_phone.setText(bean.user_name);
            String statuStr = "";
            if(TextUtils.equals(bean.status,"COMPLETED")){
                statuStr = "已完成";
            }else if(TextUtils.equals(bean.status,"PENDING")){
                statuStr = "未申请";
            }else if(TextUtils.equals(bean.status,"PENDED")){
                statuStr = "已申请";
            }else if(TextUtils.equals(bean.status,"CANCEL")){
                statuStr = "已取消";
            }
            tv_status.setText(statuStr);


//            MlistView mlv_list = holder.getView(R.id.mlv_list);
//            ListOneAdapter adapter_one = new ListOneAdapter(EquityCenterActivity.this,listStr,R.layout.list_item_item);
//            adapter_one.setOtherData(bean);
//            mlv_list.setAdapter(adapter_one);
//
//            float totalP = 0f;
//            try{
//                totalP = Float.parseFloat(bean.price)*Float.parseFloat(bean.count);
//            }catch (Exception e){}
//            tv_num.setText(NormalUtils.getDecmiTo(totalP));
        }
    }
    private class ListOneAdapter extends CommonBaseAdapter<String>{
        SubscribeRecordRowsBean mSubscribeRecordRowsBean;
        public ListOneAdapter(Context context, List<String> datas, int layoutId) {
            super(context, datas, layoutId);
        }
        public void setOtherData(SubscribeRecordRowsBean mSubscribeRecordRowsBean){
            this.mSubscribeRecordRowsBean = mSubscribeRecordRowsBean;
        }
        @Override
        public void convert(CommonBaseHolder holder, int position, String bean) {
            TextView tv_name = holder.getView(R.id.tv_name);
            TextView tv_value = holder.getView(R.id.tv_value);
            tv_name.setText(bean);
            switch (position){
                case 0:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.user_id : "");
                    break;
                case 1:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.user_name : "");
                    break;
                case 2:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.id : "");
                    break;
                case 3:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.create_date : "");
                    break;
                case 4:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.count : "");
                    break;
                case 5:
                    String statuStr = "";
                    if(TextUtils.equals(mSubscribeRecordRowsBean.status,"COMPLETED")){
                        statuStr = "已完成";
                    }else if(TextUtils.equals(mSubscribeRecordRowsBean.status,"PENDING")){
                        statuStr = "未申请";
                    }else if(TextUtils.equals(mSubscribeRecordRowsBean.status,"PENDED")){
                        statuStr = "已申请";
                    }else if(TextUtils.equals(mSubscribeRecordRowsBean.status,"CANCEL")){
                        statuStr = "已取消";
                    }
                    tv_value.setText(statuStr);
                    break;
                case 6:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.price : "");
                    break;
                case 7:
                    String type = "";
                    if(TextUtils.equals(mSubscribeRecordRowsBean.type,"DH")){
                        type = "兑换";
                    }else if(TextUtils.equals(mSubscribeRecordRowsBean.status,"JP")){
                        type = "竞拍";
                    }
                    tv_value.setText(type);
                    break;
                case 8:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.occ_date : "");
                    break;
                case 9:
                    float totalPrice = 0f;
                    try {
                        totalPrice = Float.parseFloat(mSubscribeRecordRowsBean.price)*Float.parseFloat(mSubscribeRecordRowsBean.count);
                    }catch (Exception e){}
                    tv_value.setText(totalPrice+"");
                    break;
                case 10:
                    tv_value.setText(mSubscribeRecordRowsBean != null ? mSubscribeRecordRowsBean.td_number : "");
                    break;
            }
        }
    }
    private void requestData(final int pageTemp){
        NetParamets.equity_center(EquityCenterActivity.this, pageTemp, new NetCallBack<SubscribeRecordResBean>() {
            @Override
            public void backSuccess(SubscribeRecordResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
                SubscribeRecordResultBean mSubscribeRecordResultBean = result.result;
                if(mSubscribeRecordResultBean != null){
                    List<SubscribeRecordRowsBean> rows = mSubscribeRecordResultBean.rows;
                    tv_empty.setVisibility(View.GONE);
                    lv_list.setVisibility(View.VISIBLE);
                    if (pageTemp == 0) {
                        if (result.result.rows == null || result.result.rows.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            lv_list.setVisibility(View.GONE);
                        } else {
                            page = pageTemp;
                            rows = result.result.rows;
                            adapter.setData(rows);
                        }
                    } else {
                        if (rows != null && rows.size() > 0) {
                            page = pageTemp;
                            adapter.addData(rows);
                        } else {
                            NormalUtils.customShowToast("暂无更多数据");
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void backError(String ex, SubscribeRecordResBean result) {
                NormalUtils.customShowToast(ex);
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                DialogUtils.dismissLoadingDialog();
            }
        });
    }
}
