package com.alg.shopping.ui.login;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.UserInfoResBean;
import com.alg.shopping.contants.ActionFlag;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.CacheUtils;
import com.alg.shopping.tools.DialogUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.et_username)
    EditText et_username;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.cb_box)
    CheckBox cb_box;
    @BindView(R.id.tv_login)
    TextView tv_login;
    @BindView(R.id.tv_regiest)
    TextView tv_regiest;
    @BindView(R.id.tv_forget_pwd)
    TextView tv_forget_pwd;
    @Override
    protected void reciverMesssage(EventBean obj) {
        if(TextUtils.equals(obj.getFlag(),ActionFlag.LOGINLSUCCESS)){
            finish();
        }
    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setVisibility(View.INVISIBLE);
    }
    @OnClick({R.id.iv_back,R.id.tv_login,R.id.tv_regiest,R.id.tv_forget_pwd})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_login:
                String username = et_username.getText().toString();
                String password = et_password.getText().toString();
                if(TextUtils.isEmpty(username)) {
                    NormalUtils.customShowToast("请输入用户名");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    NormalUtils.customShowToast("请输入密码");
                    return;
                }
                login(username,password);
                break;
            case R.id.tv_regiest:
                jumpPage(new Intent(LoginActivity.this,RegiestActivity.class));
                break;
            case R.id.tv_forget_pwd:
                jumpPage(new Intent(LoginActivity.this,ForgetPwdActivity.class));
                break;
        }
    }
    private void login(String username,String password){
        DialogUtils.showLoadingDialog(LoginActivity.this,getResources().getString(R.string.loading),true);
        NetParamets.login(LoginActivity.this, username, password, new NetCallBack<UserInfoResBean>() {
            @Override
            public void backSuccess(UserInfoResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.sendBroadcast(ActionFlag.LOGINLSUCCESS,null);
                if(cb_box.isChecked()){
                    CacheUtils.getInstance().saveShared("loginTime",System.currentTimeMillis()+"");
                }else{
                    CacheUtils.getInstance().removeShared("loginTime");
                }
            }

            @Override
            public void backError(String ex, UserInfoResBean result) {
                DialogUtils.dismissLoadingDialog();
                NormalUtils.customShowToast(ex);
            }
        });
    }
}
