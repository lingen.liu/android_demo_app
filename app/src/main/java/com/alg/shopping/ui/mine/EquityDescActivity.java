package com.alg.shopping.ui.mine;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.SellOrBuyResBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.bean.res.SubscribeRecordRowsBean;
import com.alg.shopping.inter.NetCallBack;
import com.alg.shopping.net.NetParamets;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.ui.base.BaseActivity;
import com.alg.shopping.ui.base.CommonBaseAdapter;
import com.alg.shopping.ui.base.CommonBaseHolder;
import com.alg.shopping.ui.view.MlistView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class EquityDescActivity extends BaseActivity implements OnRefreshListener{
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.mlv_list)
    MlistView mlv_list;
    @BindView(R.id.srf_refresh)
    SmartRefreshLayout srf_refresh;
    @BindView(R.id.tv_title)
    TextView tv_title;
    List<String> listStr;
    String id = "";
    SellOrBuyResultBean mSellOrBuyResultBean;
    ListOneAdapter adapter_one;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activiyt_equitydesc);
    }

    @Override
    protected void initViews() {
        tv_title.setText("土地权益兑换详情");
        id = getIntent().getStringExtra("id");
        String[] strArray = getResources().getStringArray(R.array.exchange_menu);
        listStr = Arrays.asList(strArray);
        adapter_one = new ListOneAdapter(EquityDescActivity.this,listStr,R.layout.list_item_item);
        mlv_list.setAdapter(adapter_one);
        srf_refresh.setOnRefreshListener(this);
        srf_refresh.setEnableLoadmore(false);
        srf_refresh.autoRefresh();
    }
    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
    public void requestData(String id){
        NetParamets.details_equity(EquityDescActivity.this, id, new NetCallBack<SellOrBuyResBean>() {
            @Override
            public void backSuccess(SellOrBuyResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                mSellOrBuyResultBean = result.result;
                float totalP = 0f;
                try{
                    totalP = Float.parseFloat(mSellOrBuyResultBean.price)*Float.parseFloat(mSellOrBuyResultBean.count);
                }catch (Exception e){}
                tv_num.setText(NormalUtils.getDecmiTo(totalP));
                adapter_one.setOtherData(mSellOrBuyResultBean);
                adapter_one.notifyDataSetChanged();
            }
            @Override
            public void backError(String ex, SellOrBuyResBean result) {
                srf_refresh.finishRefresh();
                srf_refresh.finishLoadmore();
                NormalUtils.customShowToast(ex);
            }
        });
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        requestData(id);
    }

    private class ListOneAdapter extends CommonBaseAdapter<String>{
        SellOrBuyResultBean mSellOrBuyResultBean;
        public ListOneAdapter(Context context, List<String> datas, int layoutId) {
            super(context, datas, layoutId);
        }
        public void setOtherData(SellOrBuyResultBean mSellOrBuyResultBean){
            this.mSellOrBuyResultBean = mSellOrBuyResultBean;
        }
        @Override
        public void convert(CommonBaseHolder holder, int position, String bean) {
            TextView tv_name = holder.getView(R.id.tv_name);
            TextView tv_value = holder.getView(R.id.tv_value);
            tv_name.setText(bean);
            if(mSellOrBuyResultBean != null){
                switch (position){
                    case 0:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.user_id) ? mSellOrBuyResultBean.user_id : "-");
                        break;
                    case 1:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.user_name) ? mSellOrBuyResultBean.user_name : "-");
                        break;
                    case 2:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.id) ? mSellOrBuyResultBean.id : "-");
                        break;
                    case 3:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.create_date) ? mSellOrBuyResultBean.create_date : "-");
                        break;
                    case 4:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.count) ? mSellOrBuyResultBean.count : "-");
                        break;
                    case 5:
                        String statuStr = "";
                        if(TextUtils.equals(mSellOrBuyResultBean.status,"COMPLETED")){
                            statuStr = "已完成";
                        }else if(TextUtils.equals(mSellOrBuyResultBean.status,"PENDING")){
                            statuStr = "未申请";
                        }else if(TextUtils.equals(mSellOrBuyResultBean.status,"PENDED")){
                            statuStr = "已申请 ";
                        }else if(TextUtils.equals(mSellOrBuyResultBean.status,"CANCEL")){
                            statuStr = "已取消";
                        }
                        tv_value.setText(statuStr);
                        break;
                    case 6:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.price) ? mSellOrBuyResultBean.price : "-");
                        break;
                    case 7:
                        String type = "";
                        if(TextUtils.equals(mSellOrBuyResultBean.type,"DH")){
                            type = "兑换";
                        }else if(TextUtils.equals(mSellOrBuyResultBean.type,"JP")){
                            type = "竞拍";
                        }
                        tv_value.setText(type);
                        break;
                    case 8:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.occ_date) ? mSellOrBuyResultBean.occ_date : "-");
                        break;
                    case 9:
                        float totalPrice = 0f;
                        try {
                            totalPrice = Float.parseFloat(mSellOrBuyResultBean.price)*Float.parseFloat(mSellOrBuyResultBean.count);
                        }catch (Exception e){}
                        tv_value.setText(NormalUtils.getDecmiTo(totalPrice));
                        break;
                    case 10:
                        tv_value.setText(!TextUtils.isEmpty(mSellOrBuyResultBean.td_number) ? mSellOrBuyResultBean.td_number : "-");
                        break;
                }
            }
        }
    }
}
