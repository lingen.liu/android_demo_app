package com.alg.shopping.ui.trade;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alg.shopping.R;
import com.alg.shopping.bean.EventBean;
import com.alg.shopping.bean.res.SellOrBuyResultBean;
import com.alg.shopping.net.NetConstants;
import com.alg.shopping.tools.IMGLoadUtils;
import com.alg.shopping.tools.ImageCompressionUtils;
import com.alg.shopping.tools.NormalUtils;
import com.alg.shopping.tools.ZxingUtils;
import com.alg.shopping.ui.base.BaseActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

public class TradeCodeActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_menu)
    TextView tv_menu;
    @BindView(R.id.iv_code)
    ImageView iv_code;

    @BindView(R.id.tv_trade_type)
    TextView tv_trade_type;
    @BindView(R.id.tv_userinfo)
    TextView tv_userinfo;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.llt_qrcode)
    LinearLayout llt_qrcode;
    @BindView(R.id.tv_limit_date)
    TextView tv_limit_date;
    Bitmap llt_bitmap;
    SellOrBuyResultBean mSellOrBuyResultBean;
    @Override
    protected void reciverMesssage(EventBean obj) {

    }

    @Override
    protected void setLayouts() {
        setContentView(R.layout.activity_tradecode);
    }

    @Override
    protected void initViews() {
        tv_menu.setVisibility(View.INVISIBLE);
        tv_title.setText("我的交易二维码");
        mSellOrBuyResultBean = (SellOrBuyResultBean) getIntent().getSerializableExtra("data");
        if(mSellOrBuyResultBean != null){
            tv_trade_type.setText(stringApped("交易类型：",TextUtils.equals(mSellOrBuyResultBean.type,"BUY") ? "买入" : "卖出"));
            tv_userinfo.setText(stringApped("发起用户：",mSellOrBuyResultBean.create_user_name));
            tv_num.setText(stringApped("交易数量：",mSellOrBuyResultBean.count));
            tv_price.setText(stringApped("交易价格：",mSellOrBuyResultBean.price));
            tv_limit_date.setText("交易时间：截止到"+mSellOrBuyResultBean.limit_time);
            try {
                String contentStr = mSellOrBuyResultBean.id;
                Bitmap bitmap = ZxingUtils.generateQR(contentStr);
                iv_code.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
                NormalUtils.customShowToast("二维码生成失败");
                IMGLoadUtils.loadCenterCropPic(R.mipmap.qr_code_pic,iv_code,R.mipmap.qr_code_pic);
            }
        }
    }

    private CharSequence stringApped(String name,String value){
        String priceStr = name+"<font color='#222222'>"+value+"</font>";
        return Html.fromHtml(priceStr);
    }

    @OnClick({R.id.tv_save,R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_save:
                try{
                    llt_bitmap = ImageCompressionUtils.getCacheBitmapFromView(llt_qrcode);
                    if(llt_bitmap != null && !llt_bitmap.isRecycled()){
                        try{
                            saveImageToGallery(llt_bitmap);
                        }catch (Exception e){
                            NormalUtils.customShowToast("二维码保存失败");
                        }
                    }else{
                        NormalUtils.customShowToast("未获取到二维码");
                    }
                }catch (Exception e){
                    NormalUtils.customShowToast("未获取到二维码");
                }
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }
    /*
     * 保存文件，文件名为当前日期
     */
    public void saveImageToGallery(Bitmap bitmap){
        String fileName ;
        File file ;
        String photoName = System.currentTimeMillis()+".jpg";
        if(Build.BRAND .equals("Xiaomi") ){ // 小米手机
            fileName = Environment.getExternalStorageDirectory().getPath()+"/DCIM/Camera/"+photoName;
        }else{  // Meizu 、Oppo
            fileName = Environment.getExternalStorageDirectory().getPath()+"/DCIM/"+photoName;
        }
        file = new File(fileName);

        if(file.exists()){
            file.delete();
        }
        FileOutputStream out;
        try{
            out = new FileOutputStream(file);
            // 格式为 JPEG，照相机拍出的图片为JPEG格式的，PNG格式的不能显示在相册中
            if(bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)){
                out.flush();
                out.close();
                MediaStore.Images.Media.insertImage(this.getContentResolver(), file.getAbsolutePath(),photoName, null);
            }
            // 发送广播，通知刷新图库的显示
            this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + fileName)));
            NormalUtils.customShowToast("保存成功，请在相册中查看");
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
